﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AoC2018;
using System.Collections.Generic;

namespace AoC2018Test
{
    [TestClass]
    public class Day2Test
    {
        [TestMethod]
        public void Creation ()
        {
            AoC2018.Day2 day2 = new AoC2018.Day2 ();
            Assert.IsNotNull (day2);
        }

        [TestMethod]
        public void CountCharsEmptyString ()
        {
            System.Collections.Generic.Dictionary<char, int> chars
                = Day2uut.CountChars ("");
            Assert.AreEqual (0, chars.Count);
        }
        [TestMethod]
        public void CountChars3a ()
        {
            System.Collections.Generic.Dictionary<char, int> chars
                = Day2uut.CountChars ("aaa");
            Assert.AreEqual (1, chars.Count);
            Assert.AreEqual (3, chars['a']);
        }
        [TestMethod]
        public void CountChars2a3b1c ()
        {
            System.Collections.Generic.Dictionary<char, int> chars
                = Day2uut.CountChars ("ababcb");
            Assert.AreEqual (3, chars.Count);
            Assert.AreEqual (2, chars['a']);
            Assert.AreEqual (3, chars['b']);
            Assert.AreEqual (1, chars['c']);
        }

        [TestMethod]
        public void Count2and3 ()
        {
            var result = Day2uut.Count2and3 ("ababab");
            Assert.IsFalse (result.Item1);
            Assert.IsTrue (result.Item2);

            result = Day2uut.Count2and3 ("ababac");
            Assert.IsTrue (result.Item1);
            Assert.IsTrue (result.Item2);
        }

        [TestMethod]
        public void GenerateChecksumFromFile ()
        {
            int checksum = Day2uut.GenerateChecksumFromFile ("C:\\Users\\Steffen\\aoc_2018\\c#\\Aoc\\AoC2018Test\\TestData\\Day2_a.txt");
            Assert.AreEqual (12, checksum);
        }
        [TestMethod]
        public void CountDifferentCharsEmptyTexts ()
        {
            AoC2018.Day2 day2 = new AoC2018.Day2 ();
            Assert.AreEqual (0, Day2uut.CountDifferentChars ("", ""));
        }
        [TestMethod]
        public void CountDifferentChars ()
        {
            Assert.AreEqual (2, Day2uut.CountDifferentChars ("abcde", "axcye"));
            Assert.AreEqual (1, Day2uut.CountDifferentChars ("fghij", "fguij"));
        }
        [TestMethod]
        public void FindBoxIdFromFile ()
        {
            Assert.AreEqual ("fgij"
                , Day2uut.FindBoxIdFromFile ("C:\\Users\\Steffen\\aoc_2018\\c#\\Aoc\\AoC2018Test\\TestData\\Day2_b.txt"));
        }
        [TestMethod]
        public void GetTextWithSameChars ()
        {
            AoC2018.Day2 day2 = new AoC2018.Day2 ();
            Assert.AreEqual ("fgij", Day2uut.GetTextWithSameChars ("fghij", "fguij"));
        }

        Day2 Day2uut = new Day2 ();

    }
}
