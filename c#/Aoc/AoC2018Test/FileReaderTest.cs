﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AoC2018;
using System.Collections.Generic;
using System.Text;

namespace AoC2018Test
{
    [TestClass]
    public class FileReaderTest
    {
        [TestMethod]
        public void ReadFile ()
        {
            List<string> lines = FileReader.ReadFile ("C:\\Users\\Steffen\\aoc_2018\\c#\\Aoc\\AoC2018Test\\TestData\\Day4_a.txt");
            Assert.AreEqual ("[1518-11-01 00:00] Guard #10 begins shift\n"
                           + "[1518-11-03 00:29] wakes up\n"
                           + "[1518-11-01 00:05] falls asleep\n"
                           + "[1518-11-05 00:03] Guard #99 begins shift\n"
                           + "[1518-11-01 23:58] Guard #99 begins shift\n"
                           + "[1518-11-01 00:25] wakes up\n"
                           + "[1518-11-01 00:30] falls asleep\n"
                           + "[1518-11-01 00:55] wakes up\n"
                           + "[1518-11-02 00:40] falls asleep\n"
                           + "[1518-11-02 00:50] wakes up\n"
                           + "[1518-11-03 00:05] Guard #10 begins shift\n"
                           + "[1518-11-04 00:02] Guard #99 begins shift\n"
                           + "[1518-11-04 00:36] falls asleep\n"
                           + "[1518-11-04 00:46] wakes up\n"
                           + "[1518-11-05 00:45] falls asleep\n"
                           + "[1518-11-03 00:24] falls asleep\n"
                           + "[1518-11-05 00:55] wakes up\n"
                , BuildString (lines));
        }
        [TestMethod]
        public void ReadFileAndSort ()
        {
            List<string> lines = FileReader.ReadFileAndSort ("C:\\Users\\Steffen\\aoc_2018\\c#\\Aoc\\AoC2018Test\\TestData\\Day4_a.txt");
            Assert.AreEqual (
                      "[1518-11-01 00:00] Guard #10 begins shift\n"
                    + "[1518-11-01 00:05] falls asleep\n"
                    + "[1518-11-01 00:25] wakes up\n"
                    + "[1518-11-01 00:30] falls asleep\n"
                    + "[1518-11-01 00:55] wakes up\n"
                    + "[1518-11-01 23:58] Guard #99 begins shift\n"
                    + "[1518-11-02 00:40] falls asleep\n"
                    + "[1518-11-02 00:50] wakes up\n"
                    + "[1518-11-03 00:05] Guard #10 begins shift\n"
                    + "[1518-11-03 00:24] falls asleep\n"
                    + "[1518-11-03 00:29] wakes up\n"
                    + "[1518-11-04 00:02] Guard #99 begins shift\n"
                    + "[1518-11-04 00:36] falls asleep\n"
                    + "[1518-11-04 00:46] wakes up\n"
                    + "[1518-11-05 00:03] Guard #99 begins shift\n"
                    + "[1518-11-05 00:45] falls asleep\n"
                    + "[1518-11-05 00:55] wakes up\n"
                , BuildString (lines));
        }

        static string BuildString(List<string> lines)
        {
            StringBuilder stringBuilder = new StringBuilder ();
            foreach (var line in lines)
            {
                stringBuilder.Append (line);
                stringBuilder.Append ("\n");
            }
            return stringBuilder.ToString ();
        }
    }
}
