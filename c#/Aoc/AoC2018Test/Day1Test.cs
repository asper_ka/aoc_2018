﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AoC2018;

namespace AoC2018Test
{
    [TestClass]
    public class Day1Test
    {
        [TestMethod]
        public void Creation ()
        {
            AoC2018.Day1 day1 = new AoC2018.Day1 ();
            Assert.IsNotNull (day1);
        }
        [TestMethod]
        public void InitialFreqIsNull ()
        {
            AoC2018.Day1 day1 = new AoC2018.Day1 ();
            Assert.AreEqual (day1.Result , 0);
        }
        [TestMethod]
        public void AddOneFreq ()
        {
            AoC2018.Day1 day1 = new AoC2018.Day1 ();
            day1.AddFreq (1);
            Assert.AreEqual (day1.Result , 1);
        }
        [TestMethod]
        public void SampleData1 ()
        {
            AoC2018.Day1 day1 = new AoC2018.Day1 ();
            day1.AddFreq (1);
            day1.AddFreq (-2);
            day1.AddFreq (+3);
            day1.AddFreq (1);
            Assert.AreEqual (day1.Result , 3);
        }
        [TestMethod]
        public void SampleData2 ()
        {
            AoC2018.Day1 day1 = new AoC2018.Day1 ();
            day1.AddFreq (1);
            day1.AddFreq (1);
            day1.AddFreq (1);
            Assert.AreEqual (day1.Result , 3);
        }
        [TestMethod]
        public void ReadFromFile ()
        {
            AoC2018.Day1 day1 = new AoC2018.Day1 ();
            day1.ReadFile ("C:\\Users\\Steffen\\aoc_2018\\c#\\Aoc\\AoC2018Test\\TestData\\Day1.txt");
            Assert.AreEqual (-6, day1.Result );
        }
        [TestMethod]
        public void GetFirstDuplicate_InitialFalse ()
        {
            AoC2018.Day1 day1 = new AoC2018.Day1 ();
            Assert.IsFalse (day1.HasDuplicate );
        }
        [TestMethod]
        public void GetFirstDuplicate ()
        {
            AoC2018.Day1 day1 = new AoC2018.Day1 ();
            int cnt = 0;
            while (!day1.HasDuplicate && cnt<100)
            {
                day1.ReadFile ("C:\\Users\\Steffen\\aoc_2018\\c#\\Aoc\\AoC2018Test\\TestData\\Day1_b.txt");
                ++cnt;
            }
            Assert.IsTrue (day1.HasDuplicate );
            Assert.AreEqual (14, day1.Duplicate );
        }
    }
}
