﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AoC2018;
using System.Collections.Generic;

namespace AoC2018Test
{
    [TestClass]
    public class Day3Test
    {
        [TestMethod]
        public void Creation ()
        {
            AoC2018.Day3 day3 = new AoC2018.Day3 ();
            Assert.IsNotNull (day3);
        }
        [TestMethod]
        public void InitialGridIsEmpty ()
        {
            Assert.AreEqual (0, Day3uut.Grid.Count );
        }

        [TestMethod]
        public void AddOnePatch()
        {
            Day3uut.AddPatch (1, 3, 2, 5, 4);
            Assert.AreEqual (20, Day3uut.Grid.Count);
        }

        [TestMethod]
        public void AddThreePatchesAndFindOverlap ()
        {
            Day3uut.AddPatch (1, 1, 3, 4, 4);
            Day3uut.AddPatch (2, 3, 1, 4, 4);
            Day3uut.AddPatch (3, 5, 5, 2, 2);

            Assert.AreEqual (4, Day3uut.CountOverlapping ());
        }

        [TestMethod]
        public void ParseEmptyPatchDefinition ()
        {
            var result = Day3uut.ParsePatchDefinition ("");
            Assert.AreEqual ((0, 0, 0, 0, 0), result);
        }
        [TestMethod]
        public void ParsePatchDefinition ()
        {
            var result = Day3uut.ParsePatchDefinition ("#123 @ 3,2: 5x4");
            Assert.AreEqual ((123, 3, 2, 5, 4), result);
            result = Day3uut.ParsePatchDefinition ("#1233 @ 437,811: 19x10");
            Assert.AreEqual ((1233, 437, 811, 19, 10), result);
        }

        [TestMethod]
        public void ReadPatchesFromFromFile ()
        {
            Day3uut.ReadPatchesFromFromFile ("C:\\Users\\Steffen\\aoc_2018\\c#\\Aoc\\AoC2018Test\\TestData\\Day3_a.txt");
            Assert.AreEqual (4, Day3uut.CountOverlapping ());
        }

        [TestMethod]
        public void DoesPatchOverlapOthers ()
        {
            Day3uut.AddPatch (1, 1, 3, 4, 4);
            Day3uut.AddPatch (2, 3, 1, 4, 4);
            Day3uut.AddPatch (3, 5, 5, 2, 2);

            Assert.IsTrue (Day3uut.DoesPatchOverlapOthers (1, 3, 4, 4));
            Assert.IsTrue (Day3uut.DoesPatchOverlapOthers (3, 1, 4, 4));
            Assert.IsFalse (Day3uut.DoesPatchOverlapOthers (5, 5, 2, 2));
        }
        [TestMethod]
        public void FindNotOverlappingPatch ()
        {
            Day3uut.ReadPatchesFromFromFile ("C:\\Users\\Steffen\\aoc_2018\\c#\\Aoc\\AoC2018Test\\TestData\\Day3_a.txt");
            Assert.AreEqual (3
                , Day3uut.FindNotOverlappingPatch ("C:\\Users\\Steffen\\aoc_2018\\c#\\Aoc\\AoC2018Test\\TestData\\Day3_a.txt"));
        }

        Day3 Day3uut = new Day3 ();

    }
}
