﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AoC2018;

namespace AoC2018Test
{
    [TestClass]
    public class Day4Test
    {
        [TestMethod]
        public void Creation ()
        {
            AoC2018.Day4 day4 = new AoC2018.Day4 ();
            Assert.IsNotNull (day4);
        }

        [TestMethod]
        public void PrintListInitialReturnsEmptyString()
        {
            Assert.AreEqual ("", Day4uut.PrintList ());
        }

        [TestMethod]
        public void AddOneSleepingGuard ()
        {
            Day4uut.ParseLine ("[1518-11-01 00:00] Guard #10 begins shift");
            Day4uut.ParseLine ("[1518-11-01 00:05] falls asleep");
            Day4uut.ParseLine ("[1518-11-01 00:25] wakes up");
            Assert.AreEqual ("11-01 #10  .....####################...................................\n"
                , Day4uut.PrintList ());
        }

        [TestMethod]
        public void AddOneSleepingGuardTwoSleeps ()
        {
            Day4uut.ParseLine ("[1518-11-01 00:00] Guard #10 begins shift");
            Day4uut.ParseLine ("[1518-11-01 00:05] falls asleep");
            Day4uut.ParseLine ("[1518-11-01 00:25] wakes up");
            Day4uut.ParseLine ("[1518-11-01 00:30] falls asleep");
            Day4uut.ParseLine ("[1518-11-01 00:55] wakes up");
            Assert.AreEqual ("11-01 #10  .....####################.....#########################.....\n"
                , Day4uut.PrintList ());
        }

        [TestMethod]
        public void ReadTestDataAndPrintList ()
        {
            Day4uut.ReadFile ("C:\\Users\\Steffen\\aoc_2018\\c#\\Aoc\\AoC2018Test\\TestData\\Day4_a.txt");
            Assert.AreEqual ( "11-01 #10  .....####################.....#########################.....\n"
                            + "11-02 #99  ........................................##########..........\n"
                            + "11-03 #10  ........................#####...............................\n"
                            + "11-04 #99  ....................................##########..............\n"
                            + "11-05 #99  .............................................##########.....\n"
                , Day4uut.PrintList ());
        }

        [TestMethod]
        public void GetGuardWithMostMinutesAsleep ()
        {
            Day4uut.ReadFile ("C:\\Users\\Steffen\\aoc_2018\\c#\\Aoc\\AoC2018Test\\TestData\\Day4_a.txt");
            Assert.AreEqual (10, Day4uut.GetGuardWithMostMinutesAsleep ());
        }

        [TestMethod]
        public void GetGuardWithSingleMostMinuteAsleep ()
        {
            Day4uut.ReadFile ("C:\\Users\\Steffen\\aoc_2018\\c#\\Aoc\\AoC2018Test\\TestData\\Day4_a.txt");
            var result = Day4uut.FindGuardWithSingleMostMinuteAsleep ();
            Assert.AreEqual (99, result.Item1);
            Assert.AreEqual (45, result.Item2);
        }

        [TestMethod]
        public void GetMaxNrMinuteForGuard ()
        {
            Day4uut.ReadFile ("C:\\Users\\Steffen\\aoc_2018\\c#\\Aoc\\AoC2018Test\\TestData\\Day4_a.txt");
            Assert.AreEqual (24, Day4uut.GetMaxNrMinutesForGuard (10));
        }

        Day4 Day4uut = new Day4 ();

    }
}
