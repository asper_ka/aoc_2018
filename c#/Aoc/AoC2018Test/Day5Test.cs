﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AoC2018;

namespace AoC2018Test
{
    [TestClass]
    public class Day5Test
    {
        [TestMethod]
        public void Creation ()
        {
            AoC2018.Day5 day5 = new AoC2018.Day5 ();
            Assert.IsNotNull (day5);
        }

        [TestMethod]
        public void Empty ()
        {
            Assert.AreEqual ("", Day5uut.ToString ());
            Assert.AreEqual (0, Day5uut.Count ());
        }
        [TestMethod]
        public void AddOne ()
        {
            Day5uut.Add ('A');
            Assert.AreEqual ("A", Day5uut.ToString ());
            Assert.AreEqual (1, Day5uut.Count ());
        }
        [TestMethod]
        public void AddThree ()
        {
            Day5uut.Add ('A');
            Day5uut.Add ('b');
            Day5uut.Add ('C');
            Assert.AreEqual ("AbC", Day5uut.ToString ());
            Assert.AreEqual (3, Day5uut.Count ());
        }

        [TestMethod]
        public void SameTypeReacts ()
        {
            Day5uut.Add ('b');
            Day5uut.Add ('B');
            Assert.AreEqual ("", Day5uut.ToString ());
            Assert.AreEqual (0, Day5uut.Count ());
        }

        [TestMethod]
        public void SamePolarityDoesNotReacts ()
        {
            Day5uut.Add ('C');
            Day5uut.Add ('C');
            Assert.AreEqual ("CC", Day5uut.ToString ());
            Assert.AreEqual (2, Day5uut.Count ());
        }

        [TestMethod]
        public void AddFromFile ()
        {
            Day5uut.AddFromFile ("C:\\Users\\Steffen\\aoc_2018\\c#\\Aoc\\AoC2018Test\\TestData\\Day5_a.txt");
            Assert.AreEqual ("dabCBAcaDA", Day5uut.ToString ());
            Assert.AreEqual (10, Day5uut.Count ());

        }

        [TestMethod]
        public void IgnoreTypeA ()
        {
            Day5uut.IgnoreType ('a');
            Day5uut.AddFromFile ("C:\\Users\\Steffen\\aoc_2018\\c#\\Aoc\\AoC2018Test\\TestData\\Day5_a.txt");
            Assert.AreEqual ("dbCBcD", Day5uut.ToString ());
            Assert.AreEqual (6, Day5uut.Count ());

        }

        [TestMethod]
        public void IgnoreTypeC ()
        {
            Day5uut.IgnoreType ('c');
            Day5uut.AddFromFile ("C:\\Users\\Steffen\\aoc_2018\\c#\\Aoc\\AoC2018Test\\TestData\\Day5_a.txt");
            Assert.AreEqual ("daDA", Day5uut.ToString ());
            Assert.AreEqual (4, Day5uut.Count ());
        }

        [TestMethod]
        public void OptimizePolymer ()
        {
            var result = Day5uut.OptimizePolymer ("C:\\Users\\Steffen\\aoc_2018\\c#\\Aoc\\AoC2018Test\\TestData\\Day5_a.txt");
            Assert.AreEqual ('c', result.Item1);
            Assert.AreEqual (4, result.Item2);
        }

        Day5 Day5uut = new Day5 ();

    }
}
