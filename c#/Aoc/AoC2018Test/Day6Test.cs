﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AoC2018;
using System.Collections.Generic;
using System.Linq;

namespace AoC2018Test
{
    [TestClass]
    public class Day6Test
    {
        [TestMethod]
        public void Creation ()
        {
            AoC2018.Day6 day6 = new AoC2018.Day6 ();
            Assert.IsNotNull (day6);
        }

        [TestMethod]
        public void ParseLine()
        {
            var result = Day6uut.ParseLine ("1,3");
            Assert.AreEqual ((1, 3), result);
            result = Day6uut.ParseLine ("10 ,  -34");
            Assert.AreEqual ((10, -34), result);
            result = Day6uut.ParseLine ("-23 , 3");
            Assert.AreEqual ((-23, 3), result);
        }

        [TestMethod]
        public void ReadFromFile ()
        {
            Assert.AreEqual (6, Day6uut.Coordinates.Count);
            Assert.AreEqual ((4, 4), Day6uut.Coordinates[3]);
        }

        [TestMethod]
        public void InitGrid ()
        {
            Day6uut.InitGrid ();
            Assert.AreEqual (10, Day6uut.Grid.GetUpperBound (0));
            Assert.AreEqual (11, Day6uut.Grid.GetUpperBound (1));
            Assert.AreEqual (1, Day6uut.XOffset);
            Assert.AreEqual (0, Day6uut.YOffset);
            Assert.AreEqual (0, Day6uut.Grid[0, 0]);
            Assert.AreEqual (-1, Day6uut.Grid[0, 4]);
            Assert.AreEqual (3, Day6uut.Grid[4, 2]);

        }

        [TestMethod]
        public void CountNeighbours ()
        {
            Day6uut.InitGrid ();
            Dictionary<int,int> neighbours = Day6uut.CountNeighbours ();
            Assert.AreEqual (-1, neighbours[0]); // infinite
            Assert.AreEqual (9, neighbours[3]);
            Assert.AreEqual (17, neighbours.Values.Max());
        }

        [TestMethod]
        public void CalcDistance ()
        {
            Assert.AreEqual (0, Day6.CalcDistance ((0, 0), (0, 0)));
            Assert.AreEqual (3, Day6.CalcDistance ((7, 8), (7, 5)));
            Assert.AreEqual (2, Day6.CalcDistance ((7, 3), (7, 5)));
            Assert.AreEqual (6, Day6.CalcDistance ((4, 8), (7, 5)));
            Assert.AreEqual (3, Day6.CalcDistance ((7, 18), (4, 18)));
        }

        [TestMethod]
        public void GetIndexOfCoordWithMinDist ()
        {
            Assert.AreEqual (0, Day6uut.GetIndexOfCoordWithMinDist ((0, 0)));
            Assert.AreEqual (-1, Day6uut.GetIndexOfCoordWithMinDist ((0, 4)));
            Assert.AreEqual (3, Day6uut.GetIndexOfCoordWithMinDist ((4, 2)));
        }

        [TestMethod]
        public void GetNrCoordsWithSumDistLessThan()
        {
            Day6uut.InitGrid ();
            Assert.AreEqual(16, Day6uut.GetNrCoordsWithSumDistLessThan (32));
        }


        Day6 Day6uut = new Day6 ("C:\\Users\\Steffen\\aoc_2018\\c#\\Aoc\\AoC2018Test\\TestData\\Day6.txt");

    }
}
