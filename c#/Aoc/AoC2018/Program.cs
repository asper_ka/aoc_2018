﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AoC2018
{
    class Program
    {
        static void Main(string[] args)
        {
            //RunDay1 ();
            //RunDay2 ();
            //RunDay3 ();
            //RunDay4 ();
            //RunDay5 ();
            RunDay6 ();
            Console.ReadKey ();
        }

        private static void RunDay1 ()
        {
            Day1 day1 = new Day1 ();
            Console.WriteLine ("Day1:");
            day1.ReadFile (m_PathToData + "\\1\\input.txt");
            Console.WriteLine ("resulting frequeny: {0}", day1.Result);
            int cnt = 0;
            while (!day1.HasDuplicate && cnt < 10000)
            {
                day1.ReadFile (m_PathToData + "\\1\\input.txt");
                ++cnt;
            }
            if (day1.HasDuplicate)
            {
                Console.WriteLine ("first duplicate: {0}", day1.Duplicate);
            }
            else
            {
                Console.WriteLine ("no duplicate !!");
            }
        }

        private static void RunDay2 ()
        {
            Day2 day2 = new Day2 ();
            Console.WriteLine ("Day2:");
            var checksum = day2.GenerateChecksumFromFile (m_PathToData + "\\2\\2.txt");
            Console.WriteLine ("resulting checksum: {0}", checksum);
            var boxId = day2.FindBoxIdFromFile (m_PathToData + "\\2\\2.txt");
            Console.WriteLine ("Common letters: {0}", boxId);
        }

        private static void RunDay3 ()
        {
            Day3 day3 = new Day3 ();
            String path = m_PathToData + "\\3\\3.txt";
            Console.WriteLine ("Day3:");
            day3.ReadPatchesFromFromFile(path);
            Console.WriteLine ("{0} overlapping patches", day3.CountOverlapping ());
            Console.WriteLine ("Patch {0} does not overlap", day3.FindNotOverlappingPatch (path));
        }

        private static void RunDay4 ()
        {
            Day4 day4 = new Day4 ();
            String path = m_PathToData + "\\4\\4.txt";
            Console.WriteLine ("Day4:");
            day4.ReadFile (path);
            int guard = day4.GetGuardWithMostMinutesAsleep ();
            int maxMinute = day4.GetMaxNrMinutesForGuard (guard);
            Console.WriteLine ("part one: {0} * {1} = {2}", guard, maxMinute, guard * maxMinute);
            var resultPartTwo = day4.FindGuardWithSingleMostMinuteAsleep ();
            Console.WriteLine ("part two: {0} * {1} = {2}"
                , resultPartTwo.Item1
                , resultPartTwo.Item2
                , resultPartTwo.Item1 * resultPartTwo.Item2);
        }

        private static void RunDay5 ()
        {
            Day5 day5 = new Day5 ();
            String path = m_PathToData + "\\5\\5.txt";
            Console.WriteLine ("Day5:");
            day5.AddFromFile (path);
            Console.WriteLine ("Unit: {0}", day5.Count ());
            var result = day5.OptimizePolymer (path);
            Console.WriteLine ("Optimal polymer has length {0}", result.Item2);
        }


        private static void RunDay6 ()
        {
            Console.WriteLine ("Day6:");
            String path = m_PathToData + "\\6\\6.txt";
            Day6 day6 = new Day6 (path);
            day6.InitGrid ();
            Console.WriteLine ("max area: {0}", day6.CountNeighbours ().Values.Max ());
            Console.WriteLine ("nr coordinates with totaldisa´tance less than 10000: {0}"
                , day6.GetNrCoordsWithSumDistLessThan (10000));
        }

        private static string m_PathToData = "C:\\Users\\Steff\\Documents\\AoC\\aoc_2018\\sra";
    }
}
