﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AoC2018
{
    public class Day1
    {
        public int Result { get; private set; } = 0;
        public void AddFreq(int f)
        {
            Result += f;
            if (m_Frequencies.Contains (Result) && !HasDuplicate)
            {
                HasDuplicate = true;
                Duplicate = Result;
            }
            m_Frequencies.Add (Result);
        }
        public void ReadFile(String path)
        {
            List<String> l_Lines = FileReader.ReadFile (path);
            foreach (String l_Line in l_Lines)
            {
                AddFreq (int.Parse (l_Line));
            }
        }

        public bool HasDuplicate { get; private set; } = false;

        public int Duplicate { get; private set; } = 0;

        private HashSet<int> m_Frequencies = new HashSet<int> ();
    }
}
