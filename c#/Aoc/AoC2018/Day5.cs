﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace AoC2018
{
    public class Day5
    {
        public override string ToString()
        {
            StringBuilder result = new StringBuilder();
            foreach (var unit in Polymer)
            {
                result.Append (unit);
            }
            return result.ToString ();
        }
        public void Add(char unit)
        {
            if (Char.ToLower(unit) == Char.ToLower(TypeToIgnore))
            {
                return;
            }
            if (Polymer.Count > 0 && DoesNewUnitReact (unit))
            {
                Polymer.RemoveAt (Polymer.Count-1);
            }
            else
            {
                Polymer.Add (unit);
            }
        }

        public void AddFromFile (string path)
        {
            string body = System.IO.File.ReadAllText (path);
            foreach (char c in body)
            {
                if (c >= 32)
                {
                    Add (c);
                }
            }
        }

        public void IgnoreType(char typeToIgnore)
        {
            TypeToIgnore = typeToIgnore;
        }

        private bool DoesNewUnitReact (char unit)
        {
            char lastUnit = Polymer[Polymer.Count - 1];
            if (Char.ToLower (lastUnit) == Char.ToLower (unit))
            {
                if (lastUnit != unit)
                {
                    return true;
                }
            }
            return false;
        }

        public (char,int) OptimizePolymer (string path)
        {
            int MinLength = Int32.MaxValue;
            char MinType = ' ';
            foreach(char type in "abcdefghijklmnopqrstuvwxyz")
            {
                Polymer.Clear ();
                IgnoreType (type);
                AddFromFile (path);
                if  (Count() < MinLength)
                {
                    MinLength = Count ();
                    MinType = type;
                }
            }
            return (MinType, MinLength);
        }
        public int Count()
        {
            return Polymer.Count;
        }
        private List<char> Polymer = new List<char>();
        private char TypeToIgnore = ' ';
    }
}
