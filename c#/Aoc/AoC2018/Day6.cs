﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace AoC2018
{
    public class Day6
    {
        public Day6()
        { }
        public Day6(string path)
        {
            ReadFromFile (path);
        }
        public void ReadFromFile (string path)
        {
            List<String> l_Lines = FileReader.ReadFile (path);
            foreach (String l_Line in l_Lines)
            {
                Coordinates.Add (ParseLine (l_Line));
            }
        }

        public (int,int) ParseLine(string line)
        {
            Regex regex = new Regex (@"(-?\d+)\s*,\s*(-?\d+)");
            Match match = regex.Match (line);
            if (match.Success)
            {
                return (int.Parse (match.Groups[1].Value), int.Parse (match.Groups[2].Value));
            }
            return (0, 0);
        }

        public void InitGrid()
        {
            XOffset = Int32.MaxValue;
            YOffset = Int32.MaxValue;
            int maxX = Int32.MinValue;
            int maxY = Int32.MinValue;
            foreach (var c in Coordinates)
            {
                if (c.Item1 > maxX)
                {
                    maxX = c.Item1;
                }
                if (c.Item1 < XOffset)
                {
                    XOffset = c.Item1;
                }
                if (c.Item2 > maxY)
                {
                    maxY = c.Item2;
                }
                if (c.Item2 < YOffset)
                {
                    YOffset = c.Item2;
                }
            }
            XOffset -= 1;
            YOffset -= 1;
            Grid = new int[maxX - XOffset + 3, maxY - YOffset + 3];
            FillGrid ();
        }

        private void FillGrid()
        {
            for (int x = 0; x < Grid.GetLength (0); x += 1)
            {
                for (int y = 0; y < Grid.GetLength (1); y += 1)
                {
                    Grid[x, y] = GetIndexOfCoordWithMinDist ((x + XOffset, y + YOffset));
                }
            }
        }

        public static int CalcDistance((int,int) c1, (int,int) c2)
        {
            return Math.Abs(c1.Item1 - c2.Item1)
                + Math.Abs (c1.Item2 - c2.Item2);
        }

        public int GetIndexOfCoordWithMinDist((int,int)coord)
        {
            Dictionary<int, int> distances = new Dictionary<int, int> ();
            int minDist = Int32.MaxValue;
            for (int idx = 0; idx<Coordinates.Count;++idx)
            {
                int d = CalcDistance (coord, Coordinates[idx]);
                distances.Add(idx, d);
                if (d<minDist)
                {
                    minDist = d;
                }
            }
            int minIdx = -1;
            foreach (var d in distances)
            {
                if (d.Value == minDist)
                {
                    if (minIdx >=0)
                    {
                        return -1;
                    }
                    minIdx = d.Key;
                }
            }
            return minIdx;
        }

        public Dictionary<int, int> CountNeighbours ()
        {
            Dictionary<int, int> n = new Dictionary<int, int> ();
            for (int idx = 0; idx < Coordinates.Count; ++idx)
            {
                n.Add (idx, 0);
            }
            for (int x = 0; x < Grid.GetLength (0); x += 1)
            {
                for (int y = 0; y < Grid.GetLength (1); y += 1)
                {
                    int idx = Grid[x, y];
                    if (idx >= 0)
                    {
                        if ((x == 0) || (y == 0) || (x == Grid.GetLength (0) - 1) || (y == Grid.GetLength (1) - 1))
                        {
                            //infinite
                            n[idx] = -1;
                        }
                        if (n[idx] >= 0)
                        {
                            n[idx] += 1;
                        }
                    }
                }
            }
            return n;
        }

        public int GetNrCoordsWithSumDistLessThan(int maxDist)
        {
            int nr = 0;
            for (int x = 0; x < Grid.GetLength (0); x += 1)
            {
                for (int y = 0; y < Grid.GetLength (1); y += 1)
                {
                    int sum = 0;
                    foreach (var c in Coordinates)
                    {
                        sum += CalcDistance (c, (x + XOffset, y + YOffset));
                    }
                    if (sum<maxDist)
                    {
                        nr++;
                    }
                }
            }
                    return nr;
        }

        public List<(int, int)> Coordinates { get; private set; } = new List<(int, int)>();
        public int[,] Grid { get; private set; } = new int[1, 1];
        public int XOffset { get; private set; } = 0;
        public int YOffset { get; private set; } = 0;

    }
}
