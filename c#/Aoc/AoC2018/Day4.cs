﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace AoC2018
{
    public class Day4
    {
        public string PrintList()
        {
            StringBuilder result = new StringBuilder ();
            foreach (var entry in ShiftEntries)
            {
                result.Append (entry.Key + "  ");
                foreach (var i in entry.Value)
                {
                    result.Append (i == 0 ? "." : "#");
                }
                result.Append ("\n");
            }
            return result.ToString ();
        }

        public void ParseLine (string line)
        {
            // [1518-11-01 00:00] Guard #10 begins shift
            Regex regexGuard = new Regex (@"\[\d+-\d+-\d+.*Guard #(\d+) begins shift");
            Match match = regexGuard.Match (line);
            if (match.Success)
            {
                CurrentGuard = match.Groups[1].Value;
                return;
            }
            // [1518-11-01 00:05] falls asleep
            Regex regexBeginSleep = new Regex (@"\[\d+-\d+-\d+ \d+:(\d+)] .*falls asleep");
            match = regexBeginSleep.Match (line);
            if (match.Success)
            {
                CurrentStartMinute = int.Parse (match.Groups[1].Value);
                return;
            }
            // [1518-11-01 00:25] wakes up
            Regex regexWakeUp = new Regex (@"\[\d+-(\d+-\d+) \d+:(\d+)] .*wakes up");
            match = regexWakeUp.Match (line);
            if (match.Success)
            {
                string date = match.Groups[1].Value;
                int endMinute = int.Parse (match.Groups[2].Value);
                AddEntry (date, CurrentGuard, CurrentStartMinute, endMinute);
            }
        }

        private void AddEntry(string date, string guardID, int beginMinute, int endMinute)
        {
            string key = date + " #" + guardID;
            if (!ShiftEntries.ContainsKey(key))
            {
                ShiftEntries.Add (key, new int[60]);
            }

            int intID = int.Parse (guardID);
            if (!MinutesAsleep.ContainsKey (intID))
            {
                MinutesAsleep.Add (intID, 0);
            }
            if (!NrMinutesPerGuard.ContainsKey (intID))
            {
                NrMinutesPerGuard.Add (intID, new int[60]);
            }
            for (int m = beginMinute; m < endMinute; ++m)
            {
                ShiftEntries[key][m] = 1;
                NrMinutesPerGuard[intID][m]++;
            }

            MinutesAsleep[intID] += (endMinute - beginMinute);
        }   

        public int GetMaxNrMinutesForGuard (int guardID)
        {
            int maxNr = NrMinutesPerGuard[guardID].Max ();
            return Array.IndexOf (NrMinutesPerGuard[guardID], maxNr);
        }
        public void ReadFile (string path)
        {
            List<String> l_Lines = FileReader.ReadFileAndSort (path);
            foreach (String l_Line in l_Lines)
            {
                ParseLine (l_Line);
            }
        }

        public int GetGuardWithMostMinutesAsleep()
        {
            int maxMinutes = 0;
            int maxID = -1;
            foreach (var minutes in MinutesAsleep)
            {
                if (minutes.Value > maxMinutes)
                {
                    maxMinutes = minutes.Value;
                    maxID = minutes.Key;
                }
            }
            return maxID;
        }

        public (int,int) FindGuardWithSingleMostMinuteAsleep()
        {
            int maxNr = -1;
            int maxGuardId = -1;
            int maxMinute = -1;
            foreach (var item in NrMinutesPerGuard)
            {
                int maxNrForGuard = NrMinutesPerGuard[item.Key].Max ();
                if (maxNrForGuard > maxNr)
                {
                    maxNr = maxNrForGuard;
                    maxGuardId = item.Key;
                    maxMinute = Array.IndexOf (NrMinutesPerGuard[item.Key], maxNrForGuard);
                }
            }
            return (maxGuardId, maxMinute);

        }

        private string CurrentGuard = "";
        private int CurrentStartMinute = -1;
        private Dictionary<string, int[]> ShiftEntries = new Dictionary<string, int[]>();
        private Dictionary<int, int> MinutesAsleep = new Dictionary<int, int>();
        private Dictionary<int, int[]> NrMinutesPerGuard = new Dictionary<int, int[]> ();
    }
}
