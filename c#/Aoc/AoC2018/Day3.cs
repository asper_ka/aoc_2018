﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AoC2018
{
    public class Day3
    {
        public void AddPatch(int id, int left, int top, int width, int heigth)
        {
            for (int x = 0; x < width; ++x)
            {
                for (int y = 0; y < heigth; ++y)
                {
                    var pos = (x + left, y + top);
                    if (Grid.ContainsKey (pos))
                    {
                        Grid[pos] += 1;
                    }
                    else
                    {
                        Grid.Add (pos, 1);
                    }
                }
            }
        }
        public int CountOverlapping ()
        {
            int cnt = 0;
            foreach (var item in Grid)
            {
                if (item.Value > 1)
                {
                    ++cnt;
                }
            }
            return cnt;
        }

        public (int,int,int,int,int) ParsePatchDefinition(String definition)
        {
            Regex regex = new Regex (@"#(\d+)\s*@\s*(\d+),(\d+):\s*(\d+)x(\d+)");
            Match match = regex.Match (definition);
            if (match.Success)
            {
                return (int.Parse (match.Groups[1].Value)
                    , int.Parse (match.Groups[2].Value)
                    , int.Parse (match.Groups[3].Value)
                    , int.Parse (match.Groups[4].Value)
                    , int.Parse (match.Groups[5].Value));
            }
            return (0, 0, 0, 0, 0);
        }

        public void ReadPatchesFromFromFile (String path)
        {
            List<String> l_Lines = FileReader.ReadFile (path);
            foreach (String l_Line in l_Lines)
            {
                var result = ParsePatchDefinition (l_Line);
                AddPatch (result.Item1, result.Item2, result.Item3, result.Item4, result.Item5);
            }
        }

        public bool DoesPatchOverlapOthers (int left, int top, int width, int heigth)
        {
            for (int x = 0; x < width; ++x)
            {
                for (int y = 0; y < heigth; ++y)
                {
                    var pos = (x + left, y + top);
                    if (Grid[pos] > 1)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        public int FindNotOverlappingPatch (String path)
        {
            List<String> l_Lines = FileReader.ReadFile (path);
            foreach (String l_Line in l_Lines)
            {
                var result = ParsePatchDefinition (l_Line);
                if (!DoesPatchOverlapOthers (result.Item2, result.Item3, result.Item4, result.Item5))
                {
                    return result.Item1;
                }
            }
            return -1;
        }
        public Dictionary<(int, int),int> Grid { get; private set; } = new Dictionary<(int, int), int> ();

    }
}
