﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AoC2018
{
    public class Day2
    {
        public Dictionary<char, int> CountChars(String text)
        {
            Dictionary<char, int> l_Chars = new Dictionary<char, int> ();
            foreach (char c in text)
            {
                if (l_Chars.ContainsKey (c))
                {
                    l_Chars[c]++;
                }
                else
                {
                    l_Chars.Add (c, 1);
                }
            }
            return l_Chars;
        }

        public (bool, bool) Count2and3 (String text)
        {
            bool l_Has2 = false;
            bool l_Has3 = false;
            foreach(KeyValuePair<char,int> item in CountChars(text))
            {
                if (item.Value == 2)
                {
                    l_Has2 = true;
                }
                if (item.Value == 3)
                {
                    l_Has3 = true;
                }
            }
            return (l_Has2, l_Has3);
        }
        public int GenerateChecksumFromFile (String path)
        {
            int l_Nr2 = 0;
            int l_Nr3 = 0;
            List<String> l_Lines = FileReader.ReadFile (path);
            foreach (String l_Line in l_Lines)
            {
                var result = Count2and3 (l_Line);
                if (result.Item1)
                {
                    l_Nr2++;
                }
                if (result.Item2)
                {
                    l_Nr3++;
                }
            }
            return l_Nr2 * l_Nr3;
        }

        public int CountDifferentChars(String text1, String text2)
        {
            int nrDifferent = 0;
            for (int idx = 0; idx<text1.Length; ++idx)
            {
                if (text1[idx] != text2[idx])
                {
                    ++nrDifferent;
                }
            }
            return nrDifferent;
        }

        public String FindBoxIdFromFile(String path)
        {
            List<String> l_Lines = FileReader.ReadFile (path);
            for (int idx1=0; idx1 < l_Lines.Count-1; ++idx1)
            {
                for (int idx2 = idx1+1; idx2 < l_Lines.Count; ++idx2)
                {
                    if (1== CountDifferentChars(l_Lines[idx1], l_Lines[idx2]))
                    {
                        return GetTextWithSameChars(l_Lines[idx1], l_Lines[idx2]);
                    }
                }
            }
            return "";
        }

        public String GetTextWithSameChars(String text1, String text2)
        {
            var result = new StringBuilder ();
            for (int idx = 0; idx < text1.Length; ++idx)
            {
                if (text1[idx] == text2[idx])
                {
                    result.Append(text1[idx]);
                }
            }
            return result.ToString ();
        }

    }
}
