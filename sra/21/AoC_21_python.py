# -*- coding: utf-8 -*-
"""
Created on Fri Dec 21 11:57:13 2018

@author: steff
"""
r0 = 0
r1 = 0
r3 = 0
r5 = 0

vals = []
nr_runs = 0

last_r5 = 0
found = False
while not found and nr_runs < 100000:
    r3 = r5 | 65536
    r5 = 733884
    condition = False
    while not condition:
        r1 = r3 & 255
        r5 = r5 + r1
        r5 = r5 & 16777215
        r5 = r5 * 65899
        r5 = r5 & 16777215
        #print ("r3={0}, r5={1}".format(r3,r5))
        condition = (256 > r3)
        if not condition:
            r3 = r3 >> 8
    nr_runs += 1
    #print (r5)
    if r5 in vals:
        print ("found repeat: {0} - last_r5 = {1}".format(r5, last_r5))
        found = True
    vals.append(r5)
    last_r5 = r5

#print (r5)
