# -*- coding: utf-8 -*-
"""
Created on Sun Dec 16 07:59:04 2018

@author: steff
"""


def addr(reg,a, b, c):
    reg[c] = reg[a] + reg[b]
    
def addi(reg,a,b,c):
    reg[c] = reg[a] + b

def mulr(reg,a, b, c):
    reg[c] = reg[a] * reg[b]
    
def muli(reg,a,b,c):
    reg[c] = reg[a] * b
    
def banr(reg,a, b, c):
    r = reg[a] & reg[b]
    reg[c] = r
    
def bani(reg,a,b,c):
    reg[c] = reg[a] & b
    
def borr(reg,a, b, c):
    reg[c] = reg[a] | reg[b]
    
def bori(reg,a,b,c):
    reg[c] = reg[a] | b
    
def setr(reg,a,b,c):
    reg[c] = reg[a]

def seti(reg,a,b,c):
    reg[c] = a

def gtir(reg,a,b,c):
    if a > reg[b]:
        reg[c] = 1
    else:
        reg[c] = 0

def gtri(reg,a,b,c):
    if reg[a] > b:
        reg[c] = 1
    else:
        reg[c] = 0

def gtrr(reg,a,b,c):
    if reg[a] > reg[b]:
        reg[c] = 1
    else:
        reg[c] = 0

def eqir(reg,a,b,c):
    if a == reg[b]:
        reg[c] = 1
    else:
        reg[c] = 0

def eqri(reg,a,b,c):
    if reg[a] == b:
        reg[c] = 1
    else:
        reg[c] = 0

def eqrr(reg,a,b,c):
    if reg[a] == reg[b]:
        reg[c] = 1
    else:
        reg[c] = 0

def t_addr(a, b, c):
    return "r{2} = r{0} + r{1}".format(a,b,c)
    
def t_addi(a,b,c):
    return "r{2} = r{0} + {1}".format(a,b,c)

def t_mulr(a, b, c):
    return "r{2} = r{0} * r{1}".format(a,b,c)
    
def t_muli(a,b,c):
    return "r{2} = r{0} * {1}".format(a,b,c)
    
def t_banr(a, b, c):
    return "r{2} = r{0} & r{1}".format(a,b,c)
    
def t_bani(a,b,c):
    return "r{2} = r{0} & {1}".format(a,b,c)
    
def t_borr(a, b, c):
    return "r{2} = r{0} | r{1}".format(a,b,c)
    
def t_bori(a,b,c):
    return "r{2} = r{0} | {1}".format(a,b,c)
    
def t_setr(a,b,c):
    return "r{2} = r{0}".format(a,b,c)

def t_seti(a,b,c):
    return "r{2} = {0}".format(a,b,c)

def t_gtir(a,b,c):
    return "r{2} = ({0} > r{1}) ? 1 : 0".format(a,b,c)

def t_gtri(a,b,c):
    return "r{2} = (r{0} > {1}) ? 1 : 0".format(a,b,c)

def t_gtrr(a,b,c):
    return "r{2} = (r{0} > r{1}) ? 1 : 0".format(a,b,c)

def t_eqir(a,b,c):
    return "r{2} = ({0} == r{1}) ? 1 : 0".format(a,b,c)

def t_eqri(a,b,c):
    return "r{2} = (r{0} == {1}) ? 1 : 0".format(a,b,c)

def t_eqrr(a,b,c):
    return "r{2} = (r{0} == r{1}) ? 1 : 0".format(a,b,c)

opcodes = {}
opcodes["addr"] = addr
opcodes["addi"] = addi
opcodes["mulr"] = mulr
opcodes["muli"] = muli
opcodes["banr"] = banr
opcodes["bani"] = bani
opcodes["borr"] = borr
opcodes["bori"] = bori
opcodes["setr"] = setr
opcodes["seti"] = seti
opcodes["gtir"] = gtir
opcodes["gtri"] = gtri
opcodes["gtrr"] = gtrr
opcodes["eqir"] = eqir
opcodes["eqri"] = eqri
opcodes["eqrr"] = eqrr

t_opcodes = {}
t_opcodes["addr"] = t_addr
t_opcodes["addi"] = t_addi
t_opcodes["mulr"] = t_mulr
t_opcodes["muli"] = t_muli
t_opcodes["banr"] = t_banr
t_opcodes["bani"] = t_bani
t_opcodes["borr"] = t_borr
t_opcodes["bori"] = t_bori
t_opcodes["setr"] = t_setr
t_opcodes["seti"] = t_seti
t_opcodes["gtir"] = t_gtir
t_opcodes["gtri"] = t_gtri
t_opcodes["gtrr"] = t_gtrr
t_opcodes["eqir"] = t_eqir
t_opcodes["eqri"] = t_eqri
t_opcodes["eqrr"] = t_eqrr
