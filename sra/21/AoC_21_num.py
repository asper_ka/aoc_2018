# -*- coding: utf-8 -*-
"""
Created on Sun Dec 16 08:07:04 2018

@author: steff
"""

from opcodes import *

import time
start = time.time()

#ipreg = 0
#file = open("test_program.txt")

ipreg = 4
file = open("program.txt")

code = file.read().splitlines()

numcodes = {}
numfuncs = []

idx = 0
for f in opcodes:
    numcodes[f] = opcodes[f]
    numfuncs.append(opcodes[f])
    idx += 1
    
compiled = []
uncompiled = []
translated = open("program_t.txt", "w")
l_idx = 0
for l in code:
    if l[0] != '#':
        uncompiled.append(l)
        tkns = l.split()
        #print (tkns)
        compiled.append([numcodes[tkns[0]], int(tkns[1]), int(tkns[2]), int(tkns[3])])    
        translated.write("{:2d}: ".format(l_idx) + t_opcodes[tkns[0]](int(tkns[1]), int(tkns[2]), int(tkns[3]))+"\n")
        l_idx +=1
translated.close()
    
def execute_line(l,r):
    tkns = l.split()
    opcodes[tkns[0]](r, int(tkns[1]), int(tkns[2]), int(tkns[3]))

def execute_line_c(l,r):
    l[0](r, l[1], l[2], l[3])

reg = [0,0,0,0,0,0]

#log = open("21_log.txt","w")
ip = 0
nr_lines = 0
while ip >=0 and ip < len(code) and nr_lines < 1000000:
    # jedes mal wenn wir die Abbruchbedingung erreichen, register ausgeben
    if ip == 28:
        print (reg[5])
    reg[ipreg] = ip
    line = uncompiled[ip]
    #print ("ip {0} {1} {2} ".format(ip, reg, code[ip]),end="")
    #log.write ("ip {0} {1} {2} ".format(ip, reg, uncompiled[ip]))
    #execute_line(line,reg)
    line = compiled[ip]
    execute_line_c(line,reg)
    #print (reg)
    #log.write("{0}\n".format(reg))
    ip = reg[ipreg]
    ip += 1
    nr_lines += 1
    
print (reg[0])    
#log.close()

print (time.time()-start)