# -*- coding: utf-8 -*-
"""
Created on Sun Dec 16 07:59:04 2018

@author: steff
"""


def addr(reg,a, b, c):
    reg[c] = reg[a] + reg[b]
    
def addi(reg,a,b,c):
    reg[c] = reg[a] + b

def mulr(reg,a, b, c):
    reg[c] = reg[a] * reg[b]
    
def muli(reg,a,b,c):
    reg[c] = reg[a] * b
    
def banr(reg,a, b, c):
    r = reg[a] & reg[b]
    reg[c] = r
    #print (" - banr({0},{1},{2},{3}) -> [{4}] = {5}&{6} = {7}".format(reg,a,b,c,c,reg[a],reg[b],reg[c]))
    
def bani(reg,a,b,c):
    reg[c] = reg[a] & b
    
def borr(reg,a, b, c):
    reg[c] = reg[a] | reg[b]
    
def bori(reg,a,b,c):
    reg[c] = reg[a] | b
    
def setr(reg,a,b,c):
    reg[c] = reg[a]

def seti(reg,a,b,c):
    reg[c] = a

def gtir(reg,a,b,c):
    if a > reg[b]:
        reg[c] = 1
    else:
        reg[c] = 0

def gtri(reg,a,b,c):
    if reg[a] > b:
        reg[c] = 1
    else:
        reg[c] = 0

def gtrr(reg,a,b,c):
    if reg[a] > reg[b]:
        reg[c] = 1
    else:
        reg[c] = 0

def eqir(reg,a,b,c):
    if a == reg[b]:
        reg[c] = 1
    else:
        reg[c] = 0

def eqri(reg,a,b,c):
    if reg[a] == b:
        reg[c] = 1
    else:
        reg[c] = 0

def eqrr(reg,a,b,c):
    if reg[a] == reg[b]:
        reg[c] = 1
    else:
        reg[c] = 0

opcodes = {}
opcodes["addr"] = addr
opcodes["addi"] = addi
opcodes["mulr"] = mulr
opcodes["muli"] = muli
opcodes["banr"] = banr
opcodes["bani"] = bani
opcodes["borr"] = borr
opcodes["bori"] = bori
opcodes["setr"] = setr
opcodes["seti"] = seti
opcodes["gtir"] = gtir
opcodes["gtri"] = gtri
opcodes["gtrr"] = gtrr
opcodes["eqir"] = eqir
opcodes["eqri"] = eqri
opcodes["eqrr"] = eqrr
