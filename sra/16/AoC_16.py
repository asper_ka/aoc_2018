# -*- coding: utf-8 -*-
"""
Created on Sun Dec 16 08:07:04 2018

@author: steff
"""

from opcodes import *
import re

file = open("samples.txt")
lines = file.read().splitlines()

ireg=[]
codes=[]
oreg=[]

for l in lines:
    m = re.search("After:\s*\[(\d+), (\d+), (\d+), (\d+)\]",l)
    if m:
        oreg.append([int(m.group(i)) for i in range(1,5)])
    m = re.search("Before:\s*\[(\d+), (\d+), (\d+), (\d+)\]",l)
    if m:
        ireg.append([int(m.group(i)) for i in range(1,5)])
    m = re.search("^(\d+) (\d+) (\d+) (\d+)",l)
    if m:
        codes.append([int(m.group(i)) for i in range(1,5)])

nr3 = 0
for idx in range (len(ireg)):
    nr = 0
    #print ("Input: {0}".format(ireg[idx]))
    #print ("Code: {0}".format(codes[idx]))
    for o in opcodes:
        func = opcodes[o]
        inreg = ireg[idx].copy()
        func(inreg, codes[idx][1], codes[idx][2], codes[idx][3] )
        #print ("{0}(reg, a={1},b={2},c={3}) Result: {4}".format(o,codes[idx][1], codes[idx][2], codes[idx][3],inreg))
        if inreg == oreg[idx]:
            #print (" == output ! ({0})".format(oreg[idx]))
            nr+=1
    print (nr)
    #print ("")
    if nr >= 3:
        nr3 += 1

print ("3 times: {0}".format(nr3))
