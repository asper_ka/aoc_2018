# -*- coding: utf-8 -*-
"""
Created on Sun Dec 16 08:07:04 2018

@author: steff
"""

from opcodes import *
import re

file = open("samples.txt")
lines = file.read().splitlines()

ireg=[]
codes=[]
oreg=[]

for l in lines:
    m = re.search("After:\s*\[(\d+), (\d+), (\d+), (\d+)\]",l)
    if m:
        oreg.append([int(m.group(i)) for i in range(1,5)])
    m = re.search("Before:\s*\[(\d+), (\d+), (\d+), (\d+)\]",l)
    if m:
        ireg.append([int(m.group(i)) for i in range(1,5)])
    m = re.search("^(\d+) (\d+) (\d+) (\d+)",l)
    if m:
        codes.append([int(m.group(i)) for i in range(1,5)])

program = []

file = open("program.txt")
lines = file.read().splitlines()
for l in lines:
    m = re.search("^(\d+) (\d+) (\d+) (\d+)",l)
    if m:
        program.append([int(m.group(i)) for i in range(1,5)])


translate = {}
for idx in range (len(ireg)):
    #print ("Input: {0}".format(ireg[idx]))
    #print ("Code: {0}".format(codes[idx]))
    valid_opcode = ""
    for o in opcodes:
        if not o in translate:
            translate[o] = []
        func = opcodes[o]
        inreg = ireg[idx].copy()
        func(inreg, codes[idx][1], codes[idx][2], codes[idx][3] )
        #print ("{0}(reg, a={1},b={2},c={3}) Result: {4}".format(o,codes[idx][1], codes[idx][2], codes[idx][3],inreg))
        if inreg == oreg[idx]:
            #print (" == output ! ({0})".format(oreg[idx]))
            c = codes[idx][0]
            if not c in translate[o]:
                translate[o].append(c)
    #print ("")

# Hier ist jetzt jeder mögliche Opcode gesammelt
# also z.B. translate['addr'] = [0,10,8,2]

#print (translate)

vtable = {}

# jetzt den Eintrag finden, für den es nur einen opcode gibt
# den gefunden opcode bei allen anderen streichen
# suchen ob es jetzt einen anderen Eintrag mit nur einem
# opcode gibt
# ...

for i in range(len(opcodes)):
    for t in translate:
        if len(translate[t]) == 1:
            vtable[translate[t][0]] = t
        
    for c in vtable:
        for t in translate:
            if c in translate[t]:
                translate[t].remove(c) 

#print (vtable)
#print (translate)

reg = [0,0,0,0]

for l in program:
    opcodes[vtable[l[0]]](reg, l[1], l[2], l[3])
    
print (reg[0])