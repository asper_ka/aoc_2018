# -*- coding: utf-8 -*-
"""
Created on Tue Dec  4 21:29:36 2018

@author: SRA
"""
import re
import time
start_time = time.time()

file = open("3.txt")
lines = file.read().splitlines()

#lines = [
#'#1 @ 1,3: 4x4',
#'#2 @ 3,1: 4x4',
#'#3 @ 5,5: 2x2' ]

patch = [ [0 for i in range(1000)] for j in range(1000)]

for l in lines:
    m = re.search(".* @ (.*),(.*): (.*)x(.*)", l)
    x = int(m.group(1))
    y = int(m.group(2))
    w = int(m.group(3))
    h = int(m.group(4))
    for i in range(x, x+w):
        for j in range(y, y+h):
            patch[i][j] += 1


print (sum([len([c for c in row if c > 1]) for row in patch]))

print (time.time() - start_time)
