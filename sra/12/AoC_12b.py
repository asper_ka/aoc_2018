# -*- coding: utf-8 -*-
"""
Created on Wed Dec 12 19:48:05 2018

@author: steff
"""

rules = {}
rules["#.#.#"] = "#"
rules["..#.#"] = "."
rules[".#.##"] = "#"
rules[".##.."] = "."
rules["##..."] = "#"
rules["##..#"] = "#"
rules["#.##."] = "#"
rules[".#..#"] = "#"
rules[".####"] = "."
rules["....#"] = "."
rules["#...."] = "."
rules["#.###"] = "."
rules["###.#"] = "#"
rules[".#.#."] = "."
rules["#...#"] = "."
rules[".#..."] = "#"
rules["##.#."] = "#"
rules["#..##"] = "#"
rules["..##."] = "."
rules["####."] = "#"
rules[".###."] = "."
rules["#####"] = "."
rules["#.#.."] = "."
rules["...#."] = "."
rules["..#.."] = "."
rules["###.."] = "#"
rules["#..#."] = "."
rules[".##.#"] = "."
rules["....."] = "."
rules["##.##"] = "#"
rules["..###"] = "#"
rules["...##"] = "#"
      
init_state = "##.#...#.#.#....###.#.#....##.#...##.##.###..#.##.###..####.#..##..#.##..#.......####.#.#..#....##.#"

offset = 0

def get_next_gen(state):
    s = "...." + state + "...."
    newstate = ""
    for i in range (2, len(s)-2):
        sub = s[i-2: i+3]
        #print (sub)
        if sub in rules:
            newstate += rules[sub]
        else:
            newstate += "."
    return newstate

for g in range (200):
    init_state = get_next_gen(init_state)
    #print (init_state)
    offset -= 2
    idx = init_state.find('#')
    if idx > 0:
        offset += idx
        init_state = init_state[idx:]
    init_state = init_state.strip('.')
    #print (init_state)
    #print("")
    print ("{0} - {1}".format(g,get_sum(offset, init_state)))

def get_sum(of, state):
    pot = of
    sum = 0
    for c in init_state:
        if c == '#':
            sum += pot
        pot += 1
    return sum

