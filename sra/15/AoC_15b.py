# -*- coding: utf-8 -*-
"""
Created on Sat Dec 15 21:29:52 2018

@author: steff
"""
import time

start_time = time.time()

init_grid = """#######   
#.G...#
#...EG#
#.#.#G#
#..G#E#
#.....#   
#######   
"""

init_grid = """#######
#G..#E#
#E#E.E#
#G.##.#
#...#E#
#...E.#
#######
"""

init_grid = """#######
#E..EG#
#.#G.E#
#E.##E#
#G..#.#
#..E#.#
#######
"""

init_grid = """#######
#E.G#.#
#.#G..#
#G.#.G#
#G..#.#
#...E.#
#######
"""

init_grid = """#######
#.E...#
#.#..G#
#.###.#
#E#G#G#
#...#G#
#######
"""

init_grid = """#########
#G......#
#.E.#...#
#..##..G#
#...##..#
#...#...#
#.G...G.#
#.....G.#
#########
"""

file = open("input.txt")
init_grid = file.read()

lines = init_grid.splitlines()
grid = [list(l) for l in init_grid.splitlines()]

        
def in_grid(coord):
    x = coord[0]
    y = coord[1]
    return x>=0 and x < len(grid[0]) and y >= 0 and y < len(grid)

def free_cell(coord):
    if not in_grid(coord):
        return False
    x = coord[0]
    y = coord[1]
    return grid[y][x] == '.'

class Unit:
    def __init__(self, type, x, y,power,  hit):
        self.type = type
        if (type == 'E'):
            self.etype = 'G'
        else:
            self.etype = 'E'
        self.x = x
        self.y = y
        self.attack_power = power
        self.hit_points = hit
    def get_top(self):
        return (self.x, self.y-1)
    def get_bottom(self):
        return (self.x, self.y+1)
    def get_left(self):
        return (self.x-1, self.y)
    def get_right(self):
        return (self.x+1, self.y)
    def __lt__(self,other):
        if self.y == other.y:
            return self.x < other.x
        return self.y < other.y
    
class Node:
    def __init__(self, c, d):
        self.c = c
        self.d = d
        
def min_dist(x,y,c):
    queue = []
    queue.append(Node((x, y), 0))
    visited = [[False for y in range(len(grid))] for x in range (len(grid[0]))]
    
    while queue:
        n = queue.pop(0)
        if n.c == c:
            return n.d
        ctop = (n.c[0], n.c[1]-1)
        if (free_cell(ctop)) and not visited[ctop[0]][ctop[1]]:
            queue.append(Node(ctop, n.d+1))
            visited[ctop[0]][ctop[1]] = True
        cbottom = (n.c[0], n.c[1]+1)
        if (free_cell(cbottom)) and not visited[cbottom[0]][cbottom[1]]:
            queue.append(Node(cbottom, n.d+1))
            visited[cbottom[0]][cbottom[1]] = True
        cleft = (n.c[0]-1, n.c[1])
        if (free_cell(cleft)) and not visited[cleft[0]][cleft[1]]:
            queue.append(Node(cleft, n.d+1))
            visited[cleft[0]][cleft[1]] = True
        cright = (n.c[0]+1, n.c[1])
        if (free_cell(cright)) and not visited[cright[0]][cright[1]]:
            queue.append(Node(cright, n.d+1))
            visited[cright[0]][cright[1]] = True
    return -1

def coord_cmp_key (a):
    return a[0] + 1e6*a[1]

def create_units(grid, elf_power):
    units = []
    for y in range(len(grid)):
        for x in range (len(grid[y])):
            c = grid[y][x]
            if c=='E':
                units.append(Unit('E',x,y,elf_power,200))
            elif c=='G':
                units.append(Unit('G',x,y,3,200))
    return units

def get_unit_by_coord(c,units):
    for u in units:
        if (u.x, u.y) == c:
            return u
    return None

def get_enemy_inrange(unit, units):
    min_hit = 1e6
    mint = None
    c = unit.get_top()
    if grid[c[1]][c[0]]==unit.etype:
        t = get_unit_by_coord(c, units)
        if t and t.hit_points<min_hit:
            mint = t
            min_hit=t.hit_points
    c = unit.get_left()
    if grid[c[1]][c[0]]==unit.etype:
        t = get_unit_by_coord(c, units)
        if t and t.hit_points<min_hit:
            mint = t
            min_hit=t.hit_points
    c = unit.get_right()
    if grid[c[1]][c[0]]==unit.etype:
        t = get_unit_by_coord(c, units)
        if t and t.hit_points<min_hit:
            mint = t
            min_hit=t.hit_points
    c = unit.get_bottom()
    if grid[c[1]][c[0]]==unit.etype:
        t = get_unit_by_coord(c, units)
        if t and t.hit_points<min_hit:
            mint = t
            min_hit=t.hit_points
    return mint

def print_grid(g, units):
    y = 0
    for row in g:
        for c in row:
            print (c,end="")
        for x in range(len(row)):
            u = get_unit_by_coord((x,y), units)
            if u:
                print ("  {0}({1})".format(u.type,u.hit_points), end="")
        print("")
        y+=1


def find_units(units):
    new_units = []
    for y in range(len(grid)):
        for x in range (len(grid[y])):
            u = get_unit_by_coord((x,y), units)
            if u and u.hit_points > 0:
                new_units.append(u)
    return new_units

def has_targets(unit, units):
    for u in units:
        if u.type != unit.type and u.hit_points >0:
            return True
    return False

def move(unit, units):
    ranges = []
    for u in units:
        if u.type != unit.type:
            testc = []
            testc.append(u.get_top())
            testc.append(u.get_bottom())
            testc.append(u.get_left())
            testc.append(u.get_right())
            for c in testc:
                if free_cell(c):
                    if c not in ranges:
                        ranges.append(c)
    
    distances = {}
    min_d = 1e10
    for c in ranges:
        d = min_dist(unit.x, unit.y, c)
        if d>=0:
            distances[c] = d
            if d < min_d:
                min_d = d
    if len(distances)==0:
        return False
    
    # alle anderen löschen
    di = {key:val for key, val in distances.items() if val == min_d}
    # erster in "Lesereihenfolge"
    target = sorted(list(di.keys()),key=coord_cmp_key)[0]
    
    tests = []
    tests.append(unit.get_top())
    tests.append(unit.get_bottom())
    tests.append(unit.get_left())
    tests.append(unit.get_right())
    distances = {}
    min_d = 1e10
    for s in tests:
        if free_cell(s):
            d = min_dist(s[0], s[1], target)
            if d>=0:
                distances[s] = d
                if d < min_d:
                    min_d = d
    
    di = {key:val for key, val in distances.items() if val == min_d}
    # erster in "Lesereihenfolge"
    step = sorted(list(di.keys()),key=coord_cmp_key)[0]
    grid[unit.y][unit.x] = '.'
    grid[step[1]][step[0]] = unit.type
    unit.x = step[0]
    unit.y = step[1]
    return True
    
def unit_turn(unit, units):
    e = get_enemy_inrange(unit, units)
    if not e:
        move(unit, units)
    e = get_enemy_inrange(unit, units)
    if e:
        e.hit_points -= unit.attack_power
        #print ("{0}{1} attacks {2}{3} -> {4}".format(unit.type, (unit.x,unit.y),e.type,(e.x,e.y), e.hit_points))
        if e.hit_points <=0:
            grid[e.y][e.x] = '.'
            units.remove(e)

#print_grid (grid)

def battle(units):
    i = 0
    finished = False
    while not finished:
        for u in units:
            if u.hit_points > 0:
                if not has_targets(u, units):
                    finished = True
                    return i
                else:
                    unit_turn(u, units)
        units = find_units(units)
        i += 1
        #print ("")
        #print(i)
        #print_grid (grid)

units = create_units(grid, 3)
final_round = battle(units)    
hit_points = 0
for u in units:
    hit_points += u.hit_points

print ("round {0} with {1} hit_points => {2}".format(final_round, hit_points, final_round*hit_points))

print (time.time()-start_time)