# -*- coding: utf-8 -*-
"""
Created on Wed Dec  5 23:58:41 2018

@author: SRA
"""

import re
import string

file = open("7.txt")
lines = file.read().splitlines()
#log = open("log.txt", "w")

class Step:
    def __init__(self,name):
        self.name = name
        self.parents = []
        self.children = []
        
class Worker:
    def __init__(self):
        self.step = ""
        self.remainingTime = 0

def createSteps (lines):
    steps = {}
    for l in lines:
        m = re.search("Step (.) .*step (.) ", l)
        if m:
            pname = m.group(1)
            cname = m.group(2)
            if not pname in steps:
                steps[pname] = Step(pname)
            if not cname in steps:
                steps[cname] = Step(cname)
            steps[pname].children.append(cname)
            steps[cname].parents.append(pname)
                
    return steps

def getReadyNames(steps):
    readyStepNames = []
    for s in steps:
        if len(steps[s].parents)==0:
            readyStepNames.append(steps[s].name)
    return sorted(readyStepNames)

def executeStep(name, steps):
    if name in steps:
        s = steps[name]
        for child in s.children:
            steps[child].parents.remove(name)
        del steps[name]
    
allsteps = createSteps(lines)

time = 0
workers = []
for i in range (5):
    workers.append(Worker())
    
readyNames = getReadyNames(allsteps)
workingNames = []

def getFreeName(rNames, wNames):
    for n in rNames:
        if wNames.count(n) == 0:
            return n
    return ""

while len(allsteps)>0:
    for w in workers:
        if w.remainingTime > 0:
            w.remainingTime -=1
        if w.remainingTime == 0:
            if w.step != "":
                #log.write("finished working on "+w.step)
                executeStep(w.step, allsteps)
                w.step=""
                readyNames = getReadyNames(allsteps)
                #log.write(" - jobs ready: "+str(readyNames)+"\n")
            n = getFreeName(readyNames, workingNames)
            if n != "":
                w.step = n
                w.remainingTime = string.ascii_uppercase.index(n)+61
                workingNames.append(n)
                #log.write ("names: " + str(readyNames) + "  workingNames: "+str(workingNames) + "\n")
    # log.write (str(time) + "  -   1:" 
    #        + workers[0].step + "   -   2:"
    #        + workers[1].step + "   -   3:"
    #        + workers[2].step + "   -   4:"
    #        + workers[3].step + "   -   5:"
    #        + workers[4].step + "\n"
    #        )
    time += 1
    