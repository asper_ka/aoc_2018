# -*- coding: utf-8 -*-
"""
Created on Wed Dec  5 23:58:41 2018

@author: SRA
"""

import re

file = open("7.txt")
lines = file.read().splitlines()

class Step:
    def __init__(self,name):
        self.name = name
        self.parents = []
        self.children = []

def createSteps (lines):
    steps = {}
    for l in lines:
        m = re.search("Step (.) .*step (.) ", l)
        if m:
            pname = m.group(1)
            cname = m.group(2)
            if not pname in steps:
                steps[pname] = Step(pname)
            if not cname in steps:
                steps[cname] = Step(cname)
            steps[pname].children.append(cname)
            steps[cname].parents.append(pname)
                
    return steps

def getReadyNames(steps):
    readyStepNames = []
    for s in steps:
        if len(steps[s].parents)==0:
            readyStepNames.append(steps[s].name)
    return sorted(readyStepNames)

def executeStep(name, steps):
    s = steps[name]
    for child in s.children:
        steps[child].parents.remove(name)
    del steps[name]
    
allsteps = createSteps(lines)
instruction = ""

while len(allsteps)>0:
    names = getReadyNames(allsteps)
    instruction += names[0]
    executeStep(names[0], allsteps)
    
print (instruction)

    