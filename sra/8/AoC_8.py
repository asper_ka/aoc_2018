# -*- coding: utf-8 -*-
"""
Created on Wed Dec  5 23:58:41 2018

@author: SRA
"""

#input = "aabAAB"

import time
start = time.time()

file = open("8.txt")

def words(fileobj):
    for line in fileobj:
        for word in line.split():
            yield word

wordgen = words(file)



class Node:
    next_id = 0
    def __init__ (self):
        self.id = self.next_id
        self.next_id += 1
        self.children = []
        self.metadata = []
    
    def create(self, w):
        nrChildren = int(next(w))
        nrMetadata = int(next(w))
        for _i in range (nrChildren):
            n = Node()
            n.create(w)
            self.children.append(n)
        for _i in range (nrMetadata):
            self.metadata.append(int(next(w)))

    def output(self, prefix):
        print (prefix + "Node {0}, children: {1}, metadata: ".format(self.id, len(self.children)), end="")
        print (str(self.metadata) + " - sum: " + str(sum(self.metadata)))
        for c in self.children:
            c.output (prefix + "  ")
    
    def sum_metadata (self):
        s = sum(self.metadata)
        for c in self.children:
            s += c.sum_metadata()
        return s
        
root = Node()
root.create(wordgen)    

root.output("")
print (root.sum_metadata())


print (time.time()-start)