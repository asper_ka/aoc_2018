# -*- coding: utf-8 -*-

import time

text="""+7
+7
-2
-7
-4"""

text = open("input.txt").read()

start_time = time.time()

sum = 0
freq = {}
searching = True
while searching:
    for l in text.split():
        sum += int(l)
        if sum in freq:
            print (sum)
            searching = False
            break
        freq[sum] = 1

print (time.time() - start_time)