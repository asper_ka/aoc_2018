# -*- coding: utf-8 -*-
"""
Created on Sun Dec  9 07:29:31 2018

@author: steff
"""

score = {}
current_player = 2

marbles = [0, 1]
current_index = 1

nr_players = 470
nr_marbles = 7217000

#nr_players = 30
#nr_marbles = 5807

for p in range (1, nr_players+1):
    score[p] = 0

class Marble:
    def __init__(self, nr):
        self.next = self
        self.prev = self
        self.nr = nr

current_marble = Marble(0)

first_marble = current_marble

def print_marbles():
    c = first_marble
    while True:
        if c == current_marble:
            print ("_{0}_ ".format(c.nr), end="")
        else:
            print (" {0}  ".format(c.nr), end="")
        c = c.next
        if c == first_marble:
            break
    print("")

for nr in range (1, nr_marbles+1):
    if nr % 23 == 0:
        s = nr
        m = current_marble
        for i in range(7):
            m = m.prev
        s += m.nr
        score[current_player] += s
        current_marble = m.next
        current_marble.prev = m.prev
        m.prev.next = current_marble
        #print ("player {0} scores {1}".format(current_player, s))

    else:
        marble1 = current_marble.next
        marble2 = marble1.next
        m = Marble(nr)
        marble1.next = m
        marble2.prev = m
        m.prev = marble1
        m.next = marble2
        current_marble = m
    
    current_player += 1
    if (current_player > nr_players):
        current_player = 1
    #print_marbles()
             
high_score = 0
for s in score:
    if score[s] > high_score:
        high_score = score[s]

print (high_score)