# -*- coding: utf-8 -*-
"""
Created on Sun Dec  9 07:29:31 2018

@author: steff
"""

score = {}
current_player = 2

marbles = [0, 1]
current_index = 1

nr_players = 470
nr_marbles = 72170

#nr_players = 13
#nr_marbles = 7999

for p in range (1, nr_players+1):
    score[p] = 0

for nr in range (2, nr_marbles+1):
    if nr % 23 == 0:
        s = nr
        current_index -= 7
        if (current_index < 0):
            current_index = len(marbles)+current_index
        s += marbles[current_index]
        score[current_player] += s
        #print ("player {0} scores {1} at {2}".format(current_player, s, current_index))
        del marbles[current_index]
    else:
        new_index = current_index + 2
        if new_index > len(marbles):
            new_index = new_index % len(marbles)
        marbles.insert(new_index, nr)
        #print ("player {0} adds {1} at {2}".format(current_player, nr, new_index))
        current_index = new_index
    current_player += 1
    if (current_player > nr_players):
        current_player = 1
    #print (str(marbles) + " - current {0} at {1}"
    #       .format(marbles[current_index], current_index))
    if nr % 1000 == 0:
        print ("{0} ...".format(nr))
        
high_score = 0
for s in score:
    if score[s] > high_score:
        high_score = score[s]

print (high_score)