# -*- coding: utf-8 -*-
"""
Created on Tue Dec 18 06:27:33 2018

@author: steff
"""

file = open("18.txt")
grid = [list(l) for l in file.read().splitlines()]

directions = [(-1,-1), (0,-1), (1,-1), (-1,0), (1,0), (-1,1), (0,1), (1,1)]

def in_grid(g,x,y):
    return x>=0 and x<len(g[0]) and y>=0 and y<len(g)

def get_neighbors(g, x,y):
    n = []
    for d in directions:
        nx = x+d[0]
        ny = y+d[1]
        if in_grid(g,nx,ny):
            #print ("test {0}".format((nx,ny)),end="")
            n.append(g[ny][nx])
    #print ("")
    return n
        
def print_grid(g):
    y = 0
    nr_wood = 0
    nr_lumber = 0
    for row in g:
        for c in row:
            #print (c,end="")
            if c=='|':
                nr_wood += 1
            if c=='#':
                nr_lumber += 1
        #print("")
        y+=1
    print ("{0} * {1} = {2}".format(nr_wood, nr_lumber, nr_wood*nr_lumber))

def get_next_gen(g):
    ng = []
    y = 0
    for row in g:
        ng.append(row.copy())
        x = 0
        for c in row:
            ng[y][x] = c
            n = get_neighbors(g,x,y)
            #print ("{2}: {0} and {1} -> ".format(c,n,(x,y)),end="")
            if c=='.':
                if n.count('|')>=3:
                    ng[y][x] = '|'
            elif c=='|':
                if n.count('#')>=3:
                    ng[y][x] = '#'
            elif c=='#':
                if n.count('|')<1 or n.count('#')<1:
                    ng[y][x] = '.'
            #print (ng[y][x])
            x+=1
        y+=1
    return ng

for i in range(1000):
    grid = get_next_gen(grid)
    print ("{0} - ".format(i+1), end="")
    print_grid(grid)    
            