# -*- coding: utf-8 -*-
"""
Created on Tue Dec 25 07:14:28 2018

@author: Steffen
"""

lines = open("input.txt").read().splitlines()

class Position():
    def __init__(self,x,y,z,t):
        self.x = x
        self.y = y
        self.z = z
        self.t = t

    def distance(self,c):
        return abs(self.x-c.x)  \
               +abs(self.y-c.y) \
               +abs(self.z-c.z) \
               +abs(self.t-c.t)
    
    def __str__(self):
        return "<{0},{1},{2},{3}>".format(self.x, self.y, self.z, self.t)

def parse_input(lines):        
    pos = []
    for l in lines:
        p = l.split(',')
        pos.append(Position(int(p[0]),int(p[1]),int(p[2]),int(p[3])))
    return pos

def find_neighbors(pos):    
    neighbors = {}
    for p1 in pos:
        neighbors[p1] = []
        for p2 in pos:
            if p1 != p2 and p1.distance(p2)<=3:
                neighbors[p1].append(p2)
    return neighbors

positions = parse_input(lines)
neigbors = find_neighbors(positions)

def add_neighbors_to_cluster(cluster,pos,neighbors,in_cluster):
    for p in neighbors[pos]:
        if not p in in_cluster:
            cluster.append(p)
            in_cluster.append(p)
            add_neighbors_to_cluster(cluster,p,neigbors,in_cluster)
    
in_cluster = []
clusters = []
for p in positions:
    if not p in in_cluster:
        cluster = []
        clusters.append(cluster)
        in_cluster.append(p)
        cluster.append(p)
        add_neighbors_to_cluster(cluster,p,neigbors,in_cluster)
        
print("{0} clusters:".format(len(clusters)))
#for c in clusters:
#    print ("  - ",end="")
#    for p in c:
#        print ("{0}, ".format(p),end="")
#    print("")
        


            

            
