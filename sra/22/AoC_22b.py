# -*- coding: utf-8 -*-
"""
Created on Sat Dec 22 13:46:20 2018

@author: steff
"""

from AoC_22 import *
from enum import Enum
import time
start = time.time()

class Gear(Enum):
    neither = 0
    climbing = 1
    torch = 2
    def __str__(self):
        if self == Gear.neither: return 'N'
        elif self == Gear.climbing: return 'C'
        elif self == Gear.torch: return 'T'
        else: return '?'

    def is_compatible_to_type(self,reg_type):
        if reg_type == Type.rocky:
            return self == Gear.climbing or self == Gear.torch
        elif reg_type == Type.wet:
            return self == Gear.climbing or self == Gear.neither
        elif reg_type == Type.narrow:
            return self == Gear.neither or self == Gear.torch
    
class Dir(Enum):
    north = (0,-1)
    east = (1,0)
    west = (-1,0)
    south = (0,1)
    def add_pos(self, pos):
        return (self.value[0]+pos[0], self.value[1]+pos[1])

def create_nodes(target, tgrid):
    nodes = []
    for y in range(len(tgrid[0])):
        for x in range(len(tgrid)):
            if (x,y) == target:
                nodes.append((x,y,Gear.torch))
            else:
                for g in Gear:
                    if g.is_compatible_to_type(tgrid[x][y]):
                        nodes.append((x,y,g))
    return set(nodes)

def create_distances(target, tgrid):
    distances = {}
    for y in range(target[1]+1):
        for x in range(target[0]+1):
            for g in Gear:
                if g.is_compatible_to_type(tgrid[x][y]):
                    distances[(x,y,g)] = 1e20
    return distances


def get_min_node_from_set(s,ds):
    min_node = None
    min_dist = 1e30
    for n in s:
        if ds[n] < min_dist:
            min_dist = ds[n]
            min_node = n
    return min_node

def get_neighbors(x,y,tg):
    neighbors = []
    for d in Dir:
        (nx,ny) = d.add_pos((x,y))
        if nx >=0 and ny >=0 and nx<len(tg) and ny < len(tg[0]):
            for g in Gear:
                if g.is_compatible_to_type(tg[nx][ny]):
                    neighbors.append((nx,ny,g))
    return neighbors


max_x = int(target[0]+100)
max_y = int(target[1]+100)
type_grid = create_type_grid ((max_x, max_y))
distances = create_distances((max_x, max_y), type_grid)


# https://de.wikipedia.org/wiki/Dijkstra-Algorithmus

#nodes = create_nodes(target, type_grid)
distances[(0,0,Gear.torch)] = 0
prev = {}

min_dist =1e20
final = None
nr = 0
#while len(nodes)>0:# and nr < 110000:
while len(distances)>0:# and nr < 110000:
    #n = get_min_node_from_set(nodes, distances)
    n = min(distances, key=distances.get)
    (x,y,g) = n
    nbs = get_neighbors(x,y, type_grid)
    dist = distances[(x,y,g)]
    del distances[n]
    if nr%1000 == 0:
        print ("current node: {0}/{1} - dist={2}, nodes left:{3}".format((x,y),g, dist, len(distances
)))
    if (x,y) == target:
        print ("found target: {0}/{1} - dist={2}".format((x,y),g, dist))
        if dist < min_dist:
            min_dist = dist
            final = (x,y,g)
    for nb in nbs:
        if nb in distances:
            (nx,ny,ng) = nb
            if ng.is_compatible_to_type(type_grid[x][y]):
                ndist = dist+1
                if ng!=g: ndist+=7
                if distances[nb]>ndist:
                    #print ("dist for neighbor {0} = {1}".format(n,ndist))
                    distances[nb] = ndist
                    prev[nb] = (x,y,g)
    nr += 1

path = {}
if final:
    print ("{0} - {1}".format(final, min_dist))
    p = prev[final]
    while (p[0],p[1]) != (0,0):
        path[(p[0],p[1])] = str(p[2])
        p = prev[p]

print (time.time()-start)