# -*- coding: utf-8 -*-
"""
Created on Sat Dec 22 12:18:00 2018

@author: steff
"""

from enum import Enum

system_depth = 510
target = (10,10)

system_depth = 11394
target = (7,701)

class Type(Enum):
    rocky = 0
    wet = 1
    narrow = 2
    
    def __str__(self):
        if self == Type.rocky: return '.'
        elif self == Type.wet: return '='
        elif self == Type.narrow: return '|'
        else: return '?'

def get_geo_index(grid_erosion, coord):
    if coord == (0,0): return 0
    if coord == target: return 0
    (x,y) = coord
    if y == 0: return x * 16807
    elif x == 0: return y * 48271
    else: return grid_erosion[x-1][y] * grid_erosion[x][y-1]
    
def get_type(erosion_level):
    return Type(erosion_level % 3)

def get_erosion_level(geo_index, depth):
    return (geo_index+depth)%20183

def print_type_grid(grid, specials={}):
    for y in range (len(grid[0])):
        line = ''
        for x in range (len(grid)):
            if (x,y) in specials:
                line += specials[(x,y)]
            else:
                line += str(grid[x][y])
        print (line)

def get_risk_level(grid,target):
    level = 0
    for x in range (target[0]+1):
        level += sum([i.value for i in grid[x][0:target[1]+1]])
    return level

def create_type_grid(target):
    type_grid = [ [-1 for y in range (target[1]+1)] for x in range(target[0]+1)]
    erosion_grid = [ [-1 for y in range (target[1]+1)] for x in range(target[0]+1)]
    for y in range(target[1]+1):
        for x in range(target[0]+1):
            erosion_grid[x][y] = get_erosion_level(
                    get_geo_index(erosion_grid, (x,y)), system_depth)
            type_grid[x][y] = get_type(erosion_grid[x][y])
    return type_grid

#print (get_risk_level(create_type_grid(target), target))
