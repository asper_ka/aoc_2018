# -*- coding: utf-8 -*-
"""
Created on Sat Dec 22 13:46:20 2018

@author: steff
"""

from AoC_22 import *
from enum import Enum
import networkx as nx

class Gear(Enum):
    neither = 0
    climbing = 1
    torch = 2
    def __str__(self):
        if self == Gear.neither: return 'N'
        elif self == Gear.climbing: return 'C'
        elif self == Gear.torch: return 'T'
        else: return '?'

    def is_compatible_to_type(self,reg_type):
        if reg_type == Type.rocky:
            return self == Gear.climbing or self == Gear.torch
        elif reg_type == Type.wet:
            return self == Gear.climbing or self == Gear.neither
        elif reg_type == Type.narrow:
            return self == Gear.neither or self == Gear.torch
    
valid_gears = {Type.rocky:[Gear.climbing, Gear.torch],\
               Type.wet:[Gear.climbing,Gear.neither],\
               Type.narrow:[Gear.neither,Gear.torch]}    

class Dir(Enum):
    north = (0,-1)
    east = (1,0)
    west = (-1,0)
    south = (0,1)
    def add_pos(self, pos):
        return (self.value[0]+pos[0], self.value[1]+pos[1])

def create_distances(target):
    distances = {}
    for y in range(target[1]+1):
        for x in range(target[0]+1):
            for g in Gear:
                distances[(x,y,g)] = 1e20
    return distances

def get_neighbors(x,y,tg):
    neighbors = []
    for d in Dir:
        (nx,ny) = d.add_pos((x,y))
        if nx >=0 and ny >=0 and nx<len(tg) and ny < len(tg[0]):
            for g in Gear:
                if g.is_compatible_to_type(tg[nx][ny]):
                    neighbors.append((nx,ny,g))
    return neighbors

def create_graph (max_x, max_y, tgrid):
    graph = nx.Graph()
    for x in range(max_x+1):
        for y in range(max_y+1):
            vg = valid_gears[tgrid[x][y]]
            graph.add_edge((x,y,vg[0]),(x,y,vg[1]),weight=7)
            nb = get_neighbors(x,y,tgrid)
            for (nbx,nby,nbg) in nb:
                if nbg in vg:
                    graph.add_edge((x,y,nbg),(nbx,nby,nbg),weight=1)
                    #print ("add_edge({0}, {1}, {2}) ({3} edges)".format((x,y,nbg),(nbx,nby,nbg),1,len(graph.edges)))
    return graph

max_x = int(target[0]+100)
max_y = int(target[1]+100)
type_grid = create_type_grid ((max_x, max_y))
distances = create_distances((max_x, max_y))

graph = create_graph(max_x, max_y, type_grid)
print (nx.dijkstra_path_length(graph, (0,0,Gear.torch), (target[0],target[1],Gear.torch)))
path = nx.dijkstra_path(graph, (0,0,Gear.torch), (target[0],target[1],Gear.torch))
print ("{0} steps".format(len(path)))
#print (path)
print_path = {}
for p in path:
    print_path[(p[0],p[1])] = str(p[2])

print_path[(0,0)] = 'M'
print_path[(target[0],target[1])] = 'T'

#print_type_grid(type_grid, print_path)
                            
            

