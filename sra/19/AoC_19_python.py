# -*- coding: utf-8 -*-
"""
Created on Wed Dec 19 19:51:59 2018

@author: steff
"""


def sum_teiler_1(r4):
    r0 = 0
    for r1 in range (1,r4+1):
        for r3 in range (1,r4+1):
            r5 = r1*r3
            #print ("{0}*{1}={2}".format(r1,r3,r5), end="")
            if r5==r4:
                r0 += r1
                #print ("   +")
            #else:
                #print ("")
    return r0

print (sum_teiler_1(987))

def sum_teiler_2(r4):
    r0 = 0
    for r1 in range (1,r4+1):
        if r4 % r1 == 0:
            r0 += r1
    return r0

print (sum_teiler_2(987))

print (sum_teiler_2(10551387))