# -*- coding: utf-8 -*-
"""
Created on Sun Dec 16 08:07:04 2018

@author: steff
"""

from opcodes import *
import re

ipreg = 0
file = open("test_program.txt")

ipreg = 2
file = open("program.txt")

code = file.read().splitlines()

def execute_line(l,r):
    tkns = l.split()
    opcodes[tkns[0]](r, int(tkns[1]), int(tkns[2]), int(tkns[3]))

reg = [1,0,0,0,0,0]

ip = 0
while ip >=0 and ip < len(code):
    reg[ipreg] = ip
    line = code[ip]
    #print ("ip {0} {1} {2} ".format(ip, reg, line),end="")
    execute_line(line,reg)
    #print (reg)
    ip = reg[ipreg]
    ip += 1
    
print (reg[0])    

