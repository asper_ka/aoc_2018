# -*- coding: utf-8 -*-

class LinkedListNode:
    def __init__(self,data=None):
        self.data = data
        self.next = None
        self.prev = None

class LinkedList:
    def __init__(self):
        self.start = None
        self.end = None

    def append_node(self,data):
        if not self.start:
            self.start = LinkedListNode(data)
            self.end = self.start
        else:
            self.end.next = LinkedListNode(data)
            self.end.next.prev = self.end
            self.end = self.end.next

    def remove_node(self,node):
        if node == self.start:
            self.start = self.start.next
            if self.start:
                self.start.prev = None
        if node.prev:
            node.prev.next = node.next
        if node.next:
            node.next.prev = node.prev
        if node.prev:
            return node.prev
        else:
            return node.next

    def get_length(self):
        if not self.start:
            return 0
        current = self.start
        nr = 1
        while current.next:
            nr+=1
            current = current.next
        return nr

