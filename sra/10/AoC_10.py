# -*- coding: utf-8 -*-
"""
Created on Wed Dec  5 23:58:41 2018

@author: SRA
"""

import re

file = open("10.txt")
lines = file.read().splitlines()

class Point:
    def __init__(self,x,y,vx,vy):
        self.x = x
        self.y = y
        self.vx = vx
        self.vy = vy
    def get_position(self, time):
        return (self.x + time*self.vx, self.y + time*self.vy)

def create_points (lines):
    points = []
    for l in lines:
        m = re.search(".*\<(.*),(.*)\>.*\<(.*),(.*)\>", l)
        if m:
            x = int(m.group(1))
            y = int(m.group(2))
            vx = int(m.group(3))
            vy = int(m.group(4))
            points.append(Point(x,y,vx,vy))
                
    return points

def project(time, points):
    coords = []
    for p in points:
        coords.append(p.get_position(time))
    return coords
    
def get_minmax(coords):
    cminx = 1e10
    cmaxx = 1e-10
    cminy = 1e10
    cmaxy = 1e-10
    for c in coords:
        if c[0] < cminx:
            cminx = c[0]
        if c[1] < cminy:
            cminy = c[1]
        if c[0] > cmaxx:
            cmaxx = c[0]
        if c[1] > cmaxy:
            cmaxy = c[1]

    return (cminx, cmaxx, cminy, cmaxy)

def print_field(coords):
    (cminx, cmaxx, cminy, cmaxy) = get_minmax(coords)
    field = [ [ 0 for x in range (cminx, cmaxx+1)] for y in range (cminy, cmaxy+1)]
    for c in coords:
        x = c[0]-cminx
        y = c[1]-cminy
        field[y][x] = 1
    
    for line in field:
        for c in line:
            if c == 0:
                print (". ", end="")
            else:
                print ("# ", end="")
        print("")

points = create_points(lines)
tmin = 0
dminx = 1e10
for t in range(12000):
    c = project(t, points)
    (cminx, cmaxx, cminy, cmaxy) = get_minmax(c)
    dx = cmaxx - cminx
    if dx < dminx:
        tmin = t
        dminx = dx

c = project(tmin, points)
print_field(c)
print (tmin)

# ERKECKJJ