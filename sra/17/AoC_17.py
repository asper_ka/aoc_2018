# -*- coding: utf-8 -*-
"""
Created on Tue Dec 18 16:43:34 2018

@author: steff
"""

import re

lines = open("test.txt").read().splitlines()
lines = open("test2.txt").read().splitlines()
lines = open("input.txt").read().splitlines()

ranges = {'min':{'x':1e10, 'y':1e10}, 'max':{'x':-1e10, 'y':-1e10}}

def check_min(d, val):
    if val < ranges['min'][d]:
        ranges['min'][d] = val
def check_max(d, val):
    if val > ranges['max'][d]:
        ranges['max'][d] = val
        
def print_grid(g):
    for y in range(len(g[0])):
        for x in range(len(g)):
            print (g[x][y],end="")
        print("")

def write_grid(g,ranges,filename):
    f = open(filename, "w")
    for y in range(ranges['min']['y'], ranges['max']['y']+1):
        l = "{:4d}:  ".format(y)
        for x in range(len(g)):
            if g[x][y] == '.':
                l += ' '
            else:
                l += g[x][y]
        f.write(l+"\n")
    f.close()

for l in lines:
    m = re.search("(.)=(\d+), (.)=(\d+)\.\.(\d+)",l)
    if m:
        check_min(m.group(1), int(m.group(2)))
        check_max(m.group(1), int(m.group(2)))
        check_min(m.group(3), int(m.group(4)))
        check_max(m.group(3), int(m.group(5)))
        #print ("{0}={1}, {2}={3}..{4}".format(m.group(1),m.group(2),m.group(3),m.group(4),m.group(5)))
        #print (ranges)

x_offset = ranges['min']['x']-1
y_offset = ranges['min']['y']-1
x_range = 3+ranges['max']['x']-x_offset
y_range = 2+ranges['max']['y']-y_offset

# source must be in grid!
while y_offset > 0:
    y_offset -= 1
    y_range += 1

grid = [['.' for y in range (y_range+1)] for x in range(x_range+1)]

def get_cell(n):
    return grid[n[0]-x_offset][n[1]-y_offset]
def set_cell(n,c):
    #print ("set_cell({0}, '{1}')".format(n,c))
    grid[n[0]-x_offset][n[1]-y_offset] = c
def go_down(n):
    return (n[0], n[1]+1)    
def go_left(n):
    return (n[0]-1, n[1])    
def go_right(n):
    return (n[0]+1, n[1])    


for l in lines:
    m = re.search("(.)=(\d+), (.)=(\d+)\.\.(\d+)",l)
    if m:
        start = int(m.group(4))
        stop = int(m.group(5))
        if m.group(1) == 'x':
            for y in range (start, stop+1):
                grid[int(m.group(2))-x_offset][y-y_offset] = '#'
        elif m.group(1) == 'y':
            for x in range (start, stop+1):
                grid[x-x_offset][int(m.group(2))-y_offset] = '#'

grid[500-x_offset][0-y_offset] = '+'

can_go_down = [(500,0)]

runs = 0
path = []
while len(can_go_down)>0:
    n = can_go_down.pop()
    path.append(n)
    #write_grid(grid, ranges, "grids/grid_{:06d}.txt".format(runs))
    runs += 1
    while get_cell(go_down(n)) == '.' and n[1]<=ranges['max']['y']:
        n = go_down(n)
        path.append(n)
        set_cell(n,'|')
        if get_cell(go_down(n)) != '.' and get_cell(go_down(n)) != '|':
            found_down = False
            while not found_down and len(path)>0:
                n = path[-1]
                del path[-1]
                if get_cell(go_down(n)) != '|':
                    nl = n
                    found_ld = False
                    while not found_ld and get_cell(go_left(nl)) == '.':
                        nl = go_left(nl)
                        set_cell(nl,'|')
                        if get_cell(go_down(nl)) == '.':
                            can_go_down.append(nl)
                            found_ld = True
                        
                    nr = n
                    found_lr = False
                    while not found_lr and get_cell(go_right(nr)) == '.':
                        nr = go_right(nr)
                        set_cell(nr,'|')
                        if get_cell(go_down(nr)) == '.':
                            can_go_down.append(nr)
                            found_lr = True
                    found_down = found_ld or found_lr
                    if not found_down:
                        set_cell(n,'~')
                        nl = n
                        while get_cell(go_left(nl)) == '|':
                            nl = go_left(nl)
                            set_cell(nl,'~')
                        nr = n
                        while get_cell(go_right(nr)) == '|':
                            nr = go_right(nr)
                            set_cell(nr,'~')
                        
write_grid(grid, ranges, "grids/grid_{:06d}.txt".format(runs))

nr_flow = 0
nr_settled = 0
for y in range(ranges['min']['y'], ranges['max']['y']+1):
    for x in range(len(grid)):
        if grid[x][y-y_offset] == '|':
            nr_flow += 1
        if grid[x][y-y_offset] == '~':
            nr_settled += 1
print ("flowing: {0}, settled: {1} => total: {2}".format(nr_flow, nr_settled, nr_flow+nr_settled))
