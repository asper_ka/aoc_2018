# -*- coding: utf-8 -*-
import json
from datetime import datetime
scores = json.load(open("../454363.json"))

members = scores['members']

events = {}
scores = {}
names = []
for m in members:
    data = members[m]
    name = data['name']
    names.append(name)
    scores[name] = 0
    completions = data['completion_day_level']
    for day in completions:
        parts = completions[day]
        for p in parts:
            time = int(parts[p]['get_star_ts'])
            #print ("{0} solves day {1},part {2} at ts {3}".format(name,day,p,time))
            events[time] = (name,int(day),int(p))

def test_win(n,points,tasks_solved,scores):
    pts = scores[n]
    for day in tasks_solved[n]:
        for p in tasks_solved[n][day]:
            if not tasks_solved[n][day][p]:
                pts += points[day][p]
    pts_steffen = scores['Steffen Rau']
    for day in tasks_solved['Steffen Rau']:
        for p in tasks_solved['Steffen Rau'][day]:
            if not tasks_solved['Steffen Rau'][day][p]:
                pts_steffen += 1
    return pts,pts_steffen
    

points = {key:{1:4,2:4} for key in range(1,26)}
tasks_solved = {name:{key:{1:False,2:False} for key in range(1,26)} for name in names}
for ts,event in sorted(events.items()):
    (name,day,p) = event
    win = 0
    if day != 6:
        win = points[day][p]
        scores[name] += win
        points[day][p] -= 1
    print(ts,end="")
    print(datetime.utcfromtimestamp(ts).strftime(' %Y-%m-%d %H:%M:%S'),end="")
    print (" {0} solves day {1},part {2} and gets {3} points, now {4}".format(name,day,p,win,scores[name]))
    tasks_solved[name][day][p] = True
    for n in names:
        if n != "Steffen Rau":
            pts, pts_steffen = test_win(n,points,tasks_solved,scores)
            if pts < pts_steffen:
                print ("  {0} cannot win against Steffen (max possible = {1}, min.points Steffen = {2})".format(n, pts, pts_steffen))


print (scores)