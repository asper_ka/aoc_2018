# -*- coding: utf-8 -*-
"""
Created on Thu Dec 13 06:27:48 2018

@author: steff
"""

from enum import Enum

file = open("13b_test.txt")
file = open("13.txt")
lines = file.read().splitlines()

grid = []
carts = []

class Direction(Enum):
    up = 1
    right = 2
    down = 3
    left = 4
    
dir_c = {}
dir_c[Direction.up] = '^'
dir_c[Direction.right] = '>'
dir_c[Direction.down] = 'v'
dir_c[Direction.left] = '<'
    
class Turn(Enum):
    left = 1
    straight = 2
    right = 3
    
class Cart:
    def __init__(self,x,y,dir):
        self.x = x
        self.y = y
        self.dir = dir
        self.turn = Turn.left
    def turn_left(self):
        #print ("turn left")
        if self.dir == Direction.up:
            self.dir = Direction.left
        elif self.dir == Direction.right:
            self.dir = Direction.up
        elif self.dir == Direction.down:
            self.dir = Direction.right
        elif self.dir == Direction.left:
            self.dir = Direction.down
    def turn_right(self):
        #print ("turn right")
        if self.dir == Direction.up:
            self.dir = Direction.right
        elif self.dir == Direction.right:
            self.dir = Direction.down
        elif self.dir == Direction.down:
            self.dir = Direction.left
        elif self.dir == Direction.left:
            self.dir = Direction.up
        
    def move(self, grid):
        if self.dir == Direction.up:
            self.y -= 1
        elif self.dir == Direction.right:
            self.x += 1
        elif self.dir == Direction.down:
            self.y += 1
        elif self.dir == Direction.left:
            self.x -= 1
        #print ("move {0},{1}".format(self.x, self.y))
        c = grid[self.y-1][self.x-1]
        if c == '\\':
            if self.dir == Direction.up:
                self.dir = Direction.left
            elif self.dir == Direction.right:
                self.dir = Direction.down
            elif self.dir == Direction.down:
                self.dir = Direction.right
            elif self.dir == Direction.left:
                self.dir = Direction.up
        if c == '/':
            if self.dir == Direction.up:
                self.dir = Direction.right
            elif self.dir == Direction.down:
                self.dir = Direction.left
            elif self.dir == Direction.left:
                self.dir = Direction.down
            elif self.dir == Direction.right:
                self.dir = Direction.up
        if c == '+':
            if self.turn == Turn.left:
                self.turn_left()
                self.turn = Turn.straight
            elif self.turn == Turn.straight:
                self.turn = Turn.right
            elif self.turn == Turn.right:
                self.turn_right()
                self.turn = Turn.left
    def __lt__(self,other):
        if self.y == other.y:
            return self.x < other.x
        return self.y < other.y
        
y = 1
for l in lines:
    x = 1
    row = []
    for c in l:
        if c == '>':
            carts.append(Cart(x,y,Direction.right))
            row.append('-')
        elif c == '<':
            carts.append(Cart(x,y,Direction.left))
            row.append('-')
        elif c == 'v':
            carts.append(Cart(x,y,Direction.down))
            row.append('|')
        elif c == '^':
            carts.append(Cart(x,y,Direction.up))
            row.append('|')
        else:
            row.append(c)
        x += 1
    grid.append(row)
    y += 1

def test_for_crash(cart):
    for c in carts:
        if c != cart and c.y != 0:
            if (c.x == cart.x) and (c.y == cart.y):
                c.y = 0
                cart.y = 0
                return True
    return False


def print_grid():
    y = 1
    for row in grid:
        x = 1
        for c in row:
            char = c
            for cart in carts:
                if cart.x == x and cart.y == y:
                    char = dir_c[cart.dir]
            print (char, end="")
            x += 1
        print ("")
        y+=1

#print_grid()

nr_carts = len(carts)
while nr_carts>1:
#for i in range (20):
    for c in carts:
        if c.y != 0:
            print ("c{0}, ".format((c.x-1,c.y-1)),end="")
            c.move(grid)
            if test_for_crash(c):
                nr_carts -= 2
    print("")
    carts.sort()
    #print_grid()
    
for c in carts:
    if c.y != 0:
        print ("cart at {0}, {1}".format(c.x-1,c.y-1))
        
