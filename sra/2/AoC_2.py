# -*- coding: utf-8 -*-
"""
Created on Tue Dec  4 21:00:35 2018

@author: SRA
"""

file = open("2.txt")

def count23(line):
    cnt = {}
    for c in line:
        if c in cnt:
            cnt[c] += 1
        else:
            cnt[c] = 1
    
    found2 = False
    found3 = False
    for key in cnt:
        if cnt[key] == 2:
            found2 = True
        if cnt[key] == 3:
            found3 = True
    return found2, found3

nr2 = 0
nr3 = 0
for l in file:
    found2, found3 = count23(l)
    if found2:
        nr2 += 1
    if found3:
        nr3 += 1

print (nr2*nr3)