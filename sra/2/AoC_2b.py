# -*- coding: utf-8 -*-
"""
Created on Tue Dec  4 21:00:35 2018

@author: SRA
"""
import time
start_time = time.time()

import itertools

file = open("2.txt")
lines = file.read().splitlines()

#lines = ['abcde'
#, 'fghij'
#, 'klmno'
#, 'pqrst'
#, 'fguij'
#, 'axcye'
#, 'wvxyz' ]
results = []

for pair in itertools.combinations(lines,2):
    cnt = 0
    ID1, ID2 = pair
    for c in range(len(ID1)):
        if (ID1[c] != ID2[c]):
            cnt += 1
        if cnt>1:
            break
    if cnt == 1:
        results.append(pair)
                
text = ''
if len(results) != 1:
    print ("did not find exactly on result!")
else:
    ID1, ID2 = results[0]
    for c in range(len(ID1)):
        if (ID1[c] == ID2[c]):
            text +=  ID1[c]
    
print (text)

print (time.time() - start_time)

