# -*- coding: utf-8 -*-
"""
Created on Sun Dec 23 12:06:52 2018

@author: steff
"""

import re
import random
import itertools

class Position():
    def __init__(self,x,y,z):
        self.x = (x)
        self.y = (y)
        self.z = (z)

    def distance(self,c):
        return abs(self.x-c.x)+abs(self.y-c.y)+abs(self.z-c.z)
    
    def __str__(self):
        return "<{0},{1},{2}>".format(int(self.x), int(self.y), int(self.z))
    
    def __add__(self, pos):
        return Position (int(self.x + pos.x),int(self.y + pos.y),int(self.z + pos.z))
    
    def is_same_pos(self, pos):
        return self.x == pos.x and self.y == pos.y and self.z == pos.z

    def mul_nr(self,factor):
        return Position (int(self.x*factor),int(self.y*factor),int(self.z*factor))


class NanoBot():
    def __init__(self, line):
        self.pos = Position(0,0,0)
        self.range = 0
        m = re.search(r"pos=<([-+]?\d+),([-+]?\d+),([-+]?\d+)>, r=([-+]?\d+)", line)
        if m:
            self.pos = Position (int(m.group(1)),int(m.group(2)),int(m.group(3)))
            self.range = int(m.group(4))
        
    def __str__(self):
        return "{0} {1}".format(self.pos, self.range)
    
    def in_range_bot(self,bot):
        return self.pos.distance(bot.pos)<= self.range
    
    def in_range_pos(self,pos):
        return self.pos.distance(pos)<= self.range
    
def count_bots_in_range(pos, bots):
    nr = 0
    for b in bots:    
        if b.in_range_pos(pos): nr +=1
    return nr

lines = """pos=<10,12,12>, r=2
pos=<12,14,12>, r=2
pos=<16,12,12>, r=4
pos=<14,14,14>, r=6
pos=<50,50,50>, r=200
pos=<10,10,10>, r=5
"""

lines = open("23.txt").read()

bots = [NanoBot(l) for l in lines.splitlines()]

origin = Position(0,0,0)
center = Position(0,0,0)
mass = 0
for b in bots:
    center = center + b.pos.mul_nr(1/b.range)
    mass += 1/b.range
   
center = Position(center.x/mass, center.y/mass, center.z/mass)

maxpos = max(max([b.pos.x,b.pos.y,b.pos.z]) for b in bots)
minpos = min(min([b.pos.x,b.pos.y,b.pos.z]) for b in bots)

# all positions in cube [-1..1] except for (0,0,0)
dirs = [Position(d[0],d[1],d[2]) for d in itertools.product([-1,0,1],repeat=3) if d!=(0,0,0)]
  

def find_max(bots, pos, step_size):
    nr_visited = {}
    cur_val = count_bots_in_range(pos, bots)
    start_pos,start_val = pos,cur_val
    print ("start search from {0} - nr bots: {1}, stepsize: {2}".format(pos, cur_val,step_size))
    for _nr in range (5000):
        neighbors = {}
        # store values for all neighbors
        for d in dirs:
            npos = pos + d.mul_nr(step_size)
            neighbors[npos] = (count_bots_in_range(npos, bots), npos.distance(origin))
        # keep only positions with max value
        max_val = max(i[1][0] for i in neighbors.items())
        max_pos_list = [i for i in neighbors.items() if i[1][0] == max_val]
        cur_val = max_val
        if len(max_pos_list) == 1:
            pos = max_pos_list[0][0]
        else:
            # keep only positions with minimal distance
            min_dist = min(i[1][1] for i in max_pos_list)
            min_pos_list = [i for i in max_pos_list if i[1][1] == min_dist]
            if len(min_pos_list)==1:
                pos = min_pos_list[0][0]
            else:
                # randomly choose one
                pos = min_pos_list[random.randint(0,len(min_pos_list)-1)][0]
        cur_val = max_val
        
        if not str(pos) in nr_visited: nr_visited [str(pos)] = 0
        nr_visited [str(pos)] += 1
        # have we been here for 5 times?
        if nr_visited [str(pos)] == 5:
            # oscillating between start and second best?
            if cur_val < start_val:
                cur_val = start_val
                pos = start_pos
            elif cur_val == start_val and pos.distance(origin)>start_pos.distance(origin):
                cur_val = start_val
                pos = start_pos
            print ("=> found max: {0} - nr bots: {1} - distance {2}".format(pos, cur_val, pos.distance(origin)))
            return pos
    return origin

step_size = int((maxpos-minpos)/100)
if step_size<1:
    step_size = 1
cur_pos = center
while step_size >= 1:
    cur_pos = find_max(bots, cur_pos, step_size)
    if cur_pos.is_same_pos(origin):
        break
    step_size = int(step_size / 10)

# stepsize may not be 1 during last run
cur_pos = find_max(bots, cur_pos, 1)
# run again to make sure it's the best local maximum
cur_pos = find_max(bots, cur_pos, 1)
