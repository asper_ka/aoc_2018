# -*- coding: utf-8 -*-
"""
Created on Sun Dec 23 12:06:52 2018

@author: steff
"""

import re
from operator import attrgetter

class Position():
    def __init__(self,x,y,z):
        self.x = x
        self.y = y
        self.z = z

    def distance(self,c):
        return abs(self.x-c.x)+abs(self.y-c.y)+abs(self.z-c.z)
    
    def __str__(self):
        return "<{0},{1},{2}>".format(self.x, self.y, self.z)


class NanoBot():
    def __init__(self, line):
        self.pos = Position(0,0,0)
        self.range = 0
        m = re.search("pos=<([-+]?\d+),([-+]?\d+),([-+]?\d+)>, r=([-+]?\d+)", line)
        if m:
            self.pos = Position (int(m.group(1)),int(m.group(2)),int(m.group(3)))
            self.range = int(m.group(4))
        
    def __str__(self):
        return "{0} {1}".format(self.pos, self.range)
    
    def in_range(self,bot):
        return self.pos.distance(bot.pos)<= self.range
    
lines = """pos=<0,0,0>, r=4
pos=<1,0,0>, r=1
pos=<4,0,0>, r=3
pos=<0,2,0>, r=1
pos=<0,5,0>, r=3
pos=<0,0,3>, r=1
pos=<1,1,1>, r=1
pos=<1,1,2>, r=1
pos=<1,-3,1>, r=1
"""

#lines = open("23.txt").read()

bots = [NanoBot(l) for l in lines.splitlines()]

strongest = max(bots, key=attrgetter('range'))

nr_in_range = 0
for b in bots:
    print ("{0} - {1}: dist = {2}".format(strongest, b, strongest.pos.distance(b.pos)))
    if strongest.in_range(b): nr_in_range+= 1
    
print ("strongest Nanobot: {0}, nr bots in range: {1}".format(strongest, nr_in_range))

