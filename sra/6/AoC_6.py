# -*- coding: utf-8 -*-
"""
Created on Fri Dec  7 11:33:10 2018

@author: SRA
"""

import re

file = open("6.txt")
lines = file.read().splitlines()

minx = 1e10
maxx = -1e10
miny = 1e10
maxy = -1e10

class point:
    def __init__(self, id, x, y):
        self.id = id
        self.x = x
        self.y = y

    def distance (self, x, y):
        return abs(self.x-x) + abs(self.y-y)

def addDistances(p, f):
    for x in range(minx-1, maxx+2):
        for y in range(miny-1, maxy+2):
            cx = x - (minx-1)
            cy = y - (miny-1)
            dist = p.distance(x, y)
            vals = f[cx][cy]
            if vals[0]>dist:
                f[cx][cy] = (dist, p.id)
            elif vals[0]==dist:
                f[cx][cy] = (dist, -1)
                

points = []

for l in lines:
    m = re.search("([0-9]*), ([0-9]*)", l)
    if m:
        x = int(m.group(1))
        y = int(m.group(2))
        points.append(point(len(points), x, y))
        if x > maxx:
            maxx = x
        if x < minx:
            minx = x
        if y > maxy:
            maxy = y
        if y < miny:
            miny = y

field = [ [(1e10, -1) for i in range(miny-1, maxy+2)] for j in range(minx-1, maxx+2)]

area = {}
for p in points:
    addDistances(p, field)
    area[p.id] = 0   

for row in field:
    for col in row:
        if col[1] >= 0:
            area[col[1]] += 1

for col in field[0]:
    id = col[1]
    if id in area:        
        del area[id]
    
for col in field[-1]:
    id = col[1]
    if id in area:        
        del area[id]
        
for row in field:
    id = row[0][1]
    if id in area:        
        del area[id]
    id = row[-1][1]
    if id in area:        
        del area[id]
    

#for row in field:
#    for col in row:
#        if col[1] < 0:
#            print (". ", end="")
#        else:
#            print (str(col[1]) + " ", end="")
#    print ('')
        
max = 0
for a in area:
    if area[a] > max:
        max = area[a]
print(max)
