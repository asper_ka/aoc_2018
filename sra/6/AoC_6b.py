# -*- coding: utf-8 -*-
"""
Created on Fri Dec  7 11:33:10 2018

@author: SRA
"""

import re

file = open("6.txt")
lines = file.read().splitlines()
safedist = 10000

minx = 1e10
maxx = -1e10
miny = 1e10
maxy = -1e10

class point:
    def __init__(self, id, x, y):
        self.id = id
        self.x = x
        self.y = y

    def distance (self, x, y):
        return abs(self.x-x) + abs(self.y-y)

def addDistances(p, f):
    for x in range(minx-1, maxx+2):
        for y in range(miny-1, maxy+2):
            cx = x - (minx-1)
            cy = y - (miny-1)
            dist = p.distance(x, y)
            f[cx][cy] += dist
                

points = []

for l in lines:
    m = re.search("([0-9]*), ([0-9]*)", l)
    if m:
        x = int(m.group(1))
        y = int(m.group(2))
        points.append(point(len(points), x, y))
        if x > maxx:
            maxx = x
        if x < minx:
            minx = x
        if y > maxy:
            maxy = y
        if y < miny:
            miny = y

field = [ [0 for i in range(miny-1, maxy+2)] for j in range(minx-1, maxx+2)]

for p in points:
    addDistances(p, field)

cnt = 0
for row in field:
    for col in row:
        if col < safedist:
            cnt += 1
print (cnt)

