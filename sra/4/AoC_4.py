# -*- coding: utf-8 -*-
"""
Created on Tue Dec  4 21:29:36 2018

@author: SRA
"""
import re

file = open("4.txt")
lines = sorted(file.read().splitlines())

guard = 0
minutes = {}
startminute = -1
for l in lines:
    m = re.search(r".*Guard #(\d*) ", l)
    if m:
        guard = int(m.group(1))
        if not guard in minutes:
            minutes[guard] = [0 for i in range(60)] 
    else:
        m = re.search(r"\d\d:(\d\d)", l)
        minute = int( m.group(1) )
        if l.find("falls asleep") > 0:
            startminute = minute
        elif l.find("wakes up") > 0:
            for m1 in range (startminute, minute):
                minutes[guard][m1] += 1
                
maxsum = 0
maxguard = 0
for g in minutes:
    s = sum(minutes[g])
    if s > maxsum:
        maxsum = s
        maxguard = g

maxsum = 0
maxminute = 0    
for m in range(60):
    if minutes[maxguard][m] > maxsum:
        maxsum = minutes[maxguard][m]
        maxminute = m
        
print (maxguard * maxminute)
