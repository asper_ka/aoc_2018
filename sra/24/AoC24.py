# -*- coding: utf-8 -*-
"""
Created on Mon Dec 24 11:31:19 2018

@author: steff
"""

import re
from operator import attrgetter

input = """Immune System:
17 units each with 5390 hit points (weak to radiation, bludgeoning) with an attack that does 4507 fire damage at initiative 2
989 units each with 1274 hit points (immune to fire; weak to bludgeoning, slashing) with an attack that does 25 slashing damage at initiative 3

Infection:
801 units each with 4706 hit points (weak to radiation) with an attack that does 116 bludgeoning damage at initiative 1
4485 units each with 2961 hit points (immune to radiation; weak to fire, cold) with an attack that does 12 slashing damage at initiative 4
""".splitlines()

input = open("24.txt").read().splitlines()

class Group():
    def __init__(self, m, system, nr):
        self.system = system
        self.nr = nr
        self.units = int(m.group(1))
        self.hit_points = int(m.group(2))
        self.damage = int(m.group(4))
        self.attack_type = m.group(5)
        self.initiative = int(m.group(6))
        self.specialty = {'immune':[], 'weak':[]}
        special = m.group(3)
        if len(special)>0:
            for p in special[1:-1].split(';'):
                mm = re.search("(.*) to (.*)", p)
                if mm:
                    self.specialty[mm.group(1).strip()] = mm.group(2).split(", ")
    def __lt__(self,other):
        ps = self.calc_power()
        po = other.calc_power()
        if ps == po:
            return self.initiative < other.initiative
        else:
            return ps < po
    
    def calc_power(self):
        return self.units * self.damage
        
    def calc_damage(self,attack,damage):
        if attack in self.specialty['immune']:
            return 0
        if attack in self.specialty['weak']: 
            #print ("weak")
            return damage*2
        return damage
        
    def attack(self, defender):
        #print ("{0} attacks with {2} damage {3}, power={4}" \
        #   .format(self, defender, self.attack_type, self.damage, self.calc_power()))
        #print("defender: {0}, weak to {1}, immune to {1}" \
        #   .format(defender, defender.specialty['weak'],defender.specialty['immune']))
        damage = defender.calc_damage(self.attack_type, self.calc_power())
        nr_units = damage // defender.hit_points
        #print ("hits with '{2}' {0} to {1} hit_points killing {3}".
        #    format(damage,defender.hit_points,self.attack_type,nr_units))
        defender.units -= nr_units
        if defender.units <0:
            defender.units=0
        #print ("defender has {0} units left".format(defender.units))
        #print ("")
        
    def __str__(self):
        #return ("Group {0} ({1}) - {2} units, power: {3}".format(self.nr, self.system, self.units, self.calc_power()))
        return ("Group {0} ({1}) - {2} units".format(self.nr, self.system, self.units))

    def select_targets(self,targets,selected_targets):
        targets_damage = {}        
        max_damage = 0
        for t in targets:
            if attacker != t \
             and t not in selected_targets \
             and attacker.system != t.system:
                 damage = t.calc_damage(attacker.attack_type, attacker.calc_power())
                 targets_damage[t] = damage
                 if damage > max_damage:
                     max_damage = damage
        if max_damage==0:
            return []
        #for t in targets_damage:
        #    print ("{0} would attack {1} with damage {2}".format(attacker,t,targets_damage[t]))
        ts = [k for k,v in targets_damage.items() if v==max_damage]
        ts.sort(reverse=True)
        return ts
    
def count_systems(groups):
    systems = {}
    for g in groups:
        s = g.system
        if not s in systems: systems[s] = 0
        systems[s] += 1
    return len(systems)
     
def create_groups(lines):
    system = ""
    groups= []
    system_nrs = {}
    for l in lines:
        #print (l)
        if len(l)>0:
            m = re.search("(\d+).* (\d+) hit points (\(.*\)|).* does (\d+) (.*) damage.* initiative (\d+)",l)
            if not m:
                system = l
                #print ("System: {0}".format(system))
                if not system in system_nrs: system_nrs[system] = 0
            else:
                system_nrs[system] += 1
                groups.append(Group(m, system, system_nrs[system]))
    return groups

groups = create_groups(input)
rounds = 0
while count_systems(groups)>1 and rounds < 9000:
    groups.sort(reverse=True)
    #for g in groups:
    #    print ("{0} power={1}".format(g,g.calc_power()))
    #print("")

    attacks = {}
    all_targets = []
    for attacker in groups:
        targets = attacker.select_targets(groups,all_targets)
        if len(targets)>0:
            attacks[attacker] = targets[0]
            #print ("{0} will attack {1}".format(attacker,targets[0]))    
            all_targets.append(targets[0])        
            #print ("")

    groups.sort(key=attrgetter('initiative'), reverse=True)
    for g in groups:
        if g in attacks and g.units > 0:
            g.attack(attacks[g])

    ng = [g for g in groups if g.units > 0]
    groups = ng
    rounds += 1
    #print("")
    #print("")

print ("remaining groups:")
nr_units = 0
for g in groups:
    print ("{0} power={1}".format(g,g.calc_power()))
    nr_units+=g.units
print ("units: {0}".format(nr_units))