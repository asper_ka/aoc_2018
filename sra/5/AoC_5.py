# -*- coding: utf-8 -*-
"""
Created on Wed Dec  5 23:58:41 2018

@author: SRA
"""

#input = "aabAAB"

import time
start = time.time()

file = open("5.txt")
input = file.read().strip() 

print (len(input))

def react (input):
    lastindex = 0
    output = ""
    xr = iter(range(len(input)-1))
    for i in xr:        
        if input[i] != input[i+1]:
            if input[i].upper() == input[i+1].upper():
                output += input[lastindex:i]
                lastindex = i+2
                #print ("output={0}, lastindex:{1}, i:{2}".format(output, lastindex,i))
                next(xr)
    output += input[lastindex:]
    return output 

def react_all(input):
    while True:
        output = react(input)
        if output == input:
            return output
        else:
            input = output

output = react_all(input)
print (len(output))
print (time.time()-start)