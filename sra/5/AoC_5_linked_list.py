# -*- coding: utf-8 -*-
"""
Created on Wed Dec  5 23:58:41 2018

@author: SRA
"""

import string
import sys
sys.path.append('../lib')
from LinkedList import LinkedList
input = "dabAcCaCBAcCcaDA"

import time
start = time.time()

file = open("5.txt")
input = file.read().strip() 

print (len(input))

class Polymer(LinkedList):
    def __init__(self,input,ignore=''):
        self.start = None
        for c in input:
            if c!=ignore and c!=ignore.upper():
                self.append_node(c)
    
    def react(self):
        current = self.start
        while current.next:
            reacted = False
            if current.data != current.next.data:
                if current.data.upper() == current.next.data.upper():
                    self.remove_node(current.next)
                    current = self.remove_node(current)
                    reacted = True
            if not reacted:
                current = current.next
                

# part one
p = Polymer(input)
p.react()
print (p.get_length())

print ("-----------------------")
# part two
minlength = len(input)
for c in string.ascii_lowercase:
    p = Polymer(input,c)
    p.react()
    l = p.get_length()
    print ("{0} -> {1}".format(c,l))
    if l < minlength:
        minlength = l

print (minlength)


print (time.time()-start)