# -*- coding: utf-8 -*-
"""
Created on Sun Dec  9 07:29:31 2018

@author: steff
"""


class Recipe:
    def __init__(self, score):
        self.next = self
        self.prev = self
        self.score = score

def add_recipe (rec, score):
    r = Recipe(score)
    r.prev = rec
    r.next = rec.next
    rec.next = r
    return r

first = Recipe(3)
current1 = first
current2 = add_recipe(first, 7)
last = add_recipe(current2, 1)
last = add_recipe(last, 0)
nr = 4
    
def print_recipe(r):
    if current1 == r:
        print ("({0})".format(r.score), end = "")
    elif current2 == r:
        print ("[{0}]".format(r.score), end = "")
    else:
        print (" {0} ".format(r.score), end = "")

def get_pattern(r, l):
    p = ""
    for i in range (l):
        p = str(r.score) + p
        r = r.prev
    return p

def print_recipes():
    r = first
    while True:
        print_recipe(r)
        r = r.next
        if r == first:
            break
    print (" - {0} - {1}".format(nr,get_pattern(last)))


pattern = "765071"

   
pattern_found = False
while not pattern_found and nr<30000000:
    scoresum = current1.score + current2.score
    if scoresum > 9:
        last = add_recipe(last, 1)
        nr += 1
        scoresum -= 10
    last = add_recipe(last, scoresum)
    nr += 1
    for r in range (current1.score+1):
        current1 = current1.next
    for r in range (current2.score+1):
        current2 = current2.next
    if get_pattern(last, len(pattern)) == pattern:
        pattern_found = True
    if get_pattern(last.prev, len(pattern)) == pattern:
        pattern_found = True
        nr -= 1
    #print_recipes()

print (nr-len(pattern))