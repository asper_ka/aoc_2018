# -*- coding: utf-8 -*-
"""
Created on Tue Dec 11 06:15:30 2018

@author: steff
"""



def get_power(sn, x, y):
    r_id = x + 10
    p = r_id * y
    p += sn
    p *= r_id
    if (p<100):
        p = 0
    else:
        p = int(str(p)[-3])
    p-= 5
    return p

def fill_grid(sn):
    grid = [ [ 0 for x in range (1,302)] for y in range (1,302)]
    for x in range (1,301):
        for y in range (1,301):
            grid[x][y] = get_power(sn, x, y)
    return grid

def get_power_cell(grid, x0, y0):
    s = 0
    for x in range (x0, x0+3):
        for y in range (y0, y0+3):
            s += grid[x][y]
    return s

grid = fill_grid(7857)
max_power = -1e10
maxx = 0
maxy = 0
for x in range (1,298):
    for y in range (1,298):
        p = get_power_cell(grid, x, y)
        if p > max_power:
            max_power = p
            maxx = x
            maxy = y
            
def print_region(grid, x0, y0):
    for x in range (x0, x0+5):
        for y in range (y0, y0+5):
            print ("{0} ".format(grid[x][y]), end="")
        print("")
    
print ("max {0} at {1},{2}".format(max_power,maxx,maxy))


    