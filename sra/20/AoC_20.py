# -*- coding: utf-8 -*-
"""
Created on Fri Dec 21 20:25:04 2018

@author: steff
"""



expr = list("WSSEESWWWNW(S|NENNEEEENN(ESSSSW(NWSW|SSEN)|WSWWN(E|WWS(E|SS))))")
file = open("20.txt")
expr = list(file.read())

from enum import Enum


class Dir(Enum):
    north = (0,-1)
    east = (1,0)
    west = (-1,0)
    south = (0,1)
    
    def get_opposite(dir):
        if dir == Dir.north:
            return Dir.south
        elif dir == Dir.south:
            return Dir.north
        elif dir == Dir.east:
            return Dir.west
        elif dir == Dir.west:
            return Dir.east

directions = {'E':Dir.east, 'N':Dir.north, 'W':Dir.west, 'S':Dir.south}

class Room:
    def __init__(self, pos):
        self.neighbors = {}
        for d in Dir:
            self.neighbors [d] = None
        self.pos = pos

def add_positions(pos1, pos2):
    return (pos1[0]+pos2[0], pos1[1]+pos2[1])

def print_rooms(r):
    min_x = 1e10
    max_x = -1e10
    min_y = 1e10
    max_y = -1e10
    for c in r:
        if c[0] > max_x: max_x = c[0]
        if c[0] < min_x: min_x = c[0]
        if c[1] > max_y: max_y = c[1]
        if c[1] < min_y: min_y = c[1]
    print ("{0} - {1} , {2} - {3}".format(min_x, max_x, min_y,max_y))
    nr_x = max_x-min_x+1
    for y in range (min_y, max_y+1):
        line = ''
        for x in range (min_x, max_x+1):
            line += '#'
            if (x,y) in r:
                if (r[(x,y)].neighbors[Dir.north]):
                    line += '-'
                else: line += '#'
            else: line += '#'
        line += '#'
        print (line)
        line = ''
        for x in range (min_x, max_x+1):
            if (x,y) in r:
                if (r[(x,y)].neighbors[Dir.west]):
                    line += '|'               
                else: line += '#'
                if (x,y)==(0,0):line+='X'
                else: line += ' '
            else: line += '##'
        line += '#'
        print (line)
    
    print ("#" * (nr_x*2+1))
               

rooms = {}

def add_room(room, direction):
    new_room = Room(add_positions(room.pos,direction.value))
    room.neighbors[direction] = new_room
    new_room.neighbors[Dir.get_opposite(direction)] = room
    return new_room

root = Room((0,0))

rooms[root.pos] = root


idx = 0
cur_room = root


def add_branch(cur_room, e):
    #print ("Add Branch, {0}".format(cur_room.pos))
    while len(e):
        c = e[0]
        del e[0]
        if not c in directions:
            if c == '(':
                if not fork_branches(cur_room, e):
                    return
            else:
                e.insert(0,c)
                return
        else:
            cur_room = add_room(cur_room, directions[c])
            rooms[cur_room.pos] = cur_room
            #print ("add room: {0} {1}".format(c,cur_room.pos))

def fork_branches(cur_room, e):
    while len(e):
        add_branch(cur_room,e)
        if (len(e)==0): return False
        c = e[0]
        del e[0]
        if c==')': return False
        if c=='|':
            if e[0] == ')':
                del e[0]
                return True
                
    

add_branch(root, expr)


def calc_dist(root):
    distances = {}
    queue = []
    queue.append((root,0))
    visited = [root.pos]
    while queue:
        (n,dist) = queue.pop(0)
        for d in Dir:
            nn = n.neighbors[d]
            if nn:
                if not nn.pos in visited:
                    distances[nn.pos] = dist+1
                    queue.append((nn,dist+1))
                    visited.append(nn.pos)
        
    return distances

ds = calc_dist(root)
print (max( ds.values()))
#print_rooms(rooms)
print (len([1 for i in ds.values() if i >= 1000]))


  