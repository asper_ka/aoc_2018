#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import collections


class Proc:
    def __init__(self):
        self.r = [0, 0, 0, 0]

        self.f = {
            'addr': lambda a, b: self.r[a] + self.r[b],
            'addi': lambda a, b: self.r[a] + b,

            'mulr': lambda a, b: self.r[a] * self.r[b],
            'muli': lambda a, b: self.r[a] * b,

            'banr': lambda a, b: self.r[a] & self.r[b],
            'bani': lambda a, b: self.r[a] & b,

            'borr': lambda a, b: self.r[a] | self.r[b],
            'bori': lambda a, b: self.r[a] | b,

            'setr': lambda a, b: self.r[a],
            'seti': lambda a, b: a,

            'gtir': lambda a, b: 1 if a > self.r[b] else 0,
            'gtri': lambda a, b: 1 if self.r[a] > b else 0,
            'gtrr': lambda a, b: 1 if self.r[a] > self.r[b] else 0,

            'eqir': lambda a, b: 1 if a == self.r[b] else 0,
            'eqri': lambda a, b: 1 if self.r[a] == b else 0,
            'eqrr': lambda a, b: 1 if self.r[a] == self.r[b] else 0,
        }

        self.opcodes = {}

    def set_regs(self, regs):
        self.r = regs[:]

    def get_regs(self):
        return self.r[:]

    def call(self, fname, a, b, c):
        self.r[c] = self.f[fname](a, b)

    def call_by_op(self, opts):
        opcode, a, b, c = opts
        fname = self.opcodes[opcode]
        self.call(fname, a, b, c)

    def find_matching(self, before, after, opts):
        a, b, c = opts
        matching = []
        for name in sorted(self.f.keys()):
            self.set_regs(before)
            self.call(name, a, b, c)
            if self.get_regs() == after:
                matching.append(name)
        return matching


matcher = re.compile(r'.*:\s*\[\s*(\d+),\s*(\d+),\s*(\d+),\s*(\d+)\s*\]')

before = None
after = None
opts = None

data = []
for line in file('data1').readlines():
    line = line.strip()
    if not line:
        continue
    elif 'Before' in line:
        g = matcher.match(line).groups()
        before = [int(g) for g in g]
    elif 'After' in line:
        g = matcher.match(line).groups()
        after = [int(g) for g in g]
    else:
        opts = [int(s) for s in line.split()]

    if before and after and opts:
        data.append([before, after, opts])
        before = None
        after = None
        opts = None


p = Proc()

opcodes = collections.defaultdict(set)
result = 0
for before, after, opts in data:
    op_code = opts[0]
    matching = p.find_matching(
        before,
        after,
        opts[1:]
    )

    opcodes[op_code].update(matching)

for op_code in opcodes.keys():
    opcodes[op_code] = sorted(list(opcodes[op_code]))

while not all([len(m) == 1 for m in opcodes.values()]):
    for op_code, matching in opcodes.iteritems():
        if len(matching) == 1:
            name = matching[0]
            for oc2, m2 in opcodes.iteritems():
                if op_code == oc2:
                    continue
                if name in m2:
                    m2.remove(name)

p.opcodes = dict(((oc, m[0]) for oc, m in opcodes.items()))

data = []
for line in file('data2').readlines():
    line = line.strip()
    if not line:
        continue

    opts = [int(s) for s in line.split()]
    data.append(opts)

for opts in data:
    p.call_by_op(opts)

print p.get_regs()[0]
