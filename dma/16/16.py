#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import collections


class Proc:
    def __init__(self):
        self.r = [0, 0, 0, 0]

        self.f = {
            'addr': lambda a, b: self.r[a] + self.r[b],
            'addi': lambda a, b: self.r[a] + b,

            'mulr': lambda a, b: self.r[a] * self.r[b],
            'muli': lambda a, b: self.r[a] * b,

            'banr': lambda a, b: self.r[a] & self.r[b],
            'bani': lambda a, b: self.r[a] & b,

            'borr': lambda a, b: self.r[a] | self.r[b],
            'bori': lambda a, b: self.r[a] | b,

            'setr': lambda a, b: self.r[a],
            'seti': lambda a, b: a,

            'gtir': lambda a, b: 1 if a > self.r[b] else 0,
            'gtri': lambda a, b: 1 if self.r[a] > b else 0,
            'gtrr': lambda a, b: 1 if self.r[a] > self.r[b] else 0,

            'eqir': lambda a, b: 1 if a == self.r[b] else 0,
            'eqri': lambda a, b: 1 if self.r[a] == b else 0,
            'eqrr': lambda a, b: 1 if self.r[a] == self.r[b] else 0,
        }

    def set_regs(self, regs):
        self.r = regs[:]

    def get_regs(self):
        return self.r[:]

    def call(self, fname, a, b, c):
        self.r[c] = self.f[fname](a, b)

    def find_matching(self, before, after, opts):
        a, b, c = opts
        matching = []
        for name in sorted(self.f.keys()):
            self.set_regs(before)
            self.call(name, a, b, c)
            if self.get_regs() == after:
                matching.append(name)
        return matching


matcher = re.compile(r'.*:\s*\[\s*(\d+),\s*(\d+),\s*(\d+),\s*(\d+)\s*\]')

before = None
after = None
opts = None

data = []
for line in file('data1').readlines():
    line = line.strip()
    if not line:
        continue
    elif 'Before' in line:
        g = matcher.match(line).groups()
        before = [int(g) for g in g]
    elif 'After' in line:
        g = matcher.match(line).groups()
        after = [int(g) for g in g]
    else:
        opts = [int(s) for s in line.split()]

    if before and after and opts:
        data.append([before, after, opts])
        before = None
        after = None
        opts = None


p = Proc()

result = 0
for before, after, opts in data:
    op_code = opts[0]
    matching = p.find_matching(
        before,
        after,
        opts[1:]
    )

    if len(matching) >= 3:
        result += 1

print result
