#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import re
import sys


class Field:
    Open = 1
    Tree = 2
    LumberYard = 3

    tr = {
        '.': Open,
        '|': Tree,
        '#': LumberYard
    }
    itr = dict((v, k) for k, v in tr.items())

    def __init__(self):
        pass

    def read_from_field(self, filename):
        rows = []
        for line in file(filename).readlines():
            row = []
            for c in line.strip():
                row.append(Field.tr[c])
            rows.append(row)

        self.field = np.array(rows, dtype=np.int8)
        self.num_rows, self.num_cols = self.field.shape

    def __repr__(self):
        out = ''
        for row, row_values in enumerate(self.field):
            for col, f in enumerate(row_values):
                out += '{}'.format(Field.itr[f])
            out += '\n'

        return out

    def show(self):
        print f
        print
        #raw_input()

    def get_neighbors(self, row, col):
        neighbors = []
        if row > 0:
            neighbors.append((row - 1, col))
            if col > 0:
                neighbors.append((row - 1, col - 1))
            if col < self.num_cols - 1:
                neighbors.append((row - 1, col + 1))

        if row < self.num_rows - 1:
            neighbors.append((row + 1, col))
            if col > 0:
                neighbors.append((row + 1, col - 1))
            if col < self.num_cols - 1:
                neighbors.append((row + 1, col + 1))

        if col > 0:
            neighbors.append((row, col - 1))
            if row > 0:
                neighbors.append((row - 1, col - 1))
            if row < self.num_rows - 1:
                neighbors.append((row + 1, col - 1))

        if col < self.num_cols-1:
            neighbors.append((row, col + 1))
            if row > 0:
                neighbors.append((row - 1, col + 1))
            if row < self.num_rows - 1:
                neighbors.append((row + 1, col + 1))

        return list(set(neighbors))

    def count_neighbors_by_type(self, neighbors, ftype):
        num = 0
        for neighbor in neighbors:
            if self.field[neighbor] == ftype:
                num += 1
        return num

    def get_result(self):
        n_tree = len(np.where(self.field == Field.Tree)[0])
        n_yard = len(np.where(self.field == Field.LumberYard)[0])
        return n_tree*n_yard

    def calc_next(self):
        next_field = np.zeros_like(self.field)
        for row in range(self.num_rows):
            for col in range(self.num_cols):
                neighbors = self.get_neighbors(row, col)
                state = self.field[row, col]
                if state == Field.Open:
                    if self.count_neighbors_by_type(neighbors, Field.Tree) >= 3:
                        next_state = Field.Tree
                    else:
                        next_state = Field.Open
                elif state == Field.Tree:
                    if self.count_neighbors_by_type(neighbors, Field.LumberYard) >= 3:
                        next_state = Field.LumberYard
                    else:
                        next_state = Field.Tree
                elif state == Field.LumberYard:
                    if (self.count_neighbors_by_type(neighbors, Field.LumberYard) >= 1
                            and self.count_neighbors_by_type(neighbors, Field.Tree) >= 1):
                        next_state = Field.LumberYard
                    else:
                        next_state = Field.Open
                else:
                    raise ValueError
                next_field[row, col] = next_state

        self.field = next_field


f = Field()
f.read_from_field('data')

f.show()
results = []
for i in range(1, 2000):
    f.calc_next()
    result = f.get_result()
    print "min", i, 'result', result
    results.append((i, result))
    #f.show()

results = np.array(results, dtype=int)
