#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import itertools
import sys

def get_closest_point(xi, yi):
    dists = []
    for x,y in data:
        dist = abs(xi-x) + abs(yi-y)
        dists.append(dist)
    s_dists = sorted(dists)

    if s_dists[0] == s_dists[1]:
        return None
    return np.argmin(dists)

data=np.genfromtxt('data', delimiter=',', dtype=int)
xs, ys = data.T

all_areas = []
for d in (0,1):
    areas = np.zeros(shape=len(data))
    for yi in range(min(ys)-d, max(ys)+1+d):
        print yi
        for xi in range(min(xs)-d, max(xs)+1+d):
            ci = get_closest_point(xi, yi)
            if ci is not None:
                areas[ci] += 1
    all_areas.append(areas)

a0, a1 = all_areas

a0[a0!=a1] = 0
print max(a0)
