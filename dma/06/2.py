#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import itertools
import sys

data=np.genfromtxt('data', delimiter=',', dtype=int)
xs, ys = data.T

def get_sum_distances(xi, yi):
    distsum = 0
    for x,y in data:
        dist = abs(xi-x) + abs(yi-y)
        distsum += dist

    return distsum

d = 0    
nf = 0
for yi in range(min(ys)-d, max(ys)+1+d):
    print yi
    for xi in range(min(xs)-d, max(xs)+1+d):
        s = get_sum_distances(xi, yi)
        if s < 10000:
            nf += 1

print nf
