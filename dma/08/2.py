#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import itertools
import sys


class Node:
    def __init__(self):
        self.nodes = []
        self.meta = []
    
    def getvalue(self):
        if not self.nodes:
            return sum(self.meta)
            
        value = 0
        for i in self.meta:
            if i > len(self.nodes):
                continue
            value += self.nodes[i-1].getvalue()
        return value
            
            
        
def read_tree(di):
    num_children = di.next()
    num_meta = di.next()
    
    n = Node()
    
    for i in range(num_children):
        cn = read_tree(di)
        n.nodes.append(cn)
    
    meta = []
    for i in range(num_meta):
        m = di.next()
        n.meta.append(m)
    
    return n
        

data=np.genfromtxt('data', dtype=int)
my_iter = iter(data)
root = read_tree(my_iter)
print root.getvalue()

