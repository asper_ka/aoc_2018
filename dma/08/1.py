#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import itertools
import sys


def read_tree(di):
    global meta_sum
    
    num_children = di.next()
    num_meta = di.next()
    
    meta_sum = 0
    for i in range(num_children):
        meta_sum += read_tree(di)
    
    for i in range(num_meta):
        m = di.next()
        meta_sum += m
        
    return meta_sum

data=np.genfromtxt('data', dtype=int)
my_iter = iter(data)
meta_sum = read_tree(my_iter)

print meta_sum
