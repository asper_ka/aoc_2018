#!/usr/bin/env python
# -*- coding: utf-8 -*

import numpy as np


def get_num_in_range(point):
    mhd = np.sum(np.abs(points - point), axis=1)
    num = len(np.where(mhd <= rs)[0])
    return num


def get_max_num_in_range_in_box(minp, maxp):
    xmin, ymin, zmin = minp
    xmax, ymax, zmax = maxp
    max_num = 0
    max_coord = None
    for x in range(xmin, xmax):
        for y in range(ymin, ymax):
            for z in range(zmin, zmax):
                r = get_num_in_range((x, y, z))
                if r > max_num:
                    max_num = r
                    max_coord = [x, y, z]
    return max_num, max_coord


def find_max_coord(minp, maxp, total_max, rec_depth):
    rand_sample_size = 1000

    if np.prod(maxp-minp) < rand_sample_size:
        return get_max_num_in_range_in_box(minp, maxp)

    rand_points = []
    for minc, maxc in zip(minp, maxp):
        if minc >= maxc:
            rand_points.append(np.repeat(minc, rand_sample_size))
        else:
            rand_points.append(np.random.randint(minc, maxc, size=rand_sample_size))

    rand_points = np.column_stack(rand_points)
    rand_points = np.unique(rand_points, axis=0)

    num_in_range = np.array(map(get_num_in_range, rand_points))
    max_num = max(num_in_range)

    scale_factor = rand_sample_size**(1./3)
    new_width = np.array((maxp-minp)/scale_factor, dtype=int)

    max_points = rand_points[num_in_range == max_num]

    total_max_coord = None
    print '{}: checking {} maximum positions'.format(rec_depth, len(max_points))
    for max_point in max_points:
        result_max, result_coord = find_max_coord(max_point-new_width, max_point+new_width, total_max, rec_depth+1)
        if result_max > total_max:
            total_max = result_max
            total_max_coord = result_coord
            print 'found max', total_max, result_coord, np.sum(result_coord)

    return total_max, total_max_coord


#data = np.genfromtxt('testdata2', delimiter=',')
data = np.genfromtxt('data2', delimiter=',')
points = data[:, :3]
rs = data[:, 3]
minp = np.array([np.min(points[:, i], axis=0) for i in range(3)])
maxp = np.array([np.max(points[:, i], axis=0) for i in range(3)])
max_value, max_coords = find_max_coord(minp, maxp, 0, 0)
print "final result", max_value, max_coords, np.sum(max_coords)
