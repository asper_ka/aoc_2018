#!/usr/bin/env python
# -*- coding: utf-8 -*

import numpy as np

data = np.genfromtxt('data2', delimiter=',')
max_idx = np.argmax(data.T[3])
xm, ym, zm, rm = data[max_idx]
x, y, z, r = data.T

mhd = np.abs(x-xm) + np.abs(y-ym) + np.abs(z-zm)
print len(np.where(mhd <= rm)[0])
