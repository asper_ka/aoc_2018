#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import itertools
import sys

@np.vectorize
def get_power(x, y, s):
    rack = x+10
    power = rack*y
    power += s
    power *= rack
    power_s = str(power)
    if len(power_s) < 3:
        h = 0
    else:
        h = int(power_s[-3])
    power = h
    power -= 5
    return power


width = 300
height = width
serial = 9110

xs, ys = np.meshgrid(range(1, width+1), range(1, height+1))

m=get_power(xs, ys, serial).T

#x = 101
#y = 153
#print m[x-1, y-1]

sw = 3
sw_max = None
for sw in range(1, width):
    ms = np.zeros(shape=(width-sw+1, height-sw+1))
    for x in range(1, width-sw + 1):
        for y in range(1, height-sw + 1):
            ms[x, y] = np.sum(m[x:x+sw, y:y+sw])

            
    xc, yc = np.unravel_index(ms.argmax(), ms.shape)
    print ms.max(), xc+1, yc+1, sw
    if sw_max and sw > sw_max:
        sw_max = sw
    
print 'sw_max', sw_max
