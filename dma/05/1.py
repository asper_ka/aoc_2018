#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import itertools
import sys

data=file("data").read().strip()

for i in range(65, 91):
    repl = chr(i)
    data_i = data.replace(repl, '').replace(repl.lower(), '')
    #data_i = data
    ablage=[]
    for i,c in enumerate(data_i):
        if ablage:
            lc = ablage[-1]
            if abs(ord(c)-ord(lc)) == 32:
                ablage.pop()
            else:
                ablage.append(c)
        else:
            ablage.append(c)
    
    print repl, len(ablage)
#    break
