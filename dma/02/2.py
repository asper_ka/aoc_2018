#!/usr/bin/env python
# -*- coding: utf-8 -*-

import itertools

words = [w.strip() for w in file('data').readlines()]

for w1, w2 in itertools.combinations(words, 2):
    num_diff = 0
    for c1, c2 in zip(w1, w2):
        if c1!=c2:
            num_diff += 1
    if num_diff == 1:
        print w1
        print w2
