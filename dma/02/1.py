#!/usr/bin/env python
# -*- coding: utf-8 -*-

import collections
import numpy as np

data = np.genfromtxt('data', dtype=str)

num2 = 0
num3 = 0

for key in data:
    counter = collections.Counter()
    for c in key:
        counter[c] += 1
    if 2 in counter.values():
        num2 += 1
    if 3 in counter.values():
        num3 += 1

print num2 * num3
