#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import itertools
import sys

patches = []

guard = None
times = {}
shifts = {}
for line in file('data_sorted').readlines():
    line = line.strip()
    items = line.split()
    if 'Guard' in line:
        guard = items[3][1:]
        #print 'guard', guard
        m_asleep = None
        m_wakes = None
    elif 'asleep' in line:
        m_asleep = int(items[1][-3:-1])
        #print 'asleep', m_asleep
    elif 'wakes' in line:
        m_wakes = int(items[1][-3:-1])
        #print 'guard', guard, 'asleep', m_asleep, 'wakes', m_wakes
        if guard not in times:
            times[guard] = np.zeros(dtype=float, shape=(60,))
            shifts[guard] = 0
        times[guard][m_asleep-1:m_wakes-1] += 1
        shifts[guard] += 1
        m_asleep = None
        m_wakes = None

sum_times = {}
for g in times:
   sum_times[g] = sum(times[g]) 
   

guard = 2851
max_min = np.argmax(times[str(guard)])+1
print max_min * guard
