#!/usr/bin/env python
# -*- coding: utf-8 -*

import numpy as np

Rock = 0
Wet = 1
Narrow = 2


def get_geo_index(x, y):
    if not geo_index_set[x, y]:
        if x == 0 and y == 0:
            result = 0
        elif x == tx and y == ty:
            result = 0
        elif y == 0:
            result = x*16807
        elif x == 0:
            result = y*48271
        else:
            result = get_erosion_level(x-1, y) * get_erosion_level(x, y-1)
        geo_index_set[x, y] = True
        geo_index[x, y] = result

    return geo_index[x, y]


def get_erosion_level(x, y):
    if not erosion_levels_set[x, y]:
        result = (get_geo_index(x, y) + depth) % 20183
        erosion_levels_set[x, y] = True
        erosion_levels[x, y] = result
    return erosion_levels[x, y]


def get_risk_level(x, y):
    return get_erosion_level(x, y) % 3


sx, sy = 0, 0

depth, tx, ty = 10689, 11, 722
#depth, tx, ty = 510, 10, 10


erosion_levels = np.zeros(shape=(tx+1, ty+1), dtype=int)
erosion_levels_set = np.zeros_like(erosion_levels, dtype=bool)

geo_index = np.zeros_like(erosion_levels)
geo_index_set = np.zeros_like(geo_index, dtype=bool)

total_risk = 0
for y in range(sy, ty + 1):
    for x in range(sx, tx+1):
        total_risk += get_risk_level(x, y)

print 'total_risk', total_risk
