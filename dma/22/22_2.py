#!/usr/bin/env python
# -*- coding: utf-8 -*

import numpy as np

NoTool = 0
Torch = 1
ClimbGear = 2

Rock = 0
Wet = 1
Narrow = 2

type_tr = {
    Rock: '.',
    Wet: '=',
    Narrow: '|',
}

ChangeDuration = 7
StepDuration = 1

tool_str = {
    NoTool: 'N',
    Torch: 'T',
    ClimbGear: 'C',
}


class Node(object):
    __slots__ = ['pos', 'tool', 'duration']

    def __init__(self, pos, tool):
        self.pos = pos
        self.tool = tool
        self.duration = 0

    def __repr__(self):
        return '{},{}:{}'.format(self.pos[0], self.pos[1], tool_str[self.tool])

    def __eq__(self, other):
        return self.pos == other.pos and self.tool == other.tool

    def get_distance(self, other):
        d = abs(self.pos[0] - other.pos[0]) + abs(self.pos[1] - other.pos[1])
        return d

    @staticmethod
    def from_tuple(t):
        return Node(t[0], t[1])

    def to_tuple(self):
        return self.pos, self.tool


class Cave:

    matching_tools = {
        Rock: [Torch, ClimbGear],
        Wet: [NoTool, ClimbGear],
        Narrow: [NoTool, Torch]
    }

    def __init__(self, depth, start_pos, target_pos):
        self.depth = depth
        self.trow = target_pos[1]
        self.tcol = target_pos[0]
        self.start_row = start_pos[1]
        self.start_col = start_pos[0]

        self.erosion_levels = {}
        self.geo_index = {}

    def __repr__(self):
        out = ''
        max_rows = self.trow + 10
        max_cols = self.tcol + 10
        for y in range(max_rows):
            line = ''
            for x in range(max_cols):
                if y == self.trow and x == self.tcol:
                    line += 'T'
                elif y == self.start_row and x == self.start_col:
                    line += 'X'
                else:
                    line += type_tr[self.get_type((y, x))]
            out += line + '\n'
        return out

    def get_type(self, pos):
        y, x = pos
        return self.get_erosion_level(x, y) % 3

    def get_geo_index(self, x, y):
        if (x, y) not in self.geo_index:
            if x == 0 and y == 0:
                result = 0
            elif x == self.tcol and y == self.trow:
                result = 0
            elif y == 0:
                result = x*16807
            elif x == 0:
                result = y*48271
            else:
                result = self.get_erosion_level(x-1, y) * self.get_erosion_level(x, y-1)
            self.geo_index[(x, y)] = result

        return self.geo_index[(x, y)]

    def get_erosion_level(self, x, y):
        if (x, y) not in self.erosion_levels:
            result = (self.get_geo_index(x, y) + self.depth) % 20183
            self.erosion_levels[(x, y)] = result
        return self.erosion_levels[(x, y)]

    def get_neighbor_nodes(self, node):
        nodes = []

        ctype = self.get_type(node.pos)
        matching_tools_c = self.matching_tools[ctype]
        for new_tool in matching_tools_c:
            if new_tool == node.tool:
                continue
            nodes.append(Node(node.pos, new_tool))

        positions = self.get_neighbor_positions(node.pos)
        for new_pos in positions:
            ntype = self.get_type(new_pos)
            matching_tools_n = self.matching_tools[ntype]
            if node.tool in matching_tools_n:
                nodes.append(Node(new_pos, node.tool))

        return nodes

    def get_neighbor_positions(self, pos):
        row, col = pos
        tiles = []
        if row > 0:
            tiles.append((row - 1, col))
        tiles.append((row + 1, col))
        if col > 0:
            tiles.append((row, col - 1))
        tiles.append((row, col + 1))
        return tiles

    def find_path_astar(self):
        import task_queue
        src = Node((self.start_row, self.start_col), Torch)
        target = Node((self.trow, self.tcol), Torch)

        open_list = task_queue.Queue()

        fr, fc = 20, 20
        closed = {
            NoTool: np.zeros(shape=(self.trow*fr, self.tcol*fc), dtype=bool),
            Torch: np.zeros(shape=(self.trow*fr, self.tcol*fc), dtype=bool),
            ClimbGear: np.zeros(shape=(self.trow*fr, self.tcol*fc), dtype=bool),
        }
        durations = {
            NoTool: np.zeros(shape=(self.trow*fr, self.tcol*fc), dtype=int),
            Torch: np.zeros(shape=(self.trow*fr, self.tcol*fc), dtype=int),
            ClimbGear: np.zeros(shape=(self.trow*fr, self.tcol*fc), dtype=int),
        }
        predecessors = {}
        open_list.add_task(src.to_tuple(), src.get_distance(target))

        num_iter = 0
        while not open_list.is_empty():
            current_node = Node.from_tuple(open_list.pop_task())

            if current_node == target:
                current_node.duration = durations[current_node.tool][current_node.pos]
                return current_node, predecessors, closed, durations

            closed[current_node.tool][current_node.pos] = True

            neighbors = self.get_neighbor_nodes(current_node)
            for neighbor in neighbors:
                num_iter += 1
                if num_iter % 10000 == 0:
                    print 'num_iter', num_iter, 'dist', neighbor.get_distance(target)
                if closed[neighbor.tool][neighbor.pos]:
                    continue

                if current_node.tool == neighbor.tool:
                    delta = StepDuration
                else:
                    delta = ChangeDuration

                current_duration = durations[current_node.tool][current_node.pos]
                g_value = current_duration + delta

                neighbor_t = neighbor.to_tuple()
                if neighbor_t in open_list:
                    neighbor_duration = durations[neighbor.tool][neighbor.pos]
                    if g_value >= neighbor_duration:
                        continue

                durations[neighbor.tool][neighbor.pos] = g_value

                f = g_value + neighbor.get_distance(target)
                if neighbor.tool != Torch:
                    f += ChangeDuration

                open_list.add_task(neighbor_t, f)
                predecessors[neighbor_t] = current_node.to_tuple()

        raise Exception('no path')


def check_grid():
    def generate_grid(depth, corner):
        # (x, y) -> geologic index, erosion level, risk
        grid = {}

        for y in range(0, corner[1] + 1):
            for x in range(0, corner[0] + 1):
                if (x, y) in [(0, 0), target_pos]:
                    geo = 0
                elif x == 0:
                    geo = y * 48271
                elif y == 0:
                    geo = x * 16807
                else:
                    geo = grid[(x - 1, y)][1] * grid[(x, y - 1)][1]
                ero = (geo + depth) % 20183
                risk = ero % 3
                grid[(x, y)] = (geo, ero, risk)
                #grid[(x, y)] = risk

        return grid

    corner = (target_pos[0] + 100, target_pos[1] + 100)
    grid = generate_grid(depth, corner)

    for y in range(0, corner[1] + 1):
        for x in range(0, corner[0] + 1):
            t1 = c.get_type((y, x))
            t2 = grid[(x, y)][2]
            if not t1 == t2:
                print 'types different'


def show_path():
    import matplotlib.pyplot as plt

    styles = {NoTool: '-k', ClimbGear: '-b', Torch: '-y'}
    for n1, n2 in zip(path[:-1], path[1:]):
        p1, _ = n1
        p2, c = n2
        plt.plot([p1[1], p2[1]], [p1[0], p2[0]], styles[c])

    plt.show()


def show_visited():
    import matplotlib.pyplot as plt
    plx, ply = 800, 200
    imn = np.array(closed[NoTool][:plx, :ply], dtype=int)
    imt = np.array(closed[Torch][:plx, :ply], dtype=int)
    imc = np.array(closed[ClimbGear][:plx, :ply], dtype=int)
    # plt.imshow(10*imn+ 20*imt + 30*imc)

    plt.ion()
    plt.figure()
    plt.imshow(imt)
    plt.colorbar()
    plt.show()


def check_path():
    for node1, node2 in zip(path[:-1], path[1:]):
        pos1, tool1 = node1
        pos2, tool2 = node2
        type1 = c.get_type(pos1)
        type2 = c.get_type(pos2)
        dt = (durations[tool2][pos2] - durations[tool1][pos1])
        dp = abs(pos1[0] - pos2[0]) + abs(pos1[1] - pos2[1])

        assert (dt == 1 and dp == 1 and tool1 == tool2) or (dt == 7 and dp == 0 and tool1 != tool2)
        assert tool1 in c.matching_tools[type1]
        assert tool2 in c.matching_tools[type2]

        print '{:3}, {:3} {} {} dt={} dp={} {} {}'.format(
            pos1[0], pos1[1],
            tool_str[tool1],
            type_tr[type1],
            dt, dp,
            durations[tool1][pos1],
            ('#' if dt == 7 else ''))


start_pos = (0, 0)

#depth, target_pos = 510, (10, 10)
depth, target_pos = 10689, (11, 722)

c = Cave(depth=depth, start_pos=start_pos, target_pos=target_pos)
#print c

#check_grid()
#check_path()

node, predecessors, closed, durations = c.find_path_astar()
print node.duration

path = []
target_pos_t = (target_pos[1], target_pos[0])
start_pos_t = (start_pos[1], start_pos[0])

p = target_pos_t, Torch
while True:
    if p == (start_pos_t, Torch):
        break
    path.append(p)
    p = predecessors[p]

path = path[::-1]


