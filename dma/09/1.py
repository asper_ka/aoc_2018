#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import itertools
import sys

def get_index(i):
    while i < 0:
        i += len(marbles)
    return i % len(marbles)
    
def get_marble(i):
    return marbles[get_index(i)]

def get_ins_index(c, v):
    if v == 1:
        i2 = 1
    elif v == 2:
        i2 = 1
    elif v == 3:
        i2 = 3
    else:
        i2 = get_index(c+1)+1
    return i2
    
def place_marble(c, v):
    i2 = get_ins_index(c, v)
    marbles.insert(i2, v)
    return i2

def print_marbles(ci):
    print p+1,
    for m in marbles[:ci]:
        print m,
    print '_{}_ '.format(marbles[ci]),
    for m in marbles[ci+1:]:
        print m,
    print

data = []
for line in file('data').readlines():
    items = line.split()
    data.append([int(items[0]), int(items[6])])
data = np.array(data, dtype=int)

num_players, num_marbles = data[0]

print 'num_players', num_players
print 'num_marbles', num_marbles

marbles = [0]
scores = [0]*num_players
p = 0
ci = 0

#num_marbles = 7999 + 1
#num_players = 10

#print '-', marbles
for m in range(1, num_marbles+1):
    if m%1000 == 0:
        print m
    if p == num_players:
        p = 0
    if m%23 == 0:
        scores[p] += m
        idx = get_index(ci-7)
        
        s2 = marbles.pop(idx)
        scores[p] += s2
        ci = idx
    else:
        ci = place_marble(ci, m)
    
    #print_marbles(ci)        
    p += 1

#print scores
print max(scores)
