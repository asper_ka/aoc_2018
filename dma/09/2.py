#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import itertools
import sys

class Marble(object):
    __slots__ = ['v', 'n', 'p']
    def __init__(self, v):
        self.v = v
        self.p = None
        self.n = None
    def __repr__(self):
        return '{}:{}:{}'.format(self.p.v, self.v, self.n.v)

def add_marble(c, value):
    m = Marble(value)
    if not c:
        m.p = m
        m.n = m
    else:
        m.p = c.n
        m.n = c.n.n
        c.n.n.p = m
        c.n.n = m
        
    marbles.append(m)
    return m

data = []
for line in file('data2').readlines():
    items = line.split()
    data.append([int(items[0]), int(items[6])])
data = np.array(data, dtype=int)

num_players, num_marbles = data[0]

#num_players = 9
#num_marbles = 25

print 'num_players', num_players
print 'num_marbles', num_marbles

scores = [0]*num_players
marbles = []
cur = None
cur = add_marble(cur, 0)

p = 0

for m in range(1, num_marbles):
    if m%1000 == 0:
        print m
    if p == num_players:
        p = 0
    if m%23 == 0:
        scores[p] += m
        p7 = cur.p.p.p.p.p.p.p
        scores[p] += p7.v
        cur = p7.n
        p7.p.n = cur
        cur.p = p7.p
    else:
        cur = add_marble(cur, m)
    
    p += 1

print max(scores)
