#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import re
import sys


class Direction:
    def __init__(self):
        pass
    Down = 0
    Left = 1
    Right = 2


class Field:
    Sand = 0
    Clay = 1
    Flow = 2
    Water = 3
    Source = 4

    tr = {
        '.': Sand,
        '#': Clay,
        '|': Flow,
        '~': Water,
        '+': Source
    }
    itr = dict((v, k) for k, v in tr.items())

    def __init__(self):
        pass

    def read_from_field(self, filename):
        rows = []
        for line in file(filename).readlines():
            row = []
            for c in line.strip():
                row.append(Field.tr[c])
            rows.append(row)

        self.field = np.array(rows, dtype=np.int8)
        self.init()

    def read_from_coords(self, filename):
        points = []
        for line in file(filename).readlines():
            line = line.strip()

            match = re.match('(\w)=(\d+),\s+(\w)=(\d+)\.\.(\d+)', line)
            if not match:
                raise ValueError('invalid input: {}'.format(line))
            else:
                c1, n1, c2, r1, r2 = match.groups()
                n1 = int(n1)
                r1 = int(r1)
                r2 = int(r2)

                if c1 == 'x':
                    col = n1
                    for row in range(r1, r2+1):
                        points.append((row, col))
                if c1 == 'y':
                    row = n1
                    for col in range(r1, r2+1):
                        points.append((row, col))

        points = np.array(points)
        deltas = np.min(points, axis=0) - [1, 2]
        points -= deltas
        spring_row, spring_col = 0, 500 - deltas[1]
        spring = np.array([spring_row, spring_col], dtype=int)

        m = np.max(points, axis=0) + [2, 3]

        self.field = np.zeros(shape=m)
        self.field[spring[0], spring[1]] = Field.Source

        for row, col in points:
            self.field[row, col] = Field.Clay

        self.init()

    def init(self):
        self.num_rows, self.num_cols = self.field.shape
        self.right_turns = np.zeros_like(self.field)
        self.left_turns = np.zeros_like(self.field)

        row, col = np.where(self.field == Field.Source)
        self.source_pos = int(row), int(col)

    def __repr__(self):
        out = ''
        for row, row_values in enumerate(self.field):
            for col, f in enumerate(row_values):
                out += '{}'.format(Field.itr[f])
            out += '\n'

        return out

    def get_next_pos(self, pos, direction):
        row, col = pos
        if direction == Direction.Down:
            return row+1, col
        elif direction == Direction.Left:
            return row, col-1
        elif direction == Direction.Right:
            return row, col+1

    def get_below(self, pos):
        row, col = pos
        if row < self.num_rows - 1:
            return self.field[row+1, col]
        else:
            return None

    def get_num_water(self):
        return len(np.where((self.field == Field.Water))[0])

    def get_num_flow(self):
        return len(np.where((self.field == Field.Flow))[0])

    def show(self):
        return
        print f
        print
        #raw_input()

    def move(self, start_pos, direction):
        cur_pos = start_pos

        while True:
            next_pos = self.get_next_pos(cur_pos, direction)
            if next_pos[0] >= self.num_rows-1:
                return False

            next_tile = self.field[next_pos]

            if direction == Direction.Down:
                if next_tile == Field.Clay or next_tile == Field.Water:
                    result_left = False
                    result_right = False
                    if not self.left_turns[cur_pos]:
                        result_left = self.move(cur_pos, Direction.Left)
                        if result_left == False:
                            self.left_turns[cur_pos] = True
                    if not self.right_turns[cur_pos]:
                        result_right = self.move(cur_pos, Direction.Right)
                        if result_right == False:
                            self.right_turns[cur_pos] = True

                    if (not result_left==False) and (not result_right==False):
                        if cur_pos != result_left:
                            water_pos = result_left
                        elif cur_pos != result_right:
                            water_pos = result_right
                        else:
                            water_pos = cur_pos
                        self.field[water_pos] = Field.Water
                        self.show()
                        return True
                    else:
                        return False
                elif next_tile == Field.Flow:
                    self.show()
                    cur_pos = next_pos
                else:
                    self.field[next_pos] = Field.Flow
                    self.show()
                    cur_pos = next_pos

            elif direction == Direction.Left or direction == Direction.Right:
                below = self.get_below(cur_pos)
                if below is None:
                    return False
                elif below == Field.Sand or below == Field.Flow:
                    if self.move(cur_pos, Direction.Down) == False:
                        return False
                elif below == Field.Water or below == Field.Clay:
                    next_tile = self.field[next_pos]
                    if next_tile == Field.Clay or next_tile == Field.Water:
                        return cur_pos
                    self.field[next_pos] = Field.Flow
                    self.show()
                    cur_pos = next_pos






f = Field()
#f.read_from_field('testdata_field_simple')
#f.read_from_field('testdata_field')

f.read_from_coords('data')
#f.read_from_coords('testdata_coords')
#sys.exit()

#f.show()

start_pos = f.source_pos
while True:
    result = f.move(f.source_pos, Direction.Down)
    if result == False:
        break

f.show()
print f.get_num_water() + f.get_num_flow()
print f.get_num_water()
