#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import itertools
import sys

patches = []

for line in file('data').xreadlines():
    f = line.split()
    coords, size = f[2], f[3]
    x, y = coords.replace(':', '').split(',')
    x = int(x)
    y = int(y)
    w, h = size.split('x')
    w = int(w)
    h = int(h)
    l, t = x, y
    r, b = x+w, y+h
    patches.append([l, t, r, b])

patches = np.array(patches)    

#sys.exit()

def calc_total_overlap():
    field = np.zeros(dtype=bool, shape=(1000, 1000))
    for p1, p2 in itertools.combinations(patches, 2):
        l1, t1, r1, b1 = p1
        l2, t2, r2, b2 = p2
        xo1, xo2 = min(r2, r1), max(l2, l1)
        yo1, yo2 = min(b2, b1), max(t2, t1)
        x_overlap = max(0, xo1 - xo2)
        y_overlap = max(0, yo1 - yo2)
        area = x_overlap * y_overlap
        if area == 0:
            continue
        
        xol, xor = xo2, xo1
        yot, yob = yo2, yo1
        #print xol,xor, x_overlap, yot,yob, y_overlap
        field[xol:xor, yot:yob] = np.ones(shape=(x_overlap, y_overlap))
        
    print "count", len(np.where(field)[0])

calc_total_overlap()

for i, p1 in enumerate(patches):
    patch_overlap = 0
    for j, p2 in enumerate(patches):
        if i==j:
            continue
        l1, t1, r1, b1 = p1
        l2, t2, r2, b2 = p2
        xo1, xo2 = min(r2, r1), max(l2, l1)
        yo1, yo2 = min(b2, b1), max(t2, t1)
        x_overlap = max(0, xo1 - xo2)
        y_overlap = max(0, yo1 - yo2)
        area = x_overlap * y_overlap
        patch_overlap += area
    if patch_overlap == 0:
        print i+1, patch_overlap
    
    
