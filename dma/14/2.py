#!/usr/bin/env python
# -*- coding: utf-8 -*-


class Recipes:
    def __init__(self, search):
        self.search = search
        # alles in eine Funktion verschoben. dict Lookup (self.xx) ist langsam!

    def sequence(self):
        scores = '37'
        e1 = 0
        e2 = 1
        while self.search not in scores[-7:]:
            ssum = int(scores[e1]) + int(scores[e2])

            # for c in str(ssum):
            #    scores += c
            scores += str(ssum)
            if len(scores) % 100000 == 0:
                print len(scores)

            e1 = (e1 + int(scores[e1]) + 1) % len(scores)
            e2 = (e2 + int(scores[e2]) + 1) % len(scores)

        print scores.index(self.search)


b = Recipes('190221')
b.sequence()