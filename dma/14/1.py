#!/usr/bin/env python
# -*- coding: utf-8 -*-


class Recipes:
    def __init__(self):
        self.scores = [3, 7]
        self.cur_recipe = [0, 1]

    def add_new(self):
        ssum = 0
        for i in self.cur_recipe:
            ssum += self.scores[i]
        for c in str(ssum):
            self.scores.append(int(c))
        pass

    def move_to_next(self):
        for i, cur in enumerate(self.cur_recipe):
            delta = self.scores[cur] + 1
            new_idx = (cur + delta) % len(self.scores)
            self.cur_recipe[i] = new_idx
        pass

    def show(self):
        for i in range(len(self.scores)):
            s = self.scores[i]
            if i in self.cur_recipe:
                print '[{}]'.format(s),
            else:
                print '{}'.format(s),
        print


b = Recipes()
while True:
    #b.show()
    b.add_new()
    b.move_to_next()
    num_r = 10
    start = 2018
    if len(b.scores) > start+num_r:
        print ''.join([str(s) for s in b.scores[start:start+num_r]])
        break
