#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import itertools
import sys

def getn(pattern):
    pn = []
    for c in pattern:
        if c=='#':
            pn.append(1)
        else:
            pn.append(0)
    return np.array(pn, dtype=bool)

def print_line(line):
    for c in line:
        if c:
            print '#',
        else:
            print '.',
    print
    
lines = file('data').readlines()
initial = lines[0].split()[2]
initial = getn(initial)
#print "initial", initial

patterns, results = [], []
for line in lines[1:]:
    items = line.split()
    pattern, result = items[0], items[2]
    pattern = getn(pattern)
    result = True if result == '#' else False
    #print pattern, result
    patterns.append(pattern)
    results.append(result)

fp = 0
lp = len(initial) - 1
ext = 25
field = np.array([False]*ext + list(initial) + [False]*ext, dtype=bool)
fp -= ext
lp += ext

for gen in range(20):
    nextgen = np.zeros_like(field)
    for i in range(2, len(field)-2):
        for p, r in zip(patterns, results):
            field_part = field[i-2:i+3]
            if all(field_part == p):
                nextgen[i] = r
                break

    #print_line(nextgen)
    field = nextgen

pot_nums = np.arange(fp, lp+1, dtype=int)
print sum(pot_nums[field==True])
