#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import itertools
import sys

def getn(pattern):
    pn = []
    for c in pattern:
        if c=='#':
            pn.append(1)
        else:
            pn.append(0)
    return np.array(pn, dtype=bool)

lines = file('data').readlines()
initial = lines[0].split()[2]
initial = getn(initial)

patterns, results = [], []
for line in lines[1:]:
    items = line.split()
    pattern, result = items[0], items[2]
    pattern = getn(pattern)
    result = True if result == '#' else False
    if result:
        patterns.append(pattern)
        results.append(result)

plants = set()
for i, plant in enumerate(initial):
    if plant:
        plants.add(i)

def matches(pattern, plant):
    q = np.array([plant-2 in plants,
                  plant-1 in plants,
                  plant in plants,
                  plant+1 in plants,
                  plant+2 in plants], dtype=bool)
    return all(pattern == q)

s = []
num_gen = 200
for gen in range(num_gen+1):
    s.append(sorted(list(plants)))
    plants_n = set()
    for plant in xrange(min(plants)-2, max(plants)+2+1):
        for pattern, result in zip(patterns, results):
            if matches(pattern, plant):
                if result:
                    plants_n.add(plant)
                break
    plants = plants_n
    print gen, sum(plants)

import matplotlib.pyplot as plt
plt.ion()
plt.figure()
for gen, l in enumerate(s):
    plt.plot(l, [gen]*len(l), '.k', ms=1)

print (int(50e9)-num_gen)*len(l)+sum(l)
