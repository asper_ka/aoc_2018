#!/usr/bin/env python
# -*- coding: utf-8 -*-

import networkx as nx


class Graph(nx.Graph):
    def __init__(self, start_pos):
        super(Graph, self).__init__()
        self.start_pos = start_pos

    def __str__(self):
        rows = [e[0] for e in self.nodes]
        cols = [e[1] for e in self.nodes]

        num_cols = max(cols) - min(cols) + 1

        out = ''
        out += '#' * (num_cols * 2 + 1)
        out += '\n'

        for row in range(min(rows), max(rows)+1):
            out += '#'
            for col in range(min(cols), max(cols)+1):
                if (row, col) == self.start_pos:
                    out += 'X'
                else:
                    out += '.'

                if self.has_edge((row, col), (row, col+1)):
                    out += '|'
                else:
                    out += '#'
            out += '\n'

            for col in range(min(cols), max(cols)+1):
                out += '#'
                if self.has_edge((row, col), (row+1, col)):
                    out += '-'
                else:
                    out += '#'

            out += '#\n'

        out += '\n'

        return out

    def get_distance(self, dest):
        try:
            return nx.shortest_path_length(self, self.start_pos, dest)
        except nx.exception.NetworkXNoPath:
            return None


class Parser:
    NEXT_OPTION = 1

    def __init__(self, data):
        self.data = data
        self.graph = None

    def create_graph(self):
        pos = (0, 0)
        self.graph = Graph(pos)
        self.graph.add_node(pos)

        it = iter(self.data)
        p.parse(pos, it)
        return self.graph

    def get_next_pos(self, pos, direction):
        row, col = pos
        if direction == 'N':
            return row-1, col
        elif direction == 'S':
            return row+1, col
        elif direction == 'E':
            return row, col+1
        elif direction == 'W':
            return row, col-1
        else:
            raise ValueError

    def parse(self, pos, it):
        while True:
            c = it.next()
            if c in 'NSEW':
                next_pos = self.get_next_pos(pos, c)
                self.graph.add_edge(pos, next_pos)
                pos = next_pos
            elif c == '(':
                while True:
                    ret = self.parse(pos, it)
                    if ret != Parser.NEXT_OPTION:
                        break
            elif c == '|':
                return Parser.NEXT_OPTION
            elif c in ')$':
                return
            elif c == '^':
                continue

#data = r'^WNE$'
#data = r'^ENWWW(NEEE|SSE(EE|N))$'
#data = r'^ENNWSWW(NEWS|)SSSEEN(WNSE|)EE(SWEN|)NNN$'
#data = r'^ESSWWN(E|NNENN(EESS(WNSE|)SSS|WWWSSSSE(SW|NNNE)))$'
#data = r'^WSSEESWWWNW(S|NENNEEEENN(ESSSSW(NWSW|SSEN)|WSWWN(E|WWS(E|SS))))$'

data = file('data').read().strip()

p = Parser(data)
graph = p.create_graph()
#print graph

pl = []
for node in graph.nodes:
    val = graph.get_distance(node)
    print val
    if val is not None:
        pl.append(val)

print max(pl)
print len([p for p in pl if p >= 1000])
