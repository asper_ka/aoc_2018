#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <map>
#include <sstream>

using namespace std;


int main()
{
    map<string, int> opcodes;
    opcodes["addr"] = 0;
    opcodes["addi"] = 1;
    opcodes["mulr"] = 2;
    opcodes["muli"] = 3;
    opcodes["setr"] = 4;
    opcodes["seti"] = 5;
    opcodes["gtrr"] = 6;
    opcodes["eqrr"] = 7;

    vector<vector<int>> program;

    std::ifstream infile("data");
    std::string line;
    int ip_reg;
    while (std::getline(infile, line))
    {
        std::istringstream iss(line);
        string cmd;
        int a, b, c;

        if (line[0] == '/')
        {
            continue;
        }
        else if (line[0] == '#')
        {
            string dummy;
            iss >> dummy >> ip_reg;
            cout << "ip_reg " << ip_reg << endl;
            continue;
        }
        if (!(iss >> cmd >> a >> b >> c)) { break; }
        vector<int> cmd_line(4);
        cout << cmd << " " << a << " " << b << " " << c << endl;
        cmd_line[0] = opcodes[cmd];
        cmd_line[1] = a;
        cmd_line[2] = b;
        cmd_line[3] = c;
        program.push_back(cmd_line);

    }    
    
    vector<int> r(6);
    for (int i=0; i<6; ++i)
    {
        r[i] = 0;
    }
    r[0] = 1;
    int ip = 0;
    int max_ip = program.size() - 1;
    long i_counter = 0;
    while(true)
    {
        if (ip > max_ip)
        {
            cout << "program finished" << endl;
            break;
        }
        r[ip_reg] = ip;
        vector<int> code = program[ip];

        int opcode = code[0];
        int a = code[1];
        int b = code[2];
        int c = code[3];

        switch (opcode)
        {
            case(0):
                r[c] = r[a] + r[b]; break;
            case(1):
                r[c] = r[a] + b; break;
            case(2):

                r[c] = r[a] * r[b]; break;
            case(3):
                r[c] = r[a] * b; break;
            case(4):

                r[c] = r[a]; break;
            case(5):
                r[c] = a; break;

            case(6):
                if (r[a] > r[b]) 
                    r[c] = 1;
                else
                    r[c] = 0;
                break;

            case(7):
                if (r[a] == r[b])
                    r[c] = 1;
                else
                    r[c] = 0;
                break;

        }
        
        ip = r[ip_reg];
        ip += 1;

        i_counter += 1;
        if (i_counter % 10000000 == 0)
        {
            cout << i_counter << endl;
        }
    }
    cout << "reg 0 " << r[0] << endl;
    
}



