#!/usr/bin/env python
# -*- coding: utf-8 -*-


def read_code(filename):
    ip_reg = None
    commands = []
    for line in file(filename).readlines():
        line = line.strip()
        if not line:
            continue
        if line.startswith('//'):
            continue
        elif line.startswith('#'):
            ip_reg = int(line.split()[-1])
        else:
            cmd, a, b, c = line.split()
            a = int(a)
            b = int(b)
            c = int(c)
            opcode = opcodes[cmd]
            commands.append([opcode, a, b, c])

    return ip_reg, commands


opcodes = {
    'addr': 0,
    'addi': 1,

    'mulr': 2,
    'muli': 3,

    'setr': 4,
    'seti': 5,

    'gtrr': 6,

    'eqrr': 7,
}

opcodes_inv = dict((v, k) for k, v in opcodes.items())

f = [
    lambda a, b: r[a] + r[b],
    lambda a, b: r[a] + b,

    lambda a, b: r[a] * r[b],
    lambda a, b: r[a] * b,

    lambda a, b: r[a],
    lambda a, b: a,

    lambda a, b: 1 if r[a] > r[b] else 0,

    lambda a, b: 1 if r[a] == r[b] else 0,
]

r = [1, 0, 0, 0, 0, 0]

ip = 0
i_counter = 0


ips = []
ip_reg, commands = read_code('data')
max_ip = len(commands) - 1
while True:
    if ip > max_ip:
        print 'program finished'
        break
    r[ip_reg] = ip
    code = commands[ip]
    opcode, a, b, c = code

    print r, opcodes_inv[opcode], a, b, c,
    r[c] = f[opcode](a, b)
    print r

    ip = r[ip_reg]
    ip += 1

    i_counter += 1
    #raw_input()
    #if i_counter % 100000 == 0:
    #    print r, ip
    #    print i_counter

    #if i_counter == 50:
    #    break

print 'register 0:', r[0]

#import matplotlib.pyplot as plt
#plt.plot(ips)
#plt.show()