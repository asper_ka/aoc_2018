#!/usr/bin/env python
# -*- coding: utf-8 -*-


class Proc:
    def __init__(self):
        self.r = [0, 0, 0, 0, 0, 0]
        self.ip_reg = None
        self.ip = 0
        self.i_counter = 0

        self.f = {
            'addr': lambda a, b: self.r[a] + self.r[b],
            'addi': lambda a, b: self.r[a] + b,

            'mulr': lambda a, b: self.r[a] * self.r[b],
            'muli': lambda a, b: self.r[a] * b,

            'banr': lambda a, b: self.r[a] & self.r[b],
            'bani': lambda a, b: self.r[a] & b,

            'borr': lambda a, b: self.r[a] | self.r[b],
            'bori': lambda a, b: self.r[a] | b,

            'setr': lambda a, b: self.r[a],
            'seti': lambda a, b: a,

            'gtir': lambda a, b: 1 if a > self.r[b] else 0,
            'gtri': lambda a, b: 1 if self.r[a] > b else 0,
            'gtrr': lambda a, b: 1 if self.r[a] > self.r[b] else 0,

            'eqir': lambda a, b: 1 if a == self.r[b] else 0,
            'eqri': lambda a, b: 1 if self.r[a] == b else 0,
            'eqrr': lambda a, b: 1 if self.r[a] == self.r[b] else 0,
        }

    def read_code(self, filename):
        ip_reg = None
        commands = []

        for line in file(filename).readlines():
            line = line.strip()
            if not line:
                continue
            if line.startswith('#'):
                ip_reg = int(line.split()[-1])
            else:
                cmd, a, b, c = line.split()
                a = int(a)
                b = int(b)
                c = int(c)
                commands.append([cmd, a, b, c])

        assert ip_reg is not None

        self.ip_reg = ip_reg
        self.commands = commands

    def run_program(self):
        while True:
            if self.ip > len(self.commands) - 1:
                print 'program finished'
                return
            code = self.commands[self.ip]
            self.r[self.ip_reg] = self.ip
            cmd, a, b, c = code
            self.call(cmd, a, b, c)
            self.ip = self.r[self.ip_reg]
            self.ip += 1
            self.i_counter += 1
            if self.i_counter % 10000 == 0:
                print self.i_counter

    def call(self, fname, a, b, c):
        #print 'ip={} {} {} {} {} {}'.format(self.ip, self.r, fname, a, b, c),
        self.r[c] = self.f[fname](a, b)
        #print ' {}'.format(self.r)


p = Proc()
p.read_code('data')
p.run_program()
print 'register 0:', p.r[0]
