#!/usr/bin/env python
# -*- coding: utf-8 -*

import numpy as np

data = np.genfromtxt('data', delimiter=',', dtype=int)

clusters = np.arange(len(data), dtype=int)

while True:
    assigned = np.zeros_like(clusters, dtype=bool)

    for c in set(clusters):
        if c not in clusters:
            continue
        fp = data[clusters == c]
        for p in fp:
            idx = np.where((np.sum(np.abs(p-data), axis=1) <= 3) & (assigned == False))
            clusters[idx] = c
            assigned[idx] = True
            if np.all(assigned):
                break

        if np.all(assigned):
            break

    num_clusters = len(set(clusters))
    print num_clusters
