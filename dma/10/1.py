#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import itertools
import sys

def print_field():
    px = xs - min(xs)
    py = ys - min(ys)
    lx, ly = max(px)+1, max(py)+1
    m = np.zeros(shape=(ly, lx))
    m[py, px] = 1
    
    for y in range(ly):
        for x in range(lx):
            if m[y, x]:
                print 'X',
            else:
                print ' ',
        print
    print
    

data = []
for line in file('data').readlines():
    line = line.replace('position=<', '')\
                .replace(',', '')\
                .replace('> velocity=<', ' ')\
                .replace('>', '')
    items = line.split()
    x = int(items[0])
    y = int(items[1])
    vx = int(items[2])
    vy = int(items[3])
    data.append([x, y, vx, vy])

xs, ys, vxs, vys = np.array(data, dtype=int).T

last_size = 1e10
num_sec = 0
while True:
    xs += vxs
    ys += vys
    num_sec += 1
    size = np.std(xs) + np.std(ys)
    if size > last_size:
        xs -= vxs
        ys -= vys
        num_sec -= 1
        print_field()
        print num_sec, 'seconds'
        break
    last_size = size
