#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import itertools
import sys

field = []
for line in file('data').readlines():
    line = line[:-1]
    field.append(list(line))

maxlen = max([len(l) for l in field])
for i in range(len(field)):
    field[i] += [' '] * (maxlen-len(field[i]))

field = np.array(field)

carts = []
dirs = []
for y, line in enumerate(field):
    for x,c in enumerate(line):
        if c in ('<>v^'):
            carts.append([x,y])
        if c in ('<>'):
            field[y, x] = '-'
        elif c in ('^v'):
            field[y, x] = '|'

        if c == '>':
            dirs.append('r')
        elif c == '<':
            dirs.append('l')
        elif c == '^':
            dirs.append('u')
        elif c == 'v':
            dirs.append('d')
            
carts = np.array(carts)
turns = ['left']*len(carts)

def get_next_turn(turn):
    if turn == 'left':
        return 'straight'
    elif turn == 'straight':
        return 'right'
    elif turn == 'right':
        return 'left'

def get_next_move(cart_id):
    x, y = carts[cart_id]
    d = dirs[cart_id]
    cur = field[y, x]

    if cur in ('-|'):
        return d
    elif cur == '\\':
        if d == 'r':
            return 'd'
        elif d == 'l':
            return 'u'
        elif d == 'u':
            return 'l'
        elif d == 'd':
            return 'r'
    elif cur == '/':
        if d == 'r':
            return 'u'
        elif d == 'l':
            return 'd'
        elif d == 'u':
            return 'r'
        elif d == 'd':
            return 'l'
    elif cur == '+':
        turn = turns[cart_id]
        turns[cart_id] = get_next_turn(turn)
        if turn == 'left':
            if d == 'r':
                return 'u'
            elif d == 'l':
                return 'd'
            elif d == 'u':
                return 'l'
            elif d == 'd':
                return 'r'
        elif turn == 'straight':
            return d
        elif turn == 'right':
            if d == 'r':
                return 'd'
            elif d == 'l':
                return 'u'
            elif d == 'u':
                return 'r'
            elif d == 'd':
                return 'l'

def print_field(carts):
    return
    fc = np.array(field)
    for x, y in carts:
        fc[y, x] = '#'
    for y, line in enumerate(fc):
        for x,c in enumerate(line):
            print c,
        print
    print
    raw_input()
new_pos = []

while True:
    cart_idx = carts[:,0] + carts[:,1]*maxlen
    print_field(carts)
    crashed = set()
    for cart_id in np.argsort(cart_idx):
        if cart_id in crashed:
            continue
        d = get_next_move(cart_id)
        dirs[cart_id] = d
        x, y = carts[cart_id]
        if d == 'r':
            x, y = x+1, y
        elif d == 'l':
            x, y = x-1, y
        elif d == 'u':
            x, y = x, y-1
        elif d == 'd':
            x, y = x, y+1
        carts[cart_id] = [x,y]
        #print carts
        
        #print
        
        for other_id in range(len(carts)):
            if other_id == cart_id:
                continue
            if all(carts[other_id] == carts[cart_id]):
                #print_field(carts)
                print 'crash at', x, y
                crashed.add(cart_id)
                crashed.add(other_id)
            
    carts = np.array([carts[i] for i in range(len(carts)) if i not in crashed])
    dirs = [dirs[i] for i in range(len(dirs)) if i not in crashed]
    turns = [turns[i] for i in range(len(turns)) if i not in crashed]
    #print len(carts)
    if len(carts) == 1:
        print 'remaining', carts
        sys.exit()
    print_field(carts)
