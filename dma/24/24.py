#!/usr/bin/env python
# -*- coding: utf-8 -*

import re


class Group(object):
    __slots__ = [
        'id',
        'num_units', 'hit_points', 'attack_damage', 'attack_type',
        'initiative', 'weaknesses', 'immunities', 'effective_power',
        'army', 'target', 'attacker']

    def __init__(self, group_id):
        self.id = group_id
        self.target = None  # type: Group
        self.attacker = None  # type: Group

    def __repr__(self):
        out = '{} group {}: {} units'.format(self.army.army_name, self.id, self.num_units)
        return out

    @property
    def effective_power(self):
        return self.num_units * self.attack_damage


class Army(object):
    __slots__ = ['army_name', 'groups', 'enemy']

    def __init__(self, army_name):
        self.army_name = army_name
        self.groups = []
        self.enemy = None

    def __repr__(self):
        out = ''
        for group in self.groups:
            out += repr(group) + '\n'
        return out


def read_file(filename, army_name):
    """
    :rtype: Army
    """
    army = Army(army_name)
    group_id = 0
    for line in file(filename).readlines():
        group_id += 1
        group = Group(group_id)

        m = re.search(r'(\d+) units each with (\d+) hit points', line)
        num_units, hit_points = m.groups()
        group.num_units = int(num_units)
        group.hit_points = int(hit_points)

        m = re.search(r'that does (\d+) (\w+) damage at initiative (\d+)', line)
        attack_damage, attack_type, initiative = m.groups()
        group.attack_damage = int(attack_damage)
        group.attack_type = attack_type
        group.initiative = int(initiative)

        m = re.search(r'weak to ([^\(\);]+)', line)
        weaknesses = []
        if m:
            weaknesses = m.group(1).split(',')
            weaknesses = [s.strip() for s in weaknesses]
        group.weaknesses = weaknesses

        m = re.search(r'immune to ([^\(\);]+)', line)
        immunities = []
        if m:
            immunities = m.group(1).split(',')
            immunities = [s.strip() for s in immunities]
        group.immunities = immunities
        group.army = army
        army.groups.append(group)

    return army


class Fight:
    def __init__(self, immune_data, infection_data):
        self.immune_army = read_file(immune_data, 'Immune')
        self.infection_army = read_file(infection_data, 'Infection')

        self.immune_army.enemy = self.infection_army
        self.infection_army.enemy = self.immune_army

    def choose_attacker(self, groups):
        """
        :rtype : Group
        """
        groups = [g for g in groups if g.target is None]
        if not groups:
            return None
        groups = sorted(groups, key=lambda g: g.effective_power)[::-1]
        max_power = max([g.effective_power for g in groups])
        groups = [g for g in groups if g.effective_power == max_power]
        groups = sorted(groups, key=lambda g: g.initiative)[::-1]
        attacker = groups[0]
        return attacker

    def choose_target(self, attacker):
        """
        :rtype : Group
        """
        targets = []  # type: list[Group]
        enemy_groups = attacker.army.enemy.groups
        attack_type = attacker.attack_type
        for target in enemy_groups:
            if target.attacker is not None:
                continue
            if attack_type not in target.immunities:
                targets.append(target)

        if not targets:
            return None

        weak_targets = [t for t in targets if attack_type in t.weaknesses]
        if weak_targets:
            targets = weak_targets

        targets = sorted(targets, key=lambda g: g.effective_power)
        max_power = max([g.effective_power for g in targets])
        targets = [g for g in targets if g.effective_power == max_power][::-1]
        targets = sorted(targets, key=lambda g: g.initiative)[::-1]
        target = targets[0]
        return target

    def choose_attackers_and_targets(self):
        all_groups = self.immune_army.groups + self.infection_army.groups
        attackers = []
        while all_groups:
            attacker = self.choose_attacker(all_groups)
            all_groups.remove(attacker)
            if attacker is None:
                break
            target = self.choose_target(attacker)
            attacker.target = target
            if target:
                target.attacker = attacker
            attackers.append(attacker)

        attackers = sorted(attackers, key=lambda g: g.initiative)[::-1]
        return attackers

    def reset_attackers_and_targets(self):
        for g in self.immune_army.groups + self.infection_army.groups:
            g.attacker = None
            g.target = None

    def run_round(self):
        dead = []
        attackers = self.choose_attackers_and_targets()
        for attacker in attackers:
            if attacker in dead:
                continue
            target = attacker.target
            if not target:
                continue
            power = attacker.effective_power
            if attacker.attack_type in target.weaknesses:
                power *= 2
            kills = int(power / target.hit_points)
            if kills > target.num_units:
                kills = target.num_units
            print '{} {} attacks {} {}, kills: {}'.format(
                attacker.army.army_name, attacker.id,
                target.army.army_name, target.id, kills)
            target.num_units -= kills
            if target.num_units == 0:
                dead.append(target)

        for group in dead:
            group.army.groups.remove(group)

        self.reset_attackers_and_targets()
        print

    def show_battlefield(self):
        print
        print '------------------------------'
        print self.immune_army
        print self.infection_army

    def run(self):
        while True:
            self.show_battlefield()
            self.run_round()
            #raw_input()
            if not self.immune_army.groups or not self.infection_army.groups:
                break

        self.show_battlefield()

    def get_winner(self):
        if len(self.immune_army.groups) == 0:
            return self.infection_army
        elif len(self.infection_army.groups) == 0:
            return self.immune_army
        else:
            raise Exception('no winner')


game = Fight('data_immune', 'data_infection')
#game = Fight('testdata_immune', 'testdata_infection')
game.run()

result = 0
for group in game.get_winner().groups:
    result += group.num_units

print result
