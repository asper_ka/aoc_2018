#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import sys

data = np.genfromtxt('data', dtype=int)

f = 0
freqs = set([f])

while True:
    for x in data:
        f += x
        if f in freqs:
            print f, 'found twice'
            sys.exit()
        freqs.add(f)
