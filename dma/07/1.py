#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import itertools
import sys
import collections

deps = {}
steps = set()

for line in file('data').xreadlines():
    items = line.split()
    pre, post = items[1], items[7]
    steps.add(pre)
    steps.add(post)
    
    if post not in deps:
        deps[post] = []
    deps[post].append(pre)

steps = sorted(steps)

order = ''
for i in range(len(steps)):
    for s in steps:
        if s not in deps or len(deps[s]) == 0:
            order += s
            steps.remove(s)
            break

    for d in deps.values():
        if s in d:
            d.remove(s)

print order
