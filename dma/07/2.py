#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import itertools
import sys
import collections

deps = {}
steps = set()

for line in file('data').xreadlines():
    items = line.split()
    pre, post = items[1], items[7]
    steps.add(pre)
    steps.add(post)
    
    if post not in deps:
        deps[post] = []
    deps[post].append(pre)

steps = sorted(steps)

def get_next_av_step():
    for s in steps:
        if s not in job and s not in deps or len(deps[s]) == 0:
            steps.remove(s)
            return s
    return None

def set_step_finished(s):
    for d in deps.values():
        if s in d:
            d.remove(s)

order = ''
num_w = 5
dur_delta = 60
trem = [0]*num_w
job = ['.']*num_w
sec = -1

while True:
    if len(steps) == 0 and all([t == 0 for t in trem]):
        break
    sec += 1

    for iw in range(len(trem)):
        if trem[iw] != 0:
            trem[iw] -= 1

    for iw in range(len(trem)):
        if trem[iw] == 0:
            s = job[iw]
            if s != '.':
                print iw+1, 'finished', s
                job[iw] = '.'
                set_step_finished(s)            
                order += s
        
    for iw in range(len(trem)):
        if trem[iw] == 0:
            s = get_next_av_step()
            #print 'next step', s
            if not s:
                break

            step_time = dur_delta + ord(s) - 64
            trem[iw] = step_time
            
            print iw+1, 'starts', s
            job[iw] = s
    
print order
print sec
