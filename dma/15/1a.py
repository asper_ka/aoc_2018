#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import itertools


class Field:
    Empty = 0
    Wall = 1
    Elf = 2
    Goblin = 3

    tr = {
        '.': Empty,
        '#': Wall,
        'E': Elf,
        'G': Goblin,
    }
    itr = dict((v, k) for k, v in tr.items())

    def __init__(self, filename):
        rows = []
        for line in file(filename).readlines():
            row = []
            for c in line.strip():
                row.append(Field.tr[c])
            rows.append(row)

        self.field = np.array(rows, dtype=np.int8)
        self.num_rows, self.num_cols = self.field.shape

    def __repr__(self):
        out = ''
        for row in self.field:
            for f in row:
                out += '{} '.format(Field.itr[f])
            out += '\n'
        return out

    def get_player_coords(self):
        return np.array(np.where((self.field == Field.Elf) | (self.field == Field.Goblin))).T

    def get_in_range_positions(self, row, col):
        if self.field[row, col] == Field.Elf:
            enemy = Field.Goblin
        elif self.field[row, col] == Field.Goblin:
            enemy = Field.Elf
        else:
            enemy = None
        rows, cols = np.where(self.field == enemy)
        positions = []
        for row, col in zip(rows, cols):
            if self.field[row - 1, col] == Field.Empty:
                positions.append((row - 1, col))
            if self.field[row + 1, col] == Field.Empty:
                positions.append((row + 1, col))
            if self.field[row, col - 1] == Field.Empty:
                positions.append((row, col - 1))
            if self.field[row, col + 1] == Field.Empty:
                positions.append((row, col + 1))

        return positions

    def get_rel_paths_to_target(self, row, col, t_row, t_col):
        # TODO braucht man hier alle Treppenkurven?
        segments = []
        if t_row != row:
            segments.append((t_row - row, 0))
        if t_col != col:
            segments.append((0, t_col - col))

        paths = list(itertools.permutations(segments, len(segments)))
        return paths

    def get_reachable_pos_and_paths(self, row, col, positions):
        pos_with_path = {}
        for target_pos in positions:
            t_row, t_col = target_pos
            paths_to_target = self.get_rel_paths_to_target(row, col, t_row, t_col)
            valid_paths = [p for p in paths_to_target if self.is_path_free(row, col, p)]
            if valid_paths:
                pos_with_path[target_pos] = tuple(valid_paths)
        return pos_with_path

    def get_distance(self, row, col, position):
        t_row, t_col = position
        distance = abs(row - t_row) + abs(col - t_col)
        return distance

    def get_flat_index(self, row, col):
        return self.num_cols*row + col

    def is_path_free(self, row, col, rel_path):
        idx, idy = 0, 0
        for rel_segment in rel_path:
            dy, dx = rel_segment
            if dy == 0:  # stay in same row
                s = np.sign(dx)
                squares = self.field[row+idy, col+s:col+dx+s:s]
                idx += dx
            elif dx == 0:  # stay in same col
                s = np.sign(dy)
                squares = self.field[row+s:row+dy+s:s, col+idx]
                idy += dy
            else:
                raise ValueError('invalid path segment')
            if not np.all(squares == Field.Empty):
                return False

        return True

    def choose_step_from_paths_in_reading_order(self, target, paths):
        t_row, t_col = target
        first_segments = [p[0] for p in paths]
        first_segments = sorted(first_segments,
                                key=lambda (r, c): self.get_flat_index(r+t_row, c+t_col))
        first_step = first_segments[0]
        return first_step

    def get_step(self, row, col, positions):
        pos_with_path = self.get_reachable_pos_and_paths(row, col, positions)
        reachable_positions = pos_with_path.keys()
        distances = [self.get_distance(row, col, target_pos) for target_pos in reachable_positions]
        min_dist = min(distances)

        nearest_reachable = []
        for target_pos in reachable_positions:
            if self.get_distance(row, col, target_pos) > min_dist:
                continue
            nearest_reachable.append(target_pos)

        nearest_reachable = sorted(nearest_reachable, key=lambda (r, c): self.get_flat_index(r, c))
        target = nearest_reachable[0]
        print 'target', target
        paths = pos_with_path[target]
        dy, dx = self.choose_step_from_paths_in_reading_order(target, paths)
        if dy != 0:
            return np.sign(dy), 0
        if dx != 0:
            return 0, np.sign(dx)

    def move(self, row, col):
        targets = self.get_in_range_positions(row, col)
        # TODO keine Targets verfügbar / erreichbar
        step = self.get_step(row, col, targets)
        dy, dx = step
        ftype = self.field[row, col]
        self.field[row, col] = Field.Empty
        self.field[row+dy, col+dx] = ftype


f = Field('testdata')

print f

for row, col in f.get_player_coords():
    print 'player', row, col
    # TODO prüfen, ob Angriff möglich ist
    f.move(row, col)
    raw_input()
    print f

# TODO Spieler nacheinander bewegen