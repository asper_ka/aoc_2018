#!/usr/bin/env python
# -*- coding: utf-8 -*-

from scipy.spatial.distance import pdist, squareform
from scipy.sparse import csr_matrix
from scipy.sparse.csgraph import dijkstra, shortest_path
import itertools
import numpy as np

max_value = 1e10


def calc_dist(a, b):
    xa, ya = a
    xb, yb = b
    if xa != xb and ya != yb:
        return max_value
    d = abs(xa-xb) + abs(ya-yb)
    if d != 1:
        return max_value
    if not field[xa, ya] or not field[xb, yb]:
        return max_value
    return d


field = np.array([
    [True, False, False, True],
    [True, False, False, True],
    [True, False, True, True],
    [True, True, True, False]
], dtype=bool)

size = 4

xs, ys = np.meshgrid(range(size), range(size))
xs = xs.reshape(xs.size)
ys = ys.reshape(ys.size)

coords = np.column_stack([xs, ys])
#print 'coords'
#print coords
dist = squareform(pdist(coords, metric=calc_dist))
#print 'dist'
#print dist

graph = csr_matrix(dist)

src = 0
dest = 12

print 'src', coords[src]
print 'dest', coords[dest]

#distances, predecessors = dijkstra(graph, directed=False, unweighted=False,
#                                   indices=src, return_predecessors=True)
distances, predecessors = shortest_path(graph, method='auto', directed=False,
                                        unweighted=False, return_predecessors=True,
                                        indices=src)

#print 'distances'
#print distances


path = []
i = dest

while i != src:
    path.append(coords[i])
    i = predecessors[i]

path.append(coords[src])
for x, y in path[::-1]:
    print x, y


