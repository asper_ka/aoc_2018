#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import networkx as nx


class Field:
    Empty = 0
    Wall = 1
    Elf = 2
    Goblin = 3

    tr = {
        '.': Empty,
        '#': Wall,
        'E': Elf,
        'G': Goblin,
    }
    itr = dict((v, k) for k, v in tr.items())

    def __init__(self, filename):
        rows = []
        for line in file(filename).readlines():
            row = []
            for c in line.strip():
                row.append(Field.tr[c])
            rows.append(row)

        self.field = np.array(rows, dtype=np.int8)
        self.num_rows, self.num_cols = self.field.shape
        self.create_graph()
        self.init_hitpoints()

    def __repr__(self):
        out = ''
        for row, row_values in enumerate(self.field):
            for col, f in enumerate(row_values):
                out += '{} '.format(Field.itr[f])
            for col, f in enumerate(row_values):
                if (row, col) in self.hitpoints:
                    out += '{} '.format(self.hitpoints[row, col])
            out += '\n'
        return out

    def init_hitpoints(self):
        self.hitpoints = {}
        for row, col in self.get_player_coords():
            self.hitpoints[(row, col)] = 200

    def create_graph(self):
        self.graph = nx.Graph()
        for row in range(self.num_rows):
            for col in range(self.num_cols):
                if self.field[row, col] != Field.Empty:
                    continue
                self.graph.add_node((row, col))
                self.add_edges_to_neighbors(row, col)

    def add_edges_to_neighbors(self, row, col):
        edges = []
        if col < self.num_cols - 1:
            if self.field[row, col + 1] == Field.Empty:
                edges.append(((row, col), (row, col + 1)))
        if col > 0:
            if self.field[row, col - 1] == Field.Empty:
                edges.append(((row, col), (row, col - 1)))
        if row < self.num_rows - 1:
            if self.field[row + 1, col] == Field.Empty:
                edges.append(((row, col), (row + 1, col)))
        if row > 0:
            if self.field[row - 1, col] == Field.Empty:
                edges.append(((row, col), (row - 1, col)))

        for e in edges:
            self.graph.add_edge(e[0], e[1])

    def remove_edges_to_neighbors(self, row, col):
        edges = []
        if col < self.num_cols - 1:
                edges.append(((row, col), (row, col + 1)))
        if col > 0:
                edges.append(((row, col), (row, col - 1)))
        if row < self.num_rows - 1:
                edges.append(((row, col), (row + 1, col)))
        if row > 0:
                edges.append(((row, col), (row - 1, col)))

        for e in edges:
            if self.graph.has_edge(e[0], e[1]):
                self.graph.remove_edge(e[0], e[1])

    def get_player_coords(self):
        return np.array(np.where((self.field == Field.Elf) | (self.field == Field.Goblin))).T

    def get_enemy(self, row, col):
        if self.field[row, col] == Field.Elf:
            return Field.Goblin
        elif self.field[row, col] == Field.Goblin:
            return Field.Elf
        else:
            return None

    def get_all_enemies(self, row, col):
        enemy = self.get_enemy(row, col)
        enemies = np.where(self.field == enemy)
        return enemies

    def get_in_range_positions(self, row, col):
        rows, cols = self.get_all_enemies(row, col)
        positions = []
        for row, col in zip(rows, cols):
            if self.field[row - 1, col] == Field.Empty:
                positions.append((row - 1, col))
            if self.field[row + 1, col] == Field.Empty:
                positions.append((row + 1, col))
            if self.field[row, col - 1] == Field.Empty:
                positions.append((row, col - 1))
            if self.field[row, col + 1] == Field.Empty:
                positions.append((row, col + 1))

        return positions

    def get_paths_to_target(self, row, col, t_row, t_col):
        self.add_edges_to_neighbors(row, col)
        try:
            paths = list(nx.all_shortest_paths(self.graph, (row, col), (t_row, t_col)))
        except nx.exception.NetworkXNoPath:
            return []
        self.remove_edges_to_neighbors(row, col)
        paths = [p[1:] for p in paths]
        return paths

    def get_reachable_pos_and_paths(self, row, col, positions):
        pos_with_path = {}
        for target_pos in positions:
            t_row, t_col = target_pos
            paths_to_target = self.get_paths_to_target(row, col, t_row, t_col)
            valid_paths = [p for p in paths_to_target if self.is_path_free(p)]
            if valid_paths:
                pos_with_path[target_pos] = tuple(valid_paths)
        return pos_with_path

    def get_flat_index(self, row, col):
        return self.num_cols*row + col

    def is_path_free(self, path):
        for row, col in path:
            if self.field[row, col] != Field.Empty:
                return False
        return True

    def get_next_pos_from_paths(self, paths):
        first_path_positions = [p[0] for p in paths]

        pos_sorted = sorted(first_path_positions,
                            key=lambda (r, c): self.get_flat_index(r, c))
        next_pos = pos_sorted[0]
        return next_pos

    def get_next_pos(self, row, col, positions):
        pos_with_path = self.get_reachable_pos_and_paths(row, col, positions)
        if not pos_with_path:
            return None
        min_dist = min([len(p[0]) for p in pos_with_path.values()])
        nearest_reachable = []
        for target_pos, paths in pos_with_path.items():
            if len(paths[0]) > min_dist:
                continue
            nearest_reachable.append(target_pos)

        nearest_reachable = sorted(nearest_reachable, key=lambda (r, c): self.get_flat_index(r, c))
        target = nearest_reachable[0]
        paths = pos_with_path[target]
        next_pos = self.get_next_pos_from_paths(paths)
        return next_pos

    def get_adjacent_enemies(self, row, col):
        enemy = self.get_enemy(row, col)
        enemies = []
        if col < self.num_cols - 1:
            if self.field[row, col+1] == enemy:
                enemies.append((row, col+1))
        if col > 0:
            if self.field[row, col - 1] == enemy:
                enemies.append((row, col-1))
        if row < self.num_rows - 1:
            if self.field[row+1, col] == enemy:
                enemies.append((row+1, col))
        if row > 0:
            if self.field[row-1, col] == enemy:
                enemies.append((row-1, col))
        return enemies

    def get_weakest_enemy(self, enemies):
        hitpoints = np.array([self.hitpoints[e] for e in enemies])
        idx = np.where(hitpoints == min(hitpoints))[0]
        enemies = [enemies[i] for i in idx]
        enemies = sorted(enemies,
                         key=lambda (r, c): self.get_flat_index(r, c))
        weakest_enemy = enemies[0]
        return weakest_enemy

    def run_attack(self, enemies):
        enemy = self.get_weakest_enemy(enemies)
        self.hitpoints[enemy] -= 3
        if self.hitpoints[enemy] <= 0:
            self.hitpoints.pop(enemy)
            self.field[enemy] = Field.Empty

    def run_unit_round(self, row, col):
        adjacent_enemies = self.get_adjacent_enemies(row, col)
        if adjacent_enemies:
            self.run_attack(adjacent_enemies)
            return True

        enemies = self.get_all_enemies(row, col)
        if len(enemies[0]) == 0:
            return False

        targets = self.get_in_range_positions(row, col)

        next_pos = self.get_next_pos(row, col, targets)
        if next_pos is None:
            return True

        row_n, col_n = next_pos
        ftype = self.field[row, col]
        self.field[row, col] = Field.Empty
        self.add_edges_to_neighbors(row, col)
        self.field[row_n, col_n] = ftype
        self.remove_edges_to_neighbors(row_n, col_n)

        self.hitpoints[(row_n, col_n)] = self.hitpoints[(row, col)]
        self.hitpoints.pop((row, col))

        adjacent_enemies = self.get_adjacent_enemies(row_n, col_n)
        if adjacent_enemies:
            self.run_attack(adjacent_enemies)

        return True

    def run_round(self):
        player_coords = self.get_player_coords()
        for i, (row, col) in enumerate(player_coords):
            print 'player {}/{}'.format(i+1, len(player_coords)), row, col
            if self.field[row, col] == Field.Empty:
                continue  # unit was killed by some other unit
            # print 'round', i + 1, 'player', row, col
            if not f.run_unit_round(row, col):
                return False
        return True


f = Field('testdata_summary_5')

print f
#raw_input()
for i in range(1000):
    print 'round', i+1
    if not f.run_round():
        print 'combat ends'
        rem_hp = sum(f.hitpoints.values())
        print 'remaining hitpoints {}'.format(rem_hp)
        print 'result {}'.format(rem_hp*i)

        break
    print f
    #raw_input()
