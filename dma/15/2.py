#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import networkx as nx


class Field:
    Empty = 0
    Wall = 1
    Elf = 2
    Goblin = 3

    tr = {
        '.': Empty,
        '#': Wall,
        'E': Elf,
        'G': Goblin,
    }
    itr = dict((v, k) for k, v in tr.items())

    def __init__(self, filename):
        rows = []
        for line in file(filename).readlines():
            row = []
            for c in line.strip():
                row.append(Field.tr[c])
            rows.append(row)

        self.field = np.array(rows, dtype=np.int8)
        self.num_rows, self.num_cols = self.field.shape
        self.create_graph()
        self.init_hitpoints()
        self.repr_with_edges = False
        self.killed_in_round = []
        self.attack_power = {Field.Elf: 3, Field.Goblin: 3}

    def set_power(self, ftype, power):
        self.attack_power[ftype] = power

    def __repr__(self):
        out = ''
        for row, row_values in enumerate(self.field):
            for col, f in enumerate(row_values):
                out += '{}'.format(Field.itr[f])
                if self.repr_with_edges:
                    if self.graph.has_edge((row, col), (row, col+1)):
                        out += '-'
                    else:
                        out += ' '

            out += ' '
            for col, f in enumerate(row_values):
                if (row, col) in self.hitpoints:
                    out += '{} '.format(self.hitpoints[row, col])
            out += '\n'

            if self.repr_with_edges:
                for col, f in enumerate(row_values):
                    if self.graph.has_edge((row, col), (row+1, col)):
                        out += '|'
                    else:
                        out += ' '
                    out += ' '
                out += '\n'

        return out

    def init_hitpoints(self):
        self.hitpoints = {}
        for row, col in self.get_player_coords():
            self.hitpoints[(row, col)] = 200

    def create_graph(self):
        self.graph = nx.Graph()
        for row in range(self.num_rows):
            for col in range(self.num_cols):
                self.graph.add_node((row, col))
                if self.field[row, col] != Field.Empty:
                    continue
                self.add_edges_to_empty_neighbors(row, col)

    def get_neighbors(self, row, col, ftype=None):
        neighbors = []
        if col < self.num_cols - 1:
            neighbors.append((row, col + 1))
        if col > 0:
            neighbors.append((row, col - 1))
        if row < self.num_rows - 1:
            neighbors.append((row + 1, col))
        if row > 0:
            neighbors.append((row - 1, col))

        if ftype is not None:
            neighbors = [n for n in neighbors if self.field[n[0], n[1]] == ftype]
        return neighbors

    def add_edges_to_empty_neighbors(self, row, col):
        for neighbor in self.get_neighbors(row, col, Field.Empty):
            self.graph.add_edge((row, col), neighbor)

    def remove_edges_to_neighbors(self, row, col):
        for neighbor in self.get_neighbors(row, col):
            if self.graph.has_edge((row, col), neighbor):
                self.graph.remove_edge((row, col), neighbor)

    def get_player_coords(self):
        return np.array(np.where((self.field == Field.Elf) | (self.field == Field.Goblin))).T

    def get_enemy(self, row, col):
        if self.field[row, col] == Field.Elf:
            return Field.Goblin
        elif self.field[row, col] == Field.Goblin:
            return Field.Elf
        else:
            return None

    def get_all_enemies(self, row, col):
        enemy = self.get_enemy(row, col)
        enemies = np.where(self.field == enemy)
        return enemies

    def get_in_range_positions(self, row, col):
        rows, cols = self.get_all_enemies(row, col)
        positions = []
        for row_e, col_e in zip(rows, cols):
            positions += self.get_neighbors(row_e, col_e, Field.Empty)
        positions = list(set(positions))
        return positions

    def get_shortest_distance(self, src, dest):
        row, col = src
        t_row, t_col = dest
        try:
            path_length = nx.shortest_path_length(self.graph, (row, col), (t_row, t_col))
        except nx.exception.NetworkXNoPath:
            path_length = None
        return path_length

    def get_reachable_pos_with_dist(self, row, col, positions):
        pos_with_dist = {}
        self.add_edges_to_empty_neighbors(row, col)
        for target_pos in positions:
            path_length = self.get_shortest_distance((row, col), target_pos)
            if path_length is not None:
                pos_with_dist[target_pos] = path_length
        self.remove_edges_to_neighbors(row, col)
        return pos_with_dist

    def get_flat_index(self, row, col):
        return self.num_cols*row + col

    def get_next_pos(self, row, col, positions):
        pos_with_dist = self.get_reachable_pos_with_dist(row, col, positions)
        if not pos_with_dist:
            return None
        min_dist_from_start = min(pos_with_dist.values())
        nearest_reachable = []
        for target_pos, dist in pos_with_dist.items():
            if dist > min_dist_from_start:
                continue
            nearest_reachable.append(target_pos)

        nearest_reachable = sorted(nearest_reachable, key=lambda (r, c): self.get_flat_index(r, c))
        target = nearest_reachable[0]

        neighbors_with_dist_to_target = {}
        neighbors = self.get_neighbors(row, col, Field.Empty)
        for n in neighbors:
            dist = self.get_shortest_distance(n, target)
            if dist is not None:
                neighbors_with_dist_to_target[n] = dist

        min_dist_from_start = min(neighbors_with_dist_to_target.values())

        step_candidates = []
        for neighbor, dist in neighbors_with_dist_to_target.items():
            if dist == min_dist_from_start:
                step_candidates.append(neighbor)

        next_pos_sorted = sorted(step_candidates, key=lambda (r, c): self.get_flat_index(r, c))
        next_pos = next_pos_sorted[0]
        return next_pos

    def get_adjacent_enemies(self, row, col):
        enemy = self.get_enemy(row, col)
        enemies = self.get_neighbors(row, col, enemy)
        return enemies

    def get_weakest_enemy(self, enemies):
        hitpoints = np.array([self.hitpoints[e] for e in enemies])
        idx = np.where(hitpoints == min(hitpoints))[0]
        enemies = [enemies[i] for i in idx]
        enemies = sorted(enemies,
                         key=lambda (r, c): self.get_flat_index(r, c))
        weakest_enemy = enemies[0]
        return weakest_enemy

    def run_attack(self, row, col, enemies):
        enemy = self.get_weakest_enemy(enemies)
        self.hitpoints[enemy] -= self.attack_power[self.field[row, col]]
        if self.hitpoints[enemy] <= 0:
            self.hitpoints.pop(enemy)
            self.field[enemy] = Field.Empty
            self.add_edges_to_empty_neighbors(enemy[0], enemy[1])
            self.killed_in_round.append(enemy)

    def get_num_remaining(self, ftype):
        return len(np.array(np.where((self.field == ftype))).T)

    def run_unit_round(self, row, col):
        adjacent_enemies = self.get_adjacent_enemies(row, col)
        if adjacent_enemies:
            self.run_attack(row, col, adjacent_enemies)
            return True

        enemies = self.get_all_enemies(row, col)
        if len(enemies[0]) == 0:
            return False

        targets = self.get_in_range_positions(row, col)

        next_pos = self.get_next_pos(row, col, targets)
        if next_pos is None:
            return True

        ftype = self.field[row, col]
        row_n, col_n = next_pos

        self.field[row_n, col_n] = ftype
        self.remove_edges_to_neighbors(row_n, col_n)

        self.field[row, col] = Field.Empty
        self.add_edges_to_empty_neighbors(row, col)

        self.hitpoints[(row_n, col_n)] = self.hitpoints[(row, col)]
        self.hitpoints.pop((row, col))

        adjacent_enemies = self.get_adjacent_enemies(row_n, col_n)
        if adjacent_enemies:
            self.run_attack(row_n, col_n, adjacent_enemies)

        return True

    def run_round(self):
        self.killed_in_round = []
        player_coords = self.get_player_coords()
        for row, col in player_coords:
            if (row, col) in self.killed_in_round:
                continue
            if not f.run_unit_round(row, col):
                return False
        return True


for elf_power in range(3, 1000):
    f = Field('data')
    f.set_power(Field.Elf, elf_power)
    print f
    print 'elf_power', elf_power

    e_start = f.get_num_remaining(Field.Elf)
    print 'elves at start', e_start
    for i in range(1000):
        #print 'round', i+1
        if not f.run_round():
            print 'combat ends'
            print f
            rem_hp = sum(f.hitpoints.values())
            print 'remaining hitpoints {}'.format(rem_hp)
            print 'result {}'.format(rem_hp*i)

            break
        #raw_input()
        #print f

    e_end = f.get_num_remaining(Field.Elf)
    print 'elves at end', e_end
    if e_start == e_end:
        print 'all elves survive', elf_power
        break