#!/usr/bin/env python
# -*- coding: utf-8 -*-

import networkx as nx

g = nx.Graph()
g.add_nodes_from(range(4))
g.add_edges_from([
    (0,0),
    (1,0),
    (0,1),
    (1,1),
])
g.add_edge((0,0), (1,0))
g.add_edge((0,0), (0,1))
g.add_edge((1,0), (1,1))
g.add_edge((0,1), (1,1))

print list(nx.all_shortest_paths(g, (0,0), (1,1)))
